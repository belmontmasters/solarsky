# README #

### Requirements
1. clone git https/ssh: git@bitbucket.org:belmontmasters/solarsky.git
2. copy missing files from clone in public\van_dave\solarsky:
* cache folder,
* log folder,
* img folder,
* upload folder,
* download folder,
* config\settings.inc.php file
3. update db configuration in **settings.inc.php**
4. copy latest .sql file from public\van_dave\solarsky\solarsky_todaydate.sql and import it on cmd/phpmyadmin
5. In phpmyadmin, update the value of table **ps_shop_url** according to your setup machine.
6. Generate new .htaccess file from the website/store(backend view) in Preferences > SEO & URLs : SET UP URLS block then click Save button.