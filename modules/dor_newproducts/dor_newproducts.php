<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;
if (!class_exists( 'DorImageBase' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/DorImageBase.php');
}
if (!class_exists( 'DorCaches' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/Caches/DorCaches.php');
}
class Dor_newproducts extends Module
{
	protected $html;

	public function __construct()
	{
		$this->name = 'dor_newproducts';
		$this->version = '1.0.0';
		$this->author = 'Dorado Themes';
		$this->tab = 'front_office_features';
		$this->need_instance = 0;

		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Dor New Products');
		$this->description = $this->l('Adds a block on the product page that displays products new create.');
	}

	public function install()
	{
		// Install Tabs
        if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
            $parent_tab = new Tab();
            // Need a foreach for the language
            $parent_tab->name[$this->context->language->id] = $this->l('Dor Extensions');
            $parent_tab->class_name = 'AdminDorMenu';
            $parent_tab->id_parent = 0; // Home tab
            $parent_tab->module = $this->name;
            $parent_tab->add();
        }
        $tab = new Tab();
        foreach (Language::getLanguages() as $language)
        $tab->name[$language['id_lang']] = $this->l('Dor New Products');
        $tab->class_name = 'AdminDorNewProduct';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
        $tab->module = $this->name;
        $tab->add();

		Configuration::updateValue('DOR_NEWPRODUCT_DISPLAY_PRICE', 0);
		Configuration::updateValue($this->name . '_new_module_title',"New Products");
		Configuration::updateValue($this->name . '_new_limit_pro',4);
		Configuration::updateValue($this->name . '_new_per_page',4);
		Configuration::updateValue($this->name . '_new_quanlity_image',100);
        Configuration::updateValue($this->name . '_new_thumb_width',250);
        Configuration::updateValue($this->name . '_new_thumb_height',250);
		$this->_clearCache('dor_newproducts.tpl');

		return (parent::install()
			&& $this->registerHook('productfooter')
			&& $this->registerHook('header')
			&& $this->registerHook('leftColumn')
			&& $this->registerHook('rightColumn')
			&& $this->registerHook('addproduct')
			&& $this->registerHook('updateproduct')
			&& $this->registerHook('deleteproduct')
			&& $this->registerHook('blockDorado3')
			&& $this->registerHook('blockDorado4')
			&& $this->registerHook('blockDorado5')
			&& $this->registerHook('DorNewproduct')
		);
	}

	public function uninstall()
	{
		$tab = new Tab((int) Tab::getIdFromClassName('AdminDorNewProduct'));
        $tab->delete();
		Configuration::deleteByName('DOR_NEWPRODUCT_DISPLAY_PRICE');
		Configuration::deleteByName($this->name . '_new_per_page');
		Configuration::deleteByName($this->name . '_new_module_title');
		Configuration::deleteByName($this->name . '_new_limit_pro');
		Configuration::deleteByName($this->name . '_new_quanlity_image');
        Configuration::deleteByName($this->name . '_new_thumb_width');
        Configuration::deleteByName($this->name . '_new_thumb_height');
		$this->_clearCache('dor_newproducts.tpl');

		return parent::uninstall();
	}
	private function _postProcess() {
        Configuration::updateValue($this->name . '_new_module_title', Tools::getValue('new_module_title'));
        Configuration::updateValue($this->name . '_new_limit_pro', Tools::getValue('new_limit_pro'));
        Configuration::updateValue($this->name . '_new_per_page', Tools::getValue('new_per_page'));
        Configuration::updateValue($this->name . '_new_quanlity_image', Tools::getValue('new_quanlity_image'));
        Configuration::updateValue($this->name . '_new_thumb_width', Tools::getValue('new_thumb_width'));
        Configuration::updateValue($this->name . '_new_thumb_height', Tools::getValue('new_thumb_height'));
        $this->_html .= $this->displayConfirmation($this->l('Configuration updated'));
    }
	public function getContent()
	{
		$this->html = '';
		if (Tools::isSubmit('submitCross') &&
			Tools::getValue('DOR_NEWPRODUCT_DISPLAY_PRICE') != 0 &&
			Tools::getValue('DOR_NEWPRODUCT_DISPLAY_PRICE') != 1
		)
			$this->html .= $this->displayError('Invalid displayPrice.');
		elseif (Tools::isSubmit('submitCross'))
		{
			Configuration::updateValue(
				'DOR_NEWPRODUCT_DISPLAY_PRICE',
				Tools::getValue('DOR_NEWPRODUCT_DISPLAY_PRICE')
			);
			if (!sizeof($this->_postErrors))
	            $this->_postProcess();
	        else {
	            foreach ($this->_postErrors AS $err) {
	                $this->_html .= '<div class="alert error">' . $err . '</div>';
	            }
	        }
			$this->_clearCache('dor_newproducts.tpl');
			$this->html .= $this->displayConfirmation($this->l('Settings updated successfully.'));
		}

		$this->html .= $this->renderForm();

		return $this->html;
	}

	protected function getCurrentProduct($products, $id_current)
	{
		if ($products)
		{
			foreach ($products as $key => $product)
			{
				if ($product['id_product'] == $id_current)
					return $key;
			}
		}

		return false;
	}

	public function getDataNewProduct($params){
		$new_module_title = Configuration::get($this->name.'_new_module_title');
		$nb = Configuration::get($this->name.'_new_limit_pro');
		$new_per_page = Configuration::get($this->name.'_new_per_page');
		$quanlity_image = Configuration::get($this->name.'_new_quanlity_image');
        $thumbWidth = Configuration::get($this->name.'_new_thumb_width');
        $thumbHeight = Configuration::get($this->name.'_new_thumb_height');
		$id_product = (int)$params['product']->id?(int)$params['product']->id:"id_dor_new";
		$product = $params['product'];
		$page = isset($_POST['page'])?$_POST['page']-1:0;
		$cache_id = 'dor_newproducts|'.$id_product;

		if (!$this->isCached('dor_newproducts.tpl', $this->getCacheId($cache_id)))
		{
			$fileCache = 'HomeNewProductsCaches';
			$id_lang = (int)$params['cookie']->id_lang;
			$dorCaches  = Tools::getValue('enableDorCache',Configuration::get('enableDorCache'));
			$cacheData = array();
			if($dorCaches){
				$objCache = new DorCaches(array('name'=>'default','path'=>_PS_ROOT_DIR_.'/override/Dor/Caches/smartcaches/dornewproducts/','extension'=>'.cache'));
	            $objCache->setCache($fileCache);
	            $cacheData = $objCache->renderData($fileCache);
	        }
	        if($cacheData && $dorCaches){
	                $productData = $cacheData['lists'];
	        }else{
				// Get infos
				$productData = Product::getNewProducts((int) Context::getContext()->language->id, $page, ($nb ? $nb : 5));
				
				// Remove current product from the list
				if (is_array($productData) && count($productData))
				{
					$productLists = array();
					foreach ($productData as $key => $item)
					{
						if ($product['id_product'] == $id_product)
						{
							unset($productData[$key]);
							continue;
						}else{
							$id_image = Product::getCover($item['id_product']);
	                        $images = "";
	                        if (sizeof($id_image) > 0){
	                            $image = new Image($id_image['id_image']);
	                            // get image full URL
	                            $image_url = "/p/".$image->getExistingImgPath().".jpg";
	                            $linkRewrite = $item['id_product']."_".$id_image['id_image']."_".$item['link_rewrite'];
	                            $images = DorImageBase::renderThumbProduct($image_url,$linkRewrite,$thumbWidth,$thumbHeight,$quanlity_image);
	                            $item['imageThumb'] = $images;
	                        }

	                        $roatorImage = $this->RotatorImg($item['id_product']);
	                        if($roatorImage['rotator_img']){
	                            $idRotator = $roatorImage['rotator_img'][0]['id_image'];
	                            if (sizeof($idRotator) > 0){
	                                $imageRotator = new Image($idRotator);
	                                // get image full URL
	                                $imageRotator_url = "/p/".$imageRotator->getExistingImgPath().".jpg";
	                                $linkRewriteRotator = $item['id_product']."_".$idRotator."_".$item['link_rewrite'];
	                                $imageRotator = DorImageBase::renderThumbProduct($imageRotator_url,$linkRewriteRotator,$thumbWidth,$thumbHeight,$quanlity_image);
	                                $item['thumb_image_rotator'] = $imageRotator;
	                            }
	                        }
						}
						$productLists[] = $item;
					}
					$productData = $productLists;
					$taxes = Product::getTaxCalculationMethod();
					if (Configuration::get('DOR_NEWPRODUCT_DISPLAY_PRICE'))
					{
						foreach ($productData as $key => $category_product)
						{
							if ($category_product['id_product'] != $id_product)
							{
								if ($taxes == 0 || $taxes == 2)
								{
									$productData[$key]['displayed_price'] = Product::getPriceStatic(
										(int)$category_product['id_product'],
										true,
										null,
										2
									);
								} elseif ($taxes == 1)
								{
									$productData[$key]['displayed_price'] = Product::getPriceStatic(
										(int)$category_product['id_product'],
										false,
										null,
										2
									);
								}
							}
						}
					}
					if($dorCaches){
						$data['lists'] = $productData;
			            $objCache->store($fileCache, $data, $expiration = TIME_CACHE_HOME);
		        	}
				}
			}
			$this->context->controller->addColorsToProductList($productData);
			// Display tpl
			$this->smarty->assign(
				array(
					'products' => $productData,
					'new_module_title' => $new_module_title,
					'per_page' => $new_per_page,
					'ProdDisplayPrice' => Configuration::get('DOR_NEWPRODUCT_DISPLAY_PRICE')
				)
			);
		}

		return $this->display(__FILE__, 'dor_newproducts.tpl', $this->getCacheId($cache_id));
	}
	public function RotatorImg($idproduct) {
            $id_shop = (int)Context::getContext()->shop->id;
            $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'image` img'; 
            $sql .= ' LEFT JOIN `'. _DB_PREFIX_ . 'image_shop` imgs';
            $sql .= ' ON img.id_image = imgs.id_image';
            $sql .= ' where imgs.`id_shop` ='.$id_shop ;
            $sql .= ' AND img.`id_product` ='.$idproduct ;
            $sql .= ' AND imgs.`rotator` =1' ;
            $imageNew = Db::getInstance()->ExecuteS($sql);
            if(!$imageNew) {
                  $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'image` img'; 
                  $sql .= ' where img.`rotator` =1';
                  $sql .= ' AND img.`id_product` ='.$idproduct ;
                  $imageNew = Db::getInstance()->ExecuteS($sql);
            }

            $images = array(
                'rotator_img'=>$imageNew,
                'idproduct'=>$idproduct
            );

        return $images;
    }

	public function hookHeader($params)
	{
		if (!isset($this->context->controller->php_self) || $this->context->controller->php_self != 'product')
			return;
		$this->context->controller->addCSS($this->_path.'css/dor_newproducts.css', 'all');
		$this->context->controller->addJS($this->_path.'js/dor_newproducts.js');
	}

	public function hookDorNewproduct($params){
		return $this->getDataNewProduct($params);
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'switch',
						'label' => $this->l('Display products\' prices'),
						'desc' => $this->l('Show the prices of the products displayed in the block.'),
						'name' => 'DOR_NEWPRODUCT_DISPLAY_PRICE',
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
                        'type' => 'text',
                        'label' => 'Module Title:',
                        'name' => 'new_module_title',
                        'class' => 'fixed-width-md',
                    ),
					array(
                        'type' => 'text',
                        'label' => 'Limit products:',
                        'name' => 'new_limit_pro',
                        'class' => 'fixed-width-md',
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Product Per Page:',
                        'name' => 'new_per_page',
                        'class' => 'fixed-width-md',
                    ),
					array(
                        'type' => 'text',
                        'label' => 'Quanlity Image:',
                        'name' => 'new_quanlity_image',
                        'class' => 'fixed-width-md',
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Thumb width image:',
                        'name' => 'new_thumb_width',
                        'class' => 'fixed-width-md',
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Thumb height image:',
                        'name' => 'new_thumb_height',
                        'class' => 'fixed-width-md',
                    ),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get(
			'PS_BO_ALLOW_EMPLOYEE_FORM_LANG'
		) : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitCross';
		$helper->currentIndex = $this->context->link->getAdminLink(
				'AdminModules',
				false
			).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		return array(
			'DOR_NEWPRODUCT_DISPLAY_PRICE' => Tools::getValue(
					'DOR_NEWPRODUCT_DISPLAY_PRICE',
					Configuration::get('DOR_NEWPRODUCT_DISPLAY_PRICE')
				),
			'new_per_page' => Tools::getValue('new_per_page', Configuration::get($this->name . '_new_per_page')),
			'new_module_title' => Tools::getValue('new_module_title', Configuration::get($this->name . '_new_module_title')),
			'new_limit_pro' => Tools::getValue('new_limit_pro', Configuration::get($this->name . '_new_limit_pro')),
			'new_quanlity_image' => Tools::getValue('new_quanlity_image', Configuration::get($this->name . '_new_quanlity_image')),
            'new_thumb_width' => Tools::getValue('new_thumb_width', Configuration::get($this->name . '_new_thumb_width')),
            'new_thumb_height' => Tools::getValue('new_thumb_height', Configuration::get($this->name . '_new_thumb_height')),
		);
	}

}
