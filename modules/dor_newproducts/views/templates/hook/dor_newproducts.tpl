{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if isset($products) && count($products) > 0 && $products !== false}
<div class="clearfix blockproductscategory">
	<h2 class="productscategory_h2">
		{if $products|@count == 1}
			{$new_module_title}
		{else}
			{$new_module_title}
		{/if}
	</h2>
	<div id="{if count($products) > $per_page}productscategory{else}productscategory_noscroll{/if}">
	<div id="newproductscategory_list">
		<div class="customNavigation">
	      <a class="btn dor-prev-crl"><i class="fa fa-angle-left"></i></a>
	      <a class="btn dor-next-crl"><i class="fa fa-angle-right"></i></a>
	    </div>
		<ul class="productsSamecategory_list">
			{foreach from=$categoryProducts item='categoryProduct' name=categoryProduct}
			<li>
				<a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)}" class="lnk_img" title="{$categoryProduct.name|htmlspecialchars}"><img src="{$categoryProduct.imageThumb}" alt="{$categoryProduct.name|htmlspecialchars}" /></a>
				<p class="product_name">
					<a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)|escape:'html'}" title="{$categoryProduct.name|htmlspecialchars}">{$categoryProduct.name|truncate:25:'...'|escape:'html':'UTF-8'}</a>
				</p>
				{if $ProdDisplayPrice AND $categoryProduct.show_price == 1 AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
				<p class="price_display">
					<span class="price">{convertPrice price=$categoryProduct.displayed_price}</span>
				</p>
				{else}
				<br />
				{/if}
			</li>
			{/foreach}
		</ul>
	</div>
	</div>
	<script>
    $(document).ready(function($) {
      var dorCarousel = $(".productsSamecategory_list");
      dorCarousel.owlCarousel({
      	items :4,
		itemsDesktop : [1199, 4],
        itemsDesktopSmall : [979, 3],
        itemsTablet : [768, 2],
        itemsTabletSmall : false,
        itemsMobile : [479, 1],
      });
      $(".dor-next-crl").click(function(){
        dorCarousel.trigger('owl.next');
      })
      $(".dor-prev-crl").click(function(){
        dorCarousel.trigger('owl.prev');
      })
    });
    </script>
</div>
{/if}
