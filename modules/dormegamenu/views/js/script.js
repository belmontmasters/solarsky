jQuery(document).ready(function(){
   	parentClickAble();
    $(window).resize(parentClickAble);
    DORMENU.init();
});

function parentClickAble()
{
	if($(window).width() >= 992){
        $('#dor-top-menu a.dropdown-toggle').click(function(){
            var redirect_url = $(this).attr('href');
            window.location = redirect_url;
        });
    }
}

var DORMENU = {
	init:function(){
		DORMENU.SetActiveMenu();
		DORMENU.OpenClose();
	},
	SetActiveMenu:function(){
		var urlPath = location.pathname;
		var urlFull = location.href;
		var check = jQuery('ul.megamenu > li > a[href="'+urlFull+'"]').html();
		if(typeof check != "undefined"){
			jQuery('ul.megamenu > li > a[href="'+urlFull+'"]').closest("li").addClass("active");
		}else{
			jQuery('a[href="'+urlPath+'"]').closest(".widget-heading").addClass("active");
			jQuery('a[href="'+urlFull+'"]').closest(".widget-heading").addClass("active");
			jQuery('a[href="'+urlPath+'"]').closest("li").addClass("active");
			jQuery('a[href="'+urlFull+'"]').closest("li").addClass("active");
			jQuery('a[href="'+urlFull+'"]').closest("li.parent.dropdown").addClass("active");
			jQuery('a[href="'+urlFull+'"]').closest("li").parents("li").addClass("active");
		}
		
		

	},
	OpenClose:function(){
		jQuery(".open_menu").click(function(){
			jQuery( "#dor-top-menu" ).animate({
			    left: 0,
			}, 500);
			jQuery("body").addClass("dor-mobile");
		});
		jQuery(".close_menu").click(function(){
			jQuery( "#dor-top-menu" ).animate({
			    left: -250,
			}, 500);
			jQuery("body").removeClass("dor-mobile");
		});
	}
};
