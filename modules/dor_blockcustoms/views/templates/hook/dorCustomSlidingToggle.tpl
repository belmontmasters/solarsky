<div class="custom-block-sliding col-log-7 col-sm-8 col-md-7 col-xs-12">
	<i style="margin-right: 5px;" class="icon-phone"></i>
	<span class="txt-phone-line">(+101) 123 45 67 89</span>
	<a href="#" class="txt-line-contact">{l s='Contact Us'}</a>
	<a href="#" class="compare-product-sliding" title="{l s='Compare'}"><i class="fa fa-clone"></i><span>&nbsp;{l s='Compare'}&nbsp;(<span>0</span>)</span></a>
	<div class="compare-popup theme-border-color"><p class="empty">{l s='You have no items to compare.'}</p></div>
</div>