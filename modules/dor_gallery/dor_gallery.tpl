<div id="dor-leftgallery">
	<h2 class="sdstitle_block">
		<a href="#" onclick="return false">Gallery</a>
	</h2>
	<div class="main-gallery">
		<ul class="dorGalleryData clearfix">
			{foreach from=$gallerys item=gallery name=gallery}
			<li><a href="{$gallery.main}" rel="prettyPhoto[gallery]"><img itemprop="image" alt="" src="{$gallery.thumb}"></a></li>
			{/foreach}
		</ul>
	</div>
</div>