<?php

if (!defined('_PS_VERSION_'))
	exit;
if (!class_exists( 'DorImageBase' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/DorImageBase.php');
}
class dor_gallery extends Module
{
    public function __construct()
    {
        $this->name = 'dor_gallery';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
		$this->author = 'Dorado Themes';
		$this->need_instance = 0;

        $this->bootstrap = true;
		parent::__construct();	

		$this->displayName = $this->l('Dor Gallery Images');
        $this->description = $this->l('Displays a block lists gallery images.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

	public function install()
	{

		// Install Tabs
        if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
            $parent_tab = new Tab();
            // Need a foreach for the language
            $parent_tab->name[$this->context->language->id] = $this->l('Dor Extensions');
            $parent_tab->class_name = 'AdminDorMenu';
            $parent_tab->id_parent = 0; // Home tab
            $parent_tab->module = $this->name;
            $parent_tab->add();
        }
        $tab = new Tab();
        foreach (Language::getLanguages() as $language)
        $tab->name[$language['id_lang']] = $this->l('Dor Gallery Images');
        $tab->class_name = 'AdminDorGallery';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
        $tab->module = $this->name;
        $tab->add();

		Configuration::updateValue('gallery_limit', 10);
		Configuration::updateValue('gallery_show_list', 0);
		Configuration::updateValue('gallery_thumb_width', 155);
		Configuration::updateValue('gallery_thumb_height', 80);
		Configuration::updateValue('gallery_path', "img/cms/gallery");
		Configuration::updateValue('gallery_list_images', "");
		$success = (parent::install() &&
			$this->registerHook('header') &&
			$this->registerHook('displayBackOfficeHeader') &&
			$this->registerHook('dorGallery') &&
			$this->registerHook('dorGallery2') && 
			$this->registerHook('displaySmartBlogLeft') && 
			$this->registerHook('displaySmartBlogRight')
		);

		if ($success)
		{
			// Hook the module either on the left or right column
			$theme = new Theme(Context::getContext()->shop->id_theme);
		}
		return $success;
    }
    public function uninstall() {
        $tab = new Tab((int) Tab::getIdFromClassName('AdminDorGallery'));
        $tab->delete();
        Configuration::deleteByName($this->name . 'gallery_limit', 10);
        Configuration::deleteByName($this->name . 'gallery_show_list', 0);
        Configuration::deleteByName($this->name . 'gallery_thumb_width', 155);
        Configuration::deleteByName($this->name . 'gallery_thumb_height', 80);
        Configuration::deleteByName($this->name . 'gallery_path', "img/cms/gallery");
        Configuration::deleteByName($this->name . 'gallery_list_images', "");
        // Uninstall Module
        if (!parent::uninstall())
            return false;
        return true;
    }
    private function _postProcess() {
        Configuration::updateValue($this->name . 'gallery_limit', Tools::getValue('gallery_limit'));
        Configuration::updateValue($this->name . 'gallery_show_list', Tools::getValue('gallery_show_list'));
        Configuration::updateValue($this->name . 'gallery_thumb_width', Tools::getValue('gallery_thumb_width'));
        Configuration::updateValue($this->name . 'gallery_thumb_height', Tools::getValue('gallery_thumb_height'));
        Configuration::updateValue($this->name . 'gallery_path', Tools::getValue('gallery_path'));
        Configuration::updateValue($this->name . 'gallery_list_images', Tools::getValue('gallery_list_images'));
        $this->_html .= $this->displayConfirmation($this->l('Configuration updated'));
    }
	public function hookDorGallery($params)
	{
		if (!$this->isCached('dor_gallery.tpl', $this->getCacheId()))
		{
			$pathLink = __PS_BASE_URI__;
			$pathRoot = $_SERVER['DOCUMENT_ROOT'].$pathLink;
			$gallerys = array();
			$showlists = Configuration::get('gallery_show_list');
			$pathImages = Configuration::get('gallery_path');
			$thumbWidth = Configuration::get('gallery_thumb_width');
			$thumbHeight = Configuration::get('gallery_thumb_height');
			$quanlity_image = 90;
			$itemPath = $pathLink.$pathImages;
			$gallerys = array();
			$items = array();
			if($showlists == 0 && $pathImages != ""){
				$fullPath = $pathRoot.$pathImages;
				$gallerys = $this->dirToArray($fullPath);
				if(count($gallerys) > 0){
					foreach ($gallerys as $key => $item) {
						$image_url = "/cms/gallery/".$item;
	                    $linkRewrite = $item;
	                    $items[$key]['main'] = $itemPath.'/'.$item;
	                    $items[$key]['thumb'] = DorImageBase::renderThumbGallery($image_url,$linkRewrite,$thumbWidth,$thumbHeight,$quanlity_image);
					}
				}
			}else{

			}
			$languages = Language::getLanguages(true, $this->context->shop->id);
			$per = 6;
			$this->smarty->assign(array(
				'gallerys' => $items,
				'itemPath' => $itemPath,
				'languages' => $languages,
				'gallery_limit' => Configuration::get('gallery_limit'),
			));
		}
		return $this->display(__FILE__, 'dor_gallery.tpl', $this->getCacheId());
	}
	public function hookdisplaySmartBlogLeft($params)
    {
         return $this->hookDorGallery($params);
    }
    public function hookdisplaySmartBlogRight($params)
    {
         return $this->hookDorGallery($params);
    } 

    public function dirToArray($dir) {
	   $result = array();
	   $cdir = scandir($dir);
	   foreach ($cdir as $key => $value)
	   {
	      if (!in_array($value,array(".","..")))
	      {
	         if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
	         {
	            $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
	         }
	         else
	         {
	            $result[] = $value;
	         }
	      }
	   }
	  
	   return $result;
	} 
	public function getContent()
	{
		$output = '';
		if (Tools::isSubmit('submitDorGallery'))
		{
			$text_nb = (int)(Tools::getValue('gallery_limit'));
			$gallery_show_list = (int)(Tools::getValue('gallery_show_list'));
			$gallery_thumb_width = (int)(Tools::getValue('gallery_thumb_width'));
			$gallery_thumb_height = (int)(Tools::getValue('gallery_thumb_height'));
			$gallery_path = (Tools::getValue('gallery_path'));
			$gallery_list_images = (Tools::getValue('gallery_list_images'));
			Configuration::updateValue('gallery_limit', $text_nb);
			Configuration::updateValue('gallery_show_list', $gallery_show_list);
			Configuration::updateValue('gallery_thumb_width', $gallery_thumb_width);
			Configuration::updateValue('gallery_thumb_height', $gallery_thumb_height);
			Configuration::updateValue('gallery_path', $gallery_path);
			Configuration::updateValue('gallery_list_images', $gallery_list_images);
			$this->_clearCache('dor_gallery.tpl');
			if (!sizeof($this->_postErrors))
                $this->_postProcess();
            else {
                foreach ($this->_postErrors AS $err) {
                    $this->_html .= '<div class="alert error">' . $err . '</div>';
                }
            }
		}
		return $output.$this->renderForm();
	}

	public function hookHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'dor_gallery.css', 'all');
		$this->context->controller->addCSS(($this->_path).'prettyPhoto.css', 'all');
		$this->context->controller->addJS($this->_path . 'dor_gallery.js');
		$this->context->controller->addJS($this->_path . 'jquery.prettyPhoto.js');
	}
	public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addJS($this->_path . 'dor_gallery_admin.js');
    }
	
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
                        'type' => 'switch',
                        'label' => $this->l('Enabled List:'),
                        'name' => 'gallery_show_list',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
					array(
						'type' => 'text',
						'label' => $this->l('Path Url Images'),
						'name' => 'gallery_path',
						'class' => 'fixed-width-md path-images'
					),
					array(
                        'type' => 'textarea',
                        'label' => 'List Images:',
                        'name' => 'gallery_list_images',
                        'class' => 'fixed-width-full lists-images',
                        'cols' => 45,
                        'rows' => 5,
                    ),
					array(
						'type' => 'text',
						'label' => $this->l('Number manufacturer display'),
						'name' => 'gallery_limit',
						'class' => 'fixed-width-md'
					),
					array(
						'type' => 'text',
						'label' => $this->l('Thumb Width'),
						'name' => 'gallery_thumb_width',
						'class' => 'fixed-width-md'
					),
					array(
						'type' => 'text',
						'label' => $this->l('Thumb Height'),
						'name' => 'gallery_thumb_height',
						'class' => 'fixed-width-md'
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitDorGallery';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}
	
	public function getConfigFieldsValues()
	{		
		return array(
			'gallery_limit' => Tools::getValue('gallery_limit', Configuration::get('gallery_limit')),
			'gallery_show_list' => Tools::getValue('gallery_show_list', Configuration::get('gallery_show_list')),
			'gallery_thumb_width' => Tools::getValue('gallery_thumb_width', Configuration::get('gallery_thumb_width')),
			'gallery_thumb_height' => Tools::getValue('gallery_thumb_height', Configuration::get('gallery_thumb_height')),
			'gallery_path' => Tools::getValue('gallery_path', Configuration::get('gallery_path')),
			'gallery_list_images' => Tools::getValue('gallery_list_images', Configuration::get('gallery_list_images')),
		);
	}
}
