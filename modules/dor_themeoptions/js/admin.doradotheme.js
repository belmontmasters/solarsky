var ADM_DORTHEME = {
	init:function(){
		ADM_DORTHEME.AccTabTheme();
		ADM_DORTHEME.ClearColor();
		ADM_DORTHEME.ChoseThemeColor();
		ADM_DORTHEME.ChoseBackgroundImage();
		ADM_DORTHEME.EnableCategoryThumb();
	},
	AccTabTheme:function(){
		jQuery(".square-button").click(function(){
			var _this=this;
			var objID = jQuery(this).closest("div.tool-class-admin").attr("id");
			  jQuery( "#"+objID+" .box_lab" ).slideToggle( "fast", function() {
			    if(jQuery(this).is(":visible"))
				  {
					var itext = '<i class="fa fa-minus-square"></i>';
					jQuery(_this).find('i').remove();
				  }     
				else
				  {
					var itext = '<i class="fa fa-plus-square"></i>';
					jQuery(_this).find('i').remove();
				  }
				jQuery(_this).html(itext);
			  });
		})
	},
	ClearColor:function(){
		jQuery(".clear-bg").click(function(){
			jQuery(this).closest('.input-group').find(".mColorPickerInput").val("").css("background-color","transparent");
		})
	},
	ChoseThemeColor:function(){
		$('.cl-td-layout').click(function(){
	        var val = $(this).attr('id');
	        if($(this).hasClass("active")) val = "";
	        $("#dorthemecolor").remove();
	        $(".cl-pattern").append('<input type=hidden id="dorthemecolor" name="dorthemecolor" value="'+val+'">');
	        if(!$(this).hasClass("active")){
            	$(".cl-td-layout").removeClass('active');
            	$(this).addClass('active');
            }else{
            	$(this).removeClass('active');
            }
	    });
	},
	ChoseBackgroundImage:function(){
		jQuery(".cl-pattern > div.cl-image").click(function(){
			var val = $(this).attr('id');
			if($(this).hasClass("active")) val = "";
			$("#bgdorthemebg").remove();
            $(".cl-pattern").append('<input type=hidden id="bgdorthemebg" name="dorthemebg" value="'+val+'">');
            if(!$(this).hasClass("active")){
            	$("div.cl-image").removeClass('active');
            	$(this).addClass('active');
            }else{
            	$(this).removeClass('active');
            }
		});
	},
	EnableCategoryThumb:function(){
		jQuery("input[name='dorCategoryThumb']").change(function(){
			var val = jQuery(this).val();
			if(parseInt(val) == 1){
				jQuery(".group-cate-thumb").removeClass("hidden");
			}else{
				jQuery(".group-cate-thumb").addClass("hidden");
			}
		});
	}
}

$(document).ready(function(){
	ADM_DORTHEME.init();
});