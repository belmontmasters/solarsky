var DORTHEME = {
	PathFont:"",
	PathColor:"",
	init:function(){
		DORTHEME.ControlOption();
		DORTHEME.ChooseColorOption();
		DORTHEME.ChooseFontOption();
		DORTHEME.ChooseBackgroundOption();
		DORTHEME.ChooseModeOption();
		DORTHEME.ResetOptions();
		DORTHEME.RebuiltColor();
		DORTHEME.RebuiltFont();
		DORTHEME.RebuiltBackground();
		DORTHEME.RebuiltMode();
	},
	ControlOption:function(){
		jQuery(".dor-wrap > .control").unbind("click");
		jQuery(".dor-wrap > .control").click(function(){
			var pos = jQuery(".dor-wrap").position();
			if(pos.left == 0){
				$( ".dor-wrap" ).animate({
					left: "-228"
				}, 600, function() {});
			}else{
				$( ".dor-wrap" ).animate({
					left: "0"
				}, 600, function() {});
			}
		});
	},
	ChooseFontOption:function(){
		jQuery("#dor_font_options").change(function(){
			var font = jQuery(this).val();
			if(font == "") font = "font";
			localStorage.setItem("optionFont", font);
			var linkFont = DORTHEME.PathFont+"/"+font+".css";
			$('head link[href="'+DOR_FONT+'"]').attr('href',linkFont);
			DOR_FONT = linkFont;
		});
	},
	ChooseColorOption:function(){
		jQuery(".cl-td-layout a").click(function(){
			var color = jQuery(this).attr("id");
			localStorage.setItem("optionColor", color);
			var linkColor = DORTHEME.PathColor+"/"+color+".css";
			$('head link[href="'+DOR_COLOR+'"]').attr('href',linkColor);
			jQuery(".cl-td-layout").find("span").removeClass("selected");
			jQuery("#"+color).find("span").addClass("selected");
			DOR_COLOR = linkColor;
		});
	},
	ChooseModeOption:function(){
		jQuery(".mode_theme").change(function(){
			var mode = jQuery(this).val();
			jQuery("#page").removeClass("full").removeClass("boxed");
			jQuery("#page").addClass(mode);
			localStorage.setItem("optionMode", mode);
			window.location.reload();
		});
	},
	ChooseBackgroundOption:function(){
		jQuery(".cl-pattern .cl-image").click(function(){
			for(var i=1;i<=30;i++){
				jQuery("body").removeClass("pattern"+i);
			}
			var bg = jQuery(this).attr("id");
			localStorage.setItem("optionBg", bg);
			$('body').addClass(bg);
			jQuery(".cl-image").removeClass("selected");
			jQuery("#"+bg).addClass("selected");
		});
	},
	ResetOptions:function(){
		jQuery(".cl-reset").click(function(){
			localStorage.removeItem("optionColor");
			localStorage.removeItem("optionFont");
			localStorage.removeItem("optionBg");
			localStorage.removeItem("optionMode");
			window.location.reload();
		});
	},
	RebuiltColor:function(){
		var color = localStorage.getItem("optionColor");
		linkUrl = DOR_COLOR;
		if(typeof linkUrl != "undefined"){
			var linkUrls = linkUrl.split("/");
			var newArr = linkUrls.slice(0, -1);
			var newLink = newArr.join("/");
			DORTHEME.PathColor = newLink;
			if(color != null){
				var linkColor = newLink+"/"+color+".css";
				$('head link[href="'+DOR_COLOR+'"]').attr('href',linkColor);
				jQuery(".cl-td-"+color).addClass("selected");
				jQuery("#"+color).find("span").addClass("selected");
				DOR_COLOR = linkColor;
			}
		}
	},
	RebuiltFont:function(){
		var font = localStorage.getItem("optionFont");
		var linkUrl = DOR_FONT;
		if(typeof linkUrl != "undefined"){
			var linkUrls = linkUrl.split("/");
			var newArr = linkUrls.slice(0, -1);
			var newLink = newArr.join("/");
			DORTHEME.PathFont = newLink;
			if(font != null){
				var linkFont = newLink+"/"+font+".css";
				$('head link[href="'+DOR_FONT+'"]').attr('href',linkFont);
				jQuery("#dor_font_options option[value='"+font+"']").attr("selected","selected");
				DOR_FONT = linkFont;
			}
		}
	},
	RebuiltBackground:function(){
		var bgs = localStorage.getItem("optionBg");
		if(bgs != null){
			for(var i=1;i<=30;i++){
				jQuery("body").removeClass("pattern"+i);
			}
			jQuery("body").addClass(bgs);
			jQuery("#"+bgs).addClass("selected");
		}
	},
	RebuiltMode:function(){
		var mode = localStorage.getItem("optionMode");
		if(mode != null){
			jQuery("#page").removeClass("full").removeClass("boxed");
			jQuery("#page").addClass(mode);
			jQuery(".mode_theme").removeAttr("checked");
			jQuery("input[value='"+mode+"']").prop('checked', true);
		}
	}
}

$(document).ready(function(){
	DORTHEME.init();
});