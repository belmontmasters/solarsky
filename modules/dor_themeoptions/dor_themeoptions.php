<?php

if (!defined('_PS_VERSION_'))
    exit;
if (!defined('_MYSQL_ENGINE_'))
    define('_MYSQL_ENGINE_', 'MyISAM');
class dor_themeoptions extends Module
{
    var $prefix             = '';
    var $amounts            = 4;
    var $base_config_url    = '';
    var $overrideHooks      = array();
    public function __construct()
    {
        global $currentIndex;
        $this->name                     = 'dor_themeoptions';
        $this->tab                      = 'front_office_features';
        $this->version                  = '1.0';
        $this->bootstrap                = true ;
        $this->author                   = 'Dorado Themes';
        $this->need_instance            = 0;
        $this->ps_versions_compliancy   = array('min' => '1.5', 'max' => '1.6');
        $this->currentIndex             = $currentIndex;
        parent::__construct();
        $this->displayName              = $this->l('Dor Theme Options');
        $this->description              = $this->l('Dor Theme Configuration');
        $this->confirmUninstall         = $this->l('Are you sure you want to uninstall?');
        if (!Configuration::get('THEMEOPTIONS'))
            $this->warning = $this->l('No name provided');
    }

   public function install()
    {
		if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
			$parent_tab = new Tab();
			$parent_tab->name[$this->context->language->id] = $this->l('Dor Extensions');
			$parent_tab->class_name = 'AdminDorMenu';
			$parent_tab->id_parent = 0;
			$parent_tab->module = $this->name;
			$parent_tab->add();
		}
        $tab = new Tab();
		foreach (Language::getLanguages() as $language)
        $tab->name[$language['id_lang']] = $this->l('Dor Theme Configuration');
        $tab->class_name = 'Admindorthemeoptions';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
        $tab->module = $this->name;
        $tab->add();
        if (parent::install() && $this->registerHook('dorthemeoptions') && $this->registerHook('displayHeader') && $this->registerHook('displayTop') && $this->registerHook('displayBackOfficeHeader')) {
            $res = Configuration::updateValue('dorHeaderBgOutside','');
            $res &= Configuration::updateValue('dorHeaderBgColor','');
            $res &= Configuration::updateValue('dorHeaderColorLink','');
            $res &= Configuration::updateValue('dorHeaderColorLinkHover','');
            $res &= Configuration::updateValue('dorHeaderColorText','');

            $res &= Configuration::updateValue('dorFooterBgOutside','');
            $res &= Configuration::updateValue('dorTopbarBgOutside','');
            $res &= Configuration::updateValue('dorTimeCache',86400);
            $res &= Configuration::updateValue('dorFooterBgColor','');
            $res &= Configuration::updateValue('dorTopbarBgColor','');
            $res &= Configuration::updateValue('dorFooterColorText','');
            $res &= Configuration::updateValue('dorTopbarColorText','');
            $res &= Configuration::updateValue('dorFooterColorLink','');
            $res &= Configuration::updateValue('dorTopbarColorLink','');
            $res &= Configuration::updateValue('dorFooterColorLinkHover','');
            $res &= Configuration::updateValue('dorTopbarColorLinkHover','');
            $res &= Configuration::updateValue('dorthemecolor','');
            $res &= Configuration::updateValue('dorEnableBgImage','');
            $res &= Configuration::updateValue('dorthemebg','');
            $res &= Configuration::updateValue('dorEnableThemeColor','0');
            $res &= Configuration::updateValue('dorOptReload',1);
            $res &= Configuration::updateValue('dorFloatHeader',1);
            $res &= Configuration::updateValue('dorOptfrontend','0');
            $res &= Configuration::updateValue('dorEnableAwesome','1');
            $res &= Configuration::updateValue('dorHeaderSkin','headerskin1');
            $res &= Configuration::updateValue('dorFooterSkin','footerskin1');
            $res &= Configuration::updateValue('dorTopbarSkin','topbarskin1');
            $res &= Configuration::updateValue('dorCategoryThumb',1);
            $res &= Configuration::updateValue('dorCatQuanlity',100);
            $res &= Configuration::updateValue('dorCatThumbWidth',250);
            $res &= Configuration::updateValue('dorCatThumbHeight',250);
            $res &= Configuration::updateValue('dorlayoutmode','box');
            $res &= Configuration::updateValue('dorDetailThumbList','');
            $res &= Configuration::updateValue('dorDetailInfoStyle','');
            $res &= Configuration::updateValue('dorCategoryEffect',1);
            $res &= Configuration::updateValue('dorBlogsCols','proBlogCol3');
            $res &= Configuration::updateValue('dorBlogsDetailCols','proBlogDetailCol3');
            $res &= Configuration::updateValue('dorSubsPop',1);
            $res &= Configuration::updateValue('enableAngularJs',0);
            $res &= Configuration::updateValue('enableDorCache',0);
            $res &= Configuration::updateValue('dorDetailMainImage','left');
        return (bool)$res;
        }
    }

    public function uninstall()
    {
        Configuration::deleteByName('THEMEOPTIONS');
        $tab = new Tab((int)Tab::getIdFromClassName('Admindorthemeoptions'));
        $tab->delete();
        if (!parent::uninstall())
            return false;
        return true;
    }

    function getContent()
    {
        $errors = array();
        $this->_html = '<h2>' . $this->displayName . '</h2>';
        if (Tools::isSubmit('submitUpdate')) {
            Configuration::updateValue('dorFooterBgOutside',Tools::getValue('dorFooterBgOutside'));
            Configuration::updateValue('dorTopbarBgOutside',Tools::getValue('dorTopbarBgOutside'));
            Configuration::updateValue('dorFooterBgColor',Tools::getValue('dorFooterBgColor'));
            Configuration::updateValue('dorTopbarBgColor',Tools::getValue('dorTopbarBgColor'));
            Configuration::updateValue('dorFooterColorText',Tools::getValue('dorFooterColorText'));
            Configuration::updateValue('dorTopbarColorText',Tools::getValue('dorTopbarColorText'));
            Configuration::updateValue('dorFooterColorLink',Tools::getValue('dorFooterColorLink'));
            Configuration::updateValue('dorTopbarColorLink',Tools::getValue('dorTopbarColorLink'));
            Configuration::updateValue('dorEnableThemeColor',Tools::getValue('dorEnableThemeColor'));
            Configuration::updateValue('dorOptfrontend',Tools::getValue('dorOptfrontend'));
            $this->_html .= $this->displayConfirmation($this->l('Settings updated successfully.'));
        }
        else if (Tools::isSubmit('submitUpdateFont')) {
            Configuration::updateValue('dorFont',Tools::getValue('dorFont'));
            Configuration::updateValue('dorEnableAwesome',Tools::getValue('dorEnableAwesome'));
            $this->_html .= $this->displayConfirmation($this->l('Settings updated successfully.'));
        }
        else if(Tools::isSubmit('submitUpdateheader')){
            Configuration::updateValue('dorHeaderBgOutside',Tools::getValue('dorHeaderBgOutside'));
            Configuration::updateValue('dorHeaderBgColor',Tools::getValue('dorHeaderBgColor'));
            Configuration::updateValue('dorHeaderColorLink',Tools::getValue('dorHeaderColorLink'));
            Configuration::updateValue('dorHeaderColorLinkHover',Tools::getValue('dorHeaderColorLinkHover'));
            Configuration::updateValue('dorHeaderColorText',Tools::getValue('dorHeaderColorText'));
            Configuration::updateValue('dorHeaderSkin',Tools::getValue('dorHeaderSkin'));
            Configuration::updateValue('dorFloatHeader',Tools::getValue('dorFloatHeader'));
            $this->_html .= $this->displayConfirmation($this->l('Settings updated successfully.'));
        }
        else if(Tools::isSubmit('submitUpdateTopbar')){
            Configuration::updateValue('dorTopbarBgOutside',Tools::getValue('dorTopbarBgOutside'));
            Configuration::updateValue('dorTopbarBgColor',Tools::getValue('dorTopbarBgColor'));
            Configuration::updateValue('dorTopbarColorText',Tools::getValue('dorTopbarColorText'));
            Configuration::updateValue('dorTopbarColorLink',Tools::getValue('dorTopbarColorLink'));
            Configuration::updateValue('dorTopbarColorLinkHover',Tools::getValue('dorTopbarColorLinkHover'));
            Configuration::updateValue('dorTopbarSkin',Tools::getValue('dorTopbarSkin'));
            $this->_html .= $this->displayConfirmation($this->l('Settings updated successfully.'));
        }
        else if(Tools::isSubmit('submitUpdateFooter')){
            Configuration::updateValue('dorFooterBgOutside',Tools::getValue('dorFooterBgOutside'));
            Configuration::updateValue('dorFooterBgColor',Tools::getValue('dorFooterBgColor'));
            Configuration::updateValue('dorFooterColorText',Tools::getValue('dorFooterColorText'));
            Configuration::updateValue('dorFooterColorLink',Tools::getValue('dorFooterColorLink'));
            Configuration::updateValue('dorFooterColorLinkHover',Tools::getValue('dorFooterColorLinkHover'));
            Configuration::updateValue('dorFooterSkin',Tools::getValue('dorFooterSkin'));
            $this->_html .= $this->displayConfirmation($this->l('Settings updated successfully.'));
        }
        else if(Tools::isSubmit('submitUpdateDorAdvance')){
            Configuration::updateValue('dorDetailCols',Tools::getValue('dorDetailCols'));
            Configuration::updateValue('dorCategoryCols',Tools::getValue('dorCategoryCols'));
            Configuration::updateValue('detailReview',Tools::getValue('detailReview'));
            Configuration::updateValue('detailLabel',Tools::getValue('detailLabel'));
            Configuration::updateValue('detailReduction',Tools::getValue('detailReduction'));
            Configuration::updateValue('detailOldPrice',Tools::getValue('detailOldPrice'));
            Configuration::updateValue('detailpQuantityAvailable',Tools::getValue('detailpQuantityAvailable'));
            Configuration::updateValue('detailavailability_statut',Tools::getValue('detailavailability_statut'));
            Configuration::updateValue('detailcompare',Tools::getValue('detailcompare'));
            Configuration::updateValue('detailwishlist',Tools::getValue('detailwishlist'));
            Configuration::updateValue('detaillinkblock',Tools::getValue('detaillinkblock'));
            Configuration::updateValue('detailsocialsharing',Tools::getValue('detailsocialsharing'));
            Configuration::updateValue('dorCategoryThumb',Tools::getValue('dorCategoryThumb'));
            Configuration::updateValue('dorCatQuanlity',Tools::getValue('dorCatQuanlity'));
            Configuration::updateValue('dorCatThumbWidth',Tools::getValue('dorCatThumbWidth'));
            Configuration::updateValue('dorCatThumbHeight',Tools::getValue('dorCatThumbHeight'));
            Configuration::updateValue('dorDetailReference',Tools::getValue('dorDetailReference'));
            Configuration::updateValue('dorDetailCondition',Tools::getValue('dorDetailCondition'));
            Configuration::updateValue('dorDetailThumbList',Tools::getValue('dorDetailThumbList'));
            Configuration::updateValue('dorDetailInfoStyle',Tools::getValue('dorDetailInfoStyle'));
            Configuration::updateValue('dorCategoryEffect',Tools::getValue('dorCategoryEffect'));
            Configuration::updateValue('dorBlogsCols',Tools::getValue('dorBlogsCols'));
            Configuration::updateValue('dorBlogsDetailCols',Tools::getValue('dorBlogsDetailCols'));
            Configuration::updateValue('dorSubsPop',Tools::getValue('dorSubsPop'));
            Configuration::updateValue('enableAngularJs',Tools::getValue('enableAngularJs'));
            Configuration::updateValue('enableDorCache',Tools::getValue('enableDorCache'));
            Configuration::updateValue('dorDetailMainImage',Tools::getValue('dorDetailMainImage'));
            Configuration::updateValue('dorTimeCache',Tools::getValue('dorTimeCache'));
            $this->_html .= $this->displayConfirmation($this->l('Settings updated successfully.'));
        }
        else if(Tools::isSubmit('submitUpdateThemeskin')){
            Configuration::updateValue('dorthemebg',Tools::getValue('dorthemebg'));
            Configuration::updateValue('dorthemecolor',Tools::getValue('dorthemecolor'));
            Configuration::updateValue('dorlayoutmode',Tools::getValue('dorlayoutmode'));
            Configuration::updateValue('dorOptfrontend',Tools::getValue('dorOptfrontend'));
            Configuration::updateValue('dorEnableBgImage',Tools::getValue('dorEnableBgImage'));
            Configuration::updateValue('dorEnableThemeColor',Tools::getValue('dorEnableThemeColor'));
            Configuration::updateValue('dorOptReload',Tools::getValue('dorOptReload'));
            $this->_html .= $this->displayConfirmation($this->l('Settings updated successfully.'));
        }else if(Tools::isSubmit('submitDorClearCache')){
            $fullPath = _PS_ROOT_DIR_.'/override/Dor/Caches/smartcaches';
            $paths = glob($fullPath.'/*');
            if(count($paths) > 0){
                foreach ($paths as $key => $path) {
                    if (is_dir($path)) {
                        $files = glob($path.'/*');
                        foreach($files as $file){
                          if(is_file($file))
                            unlink($file);
                        }
                    }
                }
            }
        }
        if (sizeof($errors)) {
            foreach ($errors AS $err) {
                $this->_html .= '<div class="alert error">' . $err . '</div>';
            }
        }
        $this->_html .= $this->renderForm();
        return $this->_html;
    }


    public  function renderForm(){
        $this->context->controller->addJqueryPlugin('colorpicker');
        $action                     = 'index.php?controller=AdminModules&configure='.$this->name.'&tab_module=front_office_features&module_name='.$this->name.'&token='.Tools::getValue('token').' ';
        $dorHeaderBgOutside         = Tools::getValue('dorHeaderBgOutside',Configuration::get('dorHeaderBgOutside'));
        $dorHeaderBgColor           = Tools::getValue('dorHeaderBgColor',Configuration::get('dorHeaderBgColor'));
        $dorHeaderColorLink         = Tools::getValue('dorHeaderColorLink', Configuration::get('dorHeaderColorLink'));
        $dorHeaderColorLinkHover    =  Tools::getValue('dorHeaderColorLinkHover',Configuration::get('dorHeaderColorLinkHover'));
        $dorHeaderColorText         = Tools::getValue('dorHeaderColorText',Configuration::get('dorHeaderColorText'));
        // footer
        $dorthemebg                 = Tools::getValue('dorthemebg',Configuration::get('dorthemebg'));
        $dorlayoutmode              = Tools::getValue('dorlayoutmode',Configuration::get('dorlayoutmode'));
        $dorOptfrontend             = Tools::getValue('dorOptfrontend',Configuration::get('dorOptfrontend'));
        $dorEnableThemeColor        = Tools::getValue('dorEnableThemeColor',Configuration::get('dorEnableThemeColor'));
        $dorOptReload               = Tools::getValue('dorOptReload',Configuration::get('dorOptReload'));
        $dorEnableAwesome           = Tools::getValue('dorEnableAwesome',Configuration::get('dorEnableAwesome'));
        $dorEnableBgImage           = Tools::getValue('dorEnableBgImage',Configuration::get('dorEnableBgImage'));
        $dorthemecolor              = Tools::getValue('dorthemecolor',Configuration::get('dorthemecolor'));
        $dorFooterBgOutside         = Tools::getValue('dorFooterBgOutside',Configuration::get('dorFooterBgOutside'));
        $dorTopbarBgOutside         = Tools::getValue('dorTopbarBgOutside',Configuration::get('dorTopbarBgOutside'));
        $dorFooterBgColor           = Tools::getValue('dorFooterBgColor',Configuration::get('dorFooterBgColor'));
        $dorTopbarBgColor           = Tools::getValue('dorTopbarBgColor',Configuration::get('dorTopbarBgColor'));
        $dorFooterColorText         = Tools::getValue('dorFooterColorText',Configuration::get('dorFooterColorText'));
        $dorTopbarColorText         = Tools::getValue('dorTopbarColorText',Configuration::get('dorTopbarColorText'));
        $dorFooterColorLink         = Tools::getValue('dorFooterColorLink',Configuration::get('dorFooterColorLink'));
        $dorTopbarColorLink         = Tools::getValue('dorTopbarColorLink',Configuration::get('dorTopbarColorLink'));
        $dorFooterColorLinkHover    = Tools::getValue('dorFooterColorLinkHover',Configuration::get('dorFooterColorLinkHover'));
        $dorTopbarColorLinkHover    = Tools::getValue('dorTopbarColorLinkHover',Configuration::get('dorTopbarColorLinkHover'));
        // Get font data
        $dorFont                    = Tools::getValue('dorFont',Configuration::get('dorFont'));
        // Get skin header
        $dorHeaderSkin              = Tools::getValue('dorHeaderSkin',Configuration::get('dorHeaderSkin'));
        $dorFloatHeader              = Tools::getValue('dorFloatHeader',Configuration::get('dorFloatHeader'));
        // Get skin footer
        $dorFooterSkin              = Tools::getValue('dorFooterSkin',Configuration::get('dorFooterSkin'));
        $dorDetailCols              = Tools::getValue('dorDetailCols',Configuration::get('dorDetailCols'));
        $dorCategoryCols              = Tools::getValue('dorCategoryCols',Configuration::get('dorCategoryCols'));
        $dorTopbarSkin              = Tools::getValue('dorTopbarSkin',Configuration::get('dorTopbarSkin'));
        $dorCategoryThumb              = Tools::getValue('dorCategoryThumb',Configuration::get('dorCategoryThumb'));
        $dorCatQuanlity              = Tools::getValue('dorCatQuanlity',Configuration::get('dorCatQuanlity'));
        $dorCatThumbWidth              = Tools::getValue('dorCatThumbWidth',Configuration::get('dorCatThumbWidth'));
        $dorCatThumbHeight              = Tools::getValue('dorCatThumbHeight',Configuration::get('dorCatThumbHeight'));
        $dorDetailLabel              = Tools::getValue('detailLabel',Configuration::get('detailLabel'));
        $dorDetailReview            = Tools::getValue('detailReview',Configuration::get('detailReview'));
        $dorDetailOldPrice            = Tools::getValue('detailOldPrice',Configuration::get('detailOldPrice'));
        $dorDetailReduction           = Tools::getValue('detailReduction',Configuration::get('detailReduction'));
        $dorDetailpQuantityAvailable           = Tools::getValue('detailpQuantityAvailable',Configuration::get('detailpQuantityAvailable'));
        $dorDetailavailabilityStatut           = Tools::getValue('detailavailability_statut',Configuration::get('detailavailability_statut'));
        $dorDetailcompare           = Tools::getValue('detailcompare',Configuration::get('detailcompare'));
        $dorDetailwishlist           = Tools::getValue('detailwishlist',Configuration::get('detailwishlist'));
        $dorDetaillinkblock           = Tools::getValue('detaillinkblock',Configuration::get('detaillinkblock'));
        $dorDetailsocialsharing           = Tools::getValue('detailsocialsharing',Configuration::get('detailsocialsharing'));
        $dorDetailReference           = Tools::getValue('dorDetailReference',Configuration::get('dorDetailReference'));
        $dorDetailCondition           = Tools::getValue('dorDetailCondition',Configuration::get('dorDetailCondition'));
        $dorDetailThumbList           = Tools::getValue('dorDetailThumbList',Configuration::get('dorDetailThumbList'));
        $dorDetailInfoStyle           = Tools::getValue('dorDetailInfoStyle',Configuration::get('dorDetailInfoStyle'));
        $dorCategoryEffect           = Tools::getValue('dorCategoryEffect',Configuration::get('dorCategoryEffect'));
        $dorBlogsCols               = Tools::getValue('dorBlogsCols',Configuration::get('dorBlogsCols'));
        $dorBlogsDetailCols               = Tools::getValue('dorBlogsDetailCols',Configuration::get('dorBlogsDetailCols'));
        $dorSubsPop               = Tools::getValue('dorSubsPop',Configuration::get('dorSubsPop'));
        $enableAngularJs               = Tools::getValue('enableAngularJs',Configuration::get('enableAngularJs'));
        $enableDorCache               = Tools::getValue('enableDorCache',Configuration::get('enableDorCache'));
        $dorDetailMainImage           = Tools::getValue('dorDetailMainImage',Configuration::get('dorDetailMainImage'));
        $dorTimeCache           = Tools::getValue('dorTimeCache',Configuration::get('dorTimeCache'));

        $this->smarty->assign(array(
                'dorHeaderBgOutside'            =>$dorHeaderBgOutside, // header
                'dorHeaderBgColor'              =>$dorHeaderBgColor,
                'dorHeaderColorLink'            =>$dorHeaderColorLink,
                'dorHeaderColorLinkHover'       =>$dorHeaderColorLinkHover,
                'dorHeaderColorText'            =>$dorHeaderColorText,
                //footer
                'dorFooterBgOutside'            => $dorFooterBgOutside,
                'dorTopbarBgOutside'            => $dorTopbarBgOutside,
                'dorFooterBgColor'              => $dorFooterBgColor,
                'dorTopbarBgColor'              => $dorTopbarBgColor,
                'dorFooterColorText'            =>$dorFooterColorText ,
                'dorTopbarColorText'            =>$dorTopbarColorText ,
                'dorFooterColorLink'            =>$dorFooterColorLink ,
                'dorTopbarColorLink'            =>$dorTopbarColorLink ,
                'dorFooterColorLinkHover'       =>$dorFooterColorLinkHover ,
                'dorTopbarColorLinkHover'       =>$dorTopbarColorLinkHover ,
                //skin
                'dorthemebg'                    => $dorthemebg,
                'dorlayoutmode'                 => $dorlayoutmode,
                'dorthemecolor'                 => $dorthemecolor,
                'dorEnableBgImage'              => $dorEnableBgImage,
                'dorEnableThemeColor'           => $dorEnableThemeColor,
                'dorOptReload'                  => $dorOptReload,
                'dorEnableAwesome'              => $dorEnableAwesome,
                'dorOptfrontend'                => $dorOptfrontend,
                // Font
                'dorFont'                       => $dorFont,
                // Skin Footer
                'dorFooterSkin'                 => $dorFooterSkin,
                'dorDetailCols'                 => $dorDetailCols,
                'dorCategoryCols'               => $dorCategoryCols,
                'dorDetailReview'               => $dorDetailReview,
                'dorDetailLabel'                => $dorDetailLabel,
                'dorDetailReduction'                => $dorDetailReduction,
                'dorDetailOldPrice'                => $dorDetailOldPrice,
                'dorDetailReference'                => $dorDetailReference,
                'dorDetailCondition'                => $dorDetailCondition,
                'dorDetailThumbList'                => $dorDetailThumbList,
                'dorDetailInfoStyle'                => $dorDetailInfoStyle,
                'dorCategoryEffect'                => $dorCategoryEffect,
                'dorBlogsCols'                => $dorBlogsCols,
                'dorBlogsDetailCols'                => $dorBlogsDetailCols,
                'dorSubsPop'                => $dorSubsPop,
                'enableAngularJs'                => $enableAngularJs,
                'enableDorCache'                => $enableDorCache,
                'dorDetailMainImage'                => $dorDetailMainImage,
                'dorTimeCache'                => $dorTimeCache,
                'dorDetailpQuantityAvailable'                => $dorDetailpQuantityAvailable,
                'dorDetailavailabilityStatut'                => $dorDetailavailabilityStatut,
                'dorDetailcompare'                => $dorDetailcompare,
                'dorDetailwishlist'                => $dorDetailwishlist,
                'dorDetaillinkblock'                => $dorDetaillinkblock,
                'dorDetailsocialsharing'                => $dorDetailsocialsharing,
                'dorTopbarSkin'                 => $dorTopbarSkin,
                'dorCategoryThumb'                 => $dorCategoryThumb,
                'dorCatQuanlity'                 => $dorCatQuanlity,
                'dorCatThumbWidth'                 => $dorCatThumbWidth,
                'dorCatThumbHeight'                 => $dorCatThumbHeight,
                // Skin Header
                'dorHeaderSkin'                 => $dorHeaderSkin,
                'dorFloatHeader'                 => $dorFloatHeader,
                'action'                        => $action,

                'search_query' => (string)Tools::getValue('search_query')
            )
        );
		return $this->display(__FILE__, 'views/templates/admin/adminform.tpl');
    }
    public function hookDisplayBackOfficeHeader($params) {
        $this->context->controller->addCSS($this->_path . 'css/admin.doradotheme.css');
        $this->context->controller->addCSS($this->_path . 'font/css/font-awesome.min.css');
        if (method_exists($this->context->controller, 'addJquery'))
        {        
            $this->context->controller->addJquery();
            $this->context->controller->addJS(($this->_path).'js/admin.doradotheme.js');
        }
        return $this->display(__FILE__, 'views/templates/admin/fortawesome.tpl');
    }
    function hookdisplayHeader()
    {
       $this->context->controller->addJS($this->_path. "js/dorthemes.js");
       $this->context->controller->addJS($this->_path. "js/jquery.tinycolorpicker.min.js");
       $this->context->controller->addJS($this->_path. "js/owl.carousel.min.js");
       $this->context->controller->addJS($this->_path. "plugins/bootstrap-select-1.9.3/js/bootstrap-select.js");
       $this->context->controller->addJS($this->_path. "plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js");
       $this->context->controller->addCSS($this->_path. "plugins/bootstrap-select-1.9.3/dist/css/bootstrap-select.min.css");
	   $this->context->controller->addCSS($this->_path. "plugins/scrollbar/jquery.mCustomScrollbar.min.css");
       $this->context->controller->addCSS($this->_path . 'font/css/font-awesome.min.css');
       $this->context->controller->addCSS($this->_path. 'css/dorthemes.css' );
	   $this->context->controller->addCSS($this->_path. 'css/owl.carousel.css' );
       $dorFloatHeader              = Tools::getValue('dorFloatHeader',Configuration::get('dorFloatHeader'));
       $dorHeaderSkin               = Tools::getValue('dorHeaderSkin',Configuration::get('dorHeaderSkin'));
       $dorFooterSkin               = Tools::getValue('dorFooterSkin',Configuration::get('dorFooterSkin'));
       $dorFont                     = Tools::getValue('dorFont',Configuration::get('dorFont'));
       $dorthemecolor               = Tools::getValue('dorthemecolor',Configuration::get('dorthemecolor'));
       $dorEnableThemeColor         = Tools::getValue('dorEnableThemeColor',Configuration::get('dorEnableThemeColor'));
       $dorOptReload                = Tools::getValue('dorOptReload',Configuration::get('dorOptReload'));
       $dorTopbarSkin               = Tools::getValue('dorTopbarSkin',Configuration::get('dorTopbarSkin'));
       $dorOptfrontend              = Configuration::get('dorOptfrontend');

       if($dorHeaderSkin != ""){
            $this->context->controller->addCSS($this->_path. 'css/header/'.$dorHeaderSkin.'.css' );
       }
       if($dorFooterSkin != ""){
            $this->context->controller->addCSS($this->_path. 'css/footer/'.$dorFooterSkin.'.css' );
       }
       if($dorTopbarSkin != ""){
            $this->context->controller->addCSS($this->_path. 'css/topbar/'.$dorTopbarSkin.'.css' );
       }
       $dorFont = isset($dorFont) && $dorFont != "" ? $dorFont : "font";
       $font_path = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/css/modules/'.$this->name.'/fonts/'.$dorFont.'.css';
       if (!file_exists($font_path)){
            if($dorFont != ""){
                $this->context->controller->addCSS($this->_path. 'css/fonts/'.$dorFont.'.css' );
           }else{
                $this->context->controller->addCSS($this->_path. 'css/fonts/font.css' );
           }
       }
       $dorthemecolor = isset($dorthemecolor) && $dorthemecolor != "" ? $dorthemecolor : "color";
       $css_path = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/css/modules/'.$this->name.'/color/'.$dorthemecolor.'.css';
       if (!file_exists($css_path)){
            if($dorthemecolor != "" && $dorEnableThemeColor == 1){
                $this->context->controller->addCSS($this->_path. 'css/color/'.$dorthemecolor.'.css' );
            }else{
                $this->context->controller->addCSS($this->_path. 'css/color/color.css' );
            }
       }
       
       if($dorOptfrontend == 1){
            $this->context->controller->addCSS($this->_path. 'css/dor-tool.css' );
       }
	   $this->display(__FILE__, 'views/templates/admin/fortawesome.tpl');
	   
		global  $smarty;
        //header
        $dorHeaderBgOutside                 = Configuration::get('dorHeaderBgOutside');
        $dorHeaderBgColor                   = Configuration::get('dorHeaderBgColor');
        $dorHeaderColorLink                 = Configuration::get('dorHeaderColorLink');
        $dorHeaderColorLinkHover            = Configuration::get('dorHeaderColorLinkHover');
        $dorHeaderColorText                 = Configuration::get('dorHeaderColorText');
        // footer
        $dorthemebg                         = Configuration::get('dorthemebg');
        
        $dorEnableThemeColor                = Configuration::get('dorEnableThemeColor');
        $dorOptReload                       = Configuration::get('dorOptReload');
        $dorEnableAwesome                   = Configuration::get('dorEnableAwesome');
        $dorEnableBgImage                   = Configuration::get('dorEnableBgImage');
        $dorthemecolor                      = Configuration::get('dorthemecolor');
        $dorFooterBgOutside                 = Configuration::get('dorFooterBgOutside');
        $dorTopbarBgOutside                 = Configuration::get('dorTopbarBgOutside');
        $dorFooterBgColor                   = Configuration::get('dorFooterBgColor');
        $dorTopbarBgColor                   = Configuration::get('dorTopbarBgColor');
        $dorFooterColorText                 = Configuration::get('dorFooterColorText');
        $dorTopbarColorText                 = Configuration::get('dorTopbarColorText');
        $dorFooterColorLink                 = Configuration::get('dorFooterColorLink');
        $dorTopbarColorLink                 = Configuration::get('dorTopbarColorLink');
        $dorFooterColorLinkHover            = Configuration::get('dorFooterColorLinkHover');
        $dorTopbarColorLinkHover            = Configuration::get('dorTopbarColorLinkHover');
        $dorlayoutmode                      = Configuration::get('dorlayoutmode');
        $dorFont                            = Configuration::get('dorFont');
        $dorFloatHeader                     = Configuration::get('dorFloatHeader');
        $dorHeaderSkin                      = Configuration::get('dorHeaderSkin');
        $dorFooterSkin                      = Configuration::get('dorFooterSkin');
        $dorDetailCols                      = Configuration::get('dorDetailCols');
        $dorCategoryCols                    = Configuration::get('dorCategoryCols');
        $dorDetailReview                    = Configuration::get('detailReview');
        $dorDetailLabel                     = Configuration::get('detailLabel');
        $dorDetailOldPrice                  = Configuration::get('detailOldPrice');
        $dorDetailReference                  = Configuration::get('dorDetailReference');
        $dorDetailCondition                  = Configuration::get('dorDetailCondition');
        $dorDetailThumbList                  = Configuration::get('dorDetailThumbList');
        $dorDetailInfoStyle                  = Configuration::get('dorDetailInfoStyle');
        $dorCategoryEffect                  = Configuration::get('dorCategoryEffect');
        $dorBlogsCols                       = Configuration::get('dorBlogsCols') != ""?Configuration::get('dorBlogsCols'):"proBlogCol3";
        $dorBlogsDetailCols                  = Configuration::get('dorBlogsDetailCols') != ""?Configuration::get('dorBlogsDetailCols'):"proBlogDetailCol3";
        $dorDetailMainImage                  = Configuration::get('dorDetailMainImage');
        $dorTimeCache                  = Configuration::get('dorTimeCache');
        $dorSubsPop                  = Configuration::get('dorSubsPop');
        $enableAngularJs                  = Configuration::get('enableAngularJs');
        $enableDorCache                  = Configuration::get('enableDorCache');
        $dorDetailReduction                 = Configuration::get('detailReduction');
        $dorDetailpQuantityAvailable        = Configuration::get('detailpQuantityAvailable');
        $dorDetailavailabilityStatut       = Configuration::get('detailavailability_statut');
        $dorDetailcompare                   = Configuration::get('detailcompare');
        $dorDetailwishlist                  = Configuration::get('detailwishlist');
        $dorDetaillinkblock                 = Configuration::get('detaillinkblock');
        $dorDetailsocialsharing             = Configuration::get('detailsocialsharing');
        $dorTopbarSkin                      = Configuration::get('dorTopbarSkin');
        $dorCategoryThumb                      = Configuration::get('dorCategoryThumb');
        $dorCatQuanlity                      = Configuration::get('dorCatQuanlity');
        $dorCatThumbWidth                      = Configuration::get('dorCatThumbWidth');
        $dorCatThumbHeight                      = Configuration::get('dorCatThumbHeight');
        if($dorthemecolor == ""){$dorthemecolor="color";}
        if($dorFont == ""){$dorFont="font";}
        if (!defined('TIME_CACHE_HOME'))
            define('TIME_CACHE_HOME', $dorTimeCache);
        $headerSkin = dirname(__FILE__).'/options/header/'.$dorHeaderSkin.'.tpl';
        $tpheaderSkin = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/'.$this->name.'/options/header/'.$dorHeaderSkin.'.tpl';
        if (file_exists($tpheaderSkin))
            $headerSkin = $tpheaderSkin;

        $footerSkin = dirname(__FILE__).'/options/footer/'.$dorFooterSkin.'.tpl';
        $tpfooterSkin = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/'.$this->name.'/options/footer/'.$dorFooterSkin.'.tpl';
        if($dorOptfrontend) $this->context->controller->addCSS($this->_path. 'css/dor-tool.css');

        if (file_exists($tpfooterSkin))
            $footerSkin = $tpfooterSkin;

        $id_product = (int)Tools::getValue('id_product');
        $tagProducts = array();
        if($id_product > 0){
            $tags = Tag::getMainTags((int)($this->context->language->id), 3);
            if(count($tags) > 0){
                $max = -1;
                $min = -1;
                foreach ($tags as $tag)
                {
                    if ($tag['times'] > $max)
                        $max = $tag['times'];
                    if ($tag['times'] < $min || $min == -1)
                        $min = $tag['times'];
                }

                if ($min == $max)
                    $coef = $max;
                else
                    $coef = (Configuration::get('BLOCKTAGS_MAX_LEVEL') - 1) / ($max - $min);

                if (!count($tags))
                    return false;
                if (Configuration::get('BLOCKTAGS_RANDOMIZE'))
                    shuffle($tags);
                foreach ($tags as &$tag){
                    $tag['class'] = 'tag_level'.(int)(($tag['times'] - $min) * $coef + 1);
                }
                $tagProducts = $tags;
            }
        }

          $ps = array(
              'DORTHEMENAME'                => _THEME_NAME_,
              'PS_BASE_URL'                 => _PS_BASE_URL_,
              'PS_BASE_URI'                 => __PS_BASE_URI__,
              'PS_BASE_URL'                 => _PS_BASE_URL_,
              //start color
              'dorHeaderBgOutside'          => $dorHeaderBgOutside, // header
              'dorHeaderBgColor'            => $dorHeaderBgColor,
              'dorHeaderColorLink'          => $dorHeaderColorLink,
              'dorHeaderColorLinkHover'     => $dorHeaderColorLinkHover,
              'dorHeaderColorText'          => $dorHeaderColorText,
              //footer
              'dorFooterBgOutside'          => $dorFooterBgOutside,
              'dorTopbarBgOutside'          => $dorTopbarBgOutside,
              'dorFooterBgColor'            => $dorFooterBgColor,
              'dorTopbarBgColor'            => $dorTopbarBgColor,
              'dorFooterColorText'          => $dorFooterColorText ,
              'dorTopbarColorText'          => $dorTopbarColorText ,
              'dorFooterColorLink'          => $dorFooterColorLink ,
              'dorTopbarColorLink'          => $dorTopbarColorLink ,
              'dorFooterColorLinkHover'     => $dorFooterColorLinkHover ,
              'dorTopbarColorLinkHover'     => $dorTopbarColorLinkHover ,
              // end color
              'dorEnableThemeColor'         => (int)$dorEnableThemeColor, //show color
              'dorOptReload'                => (int)$dorOptReload, //show color
              'dorEnableAwesome'            => (int)$dorEnableAwesome, //show color
              'dorEnableBgImage'            => $dorEnableBgImage, // color skin
              'dorthemecolor'               => $dorthemecolor, // color skin
              'dorPathColor'                => $this->_path. 'css/color/'.$dorthemecolor.'.css',
              'dorlayoutmode'               => $dorlayoutmode, // mode theme
              'dorFloatHeader'              => $dorFloatHeader, // mode theme
              'dorHeaderSkinName'           => $dorHeaderSkin, // mode theme
              'dorHeaderSkin'               => $dorHeaderSkin != ""?$headerSkin:"", // Skin Header
              'dorFont'                     => $dorFont, // Font Theme
              'dorPathFont'                => $this->_path. 'css/fonts/'.$dorFont.'.css',
              'dorFooterSkinName'           => $dorFooterSkin,
              'dorTopbarSkinName'           => $dorTopbarSkin,
              'dorCategoryThumb'            => $dorCategoryThumb,
              'dorCatQuanlity'            => $dorCatQuanlity,
              'dorCatThumbWidth'            => $dorCatThumbWidth,
              'dorCatThumbHeight'           => $dorCatThumbHeight,
              'dorFooterSkin'               => $dorFooterSkin != ""?$footerSkin:"", // Skin Footer
              'dorDetailCols'               => $dorDetailCols, // Skin Product Detail
              'dorCategoryCols'             => $dorCategoryCols, // Skin Category Lists
              'dorDetailReview'             => $dorDetailReview,
              'dorDetailLabel'              => $dorDetailLabel,
              'dorDetailavailabilityStatut'              => $dorDetailavailabilityStatut,
              'dorDetailcompare'              => $dorDetailcompare,
              'dorDetailwishlist'              => $dorDetailwishlist,
              'dorDetaillinkblock'              => $dorDetaillinkblock,
              'dorDetailsocialsharing'              => $dorDetailsocialsharing,
              'dorDetailOldPrice'              => $dorDetailOldPrice,
              'dorDetailReference'              => $dorDetailReference,
              'dorDetailCondition'              => $dorDetailCondition,
              'dorDetailThumbList'              => $dorDetailThumbList,
              'dorDetailInfoStyle'              => $dorDetailInfoStyle,
              'dorCategoryEffect'              => $dorCategoryEffect,
              'dorBlogsCols'              => $dorBlogsCols,
              'dorBlogsDetailCols'              => $dorBlogsDetailCols,
              'dorSubsPop'              => $dorSubsPop,
              'enableAngularJs'              => $enableAngularJs,
              'enableDorCache'              => $enableDorCache,
              'dorDetailMainImage'              => $dorDetailMainImage,
              'dorTimeCache'              => $dorTimeCache,
              'dorDetailReduction'          => $dorDetailReduction,
              'dorDetailpQuantityAvailable'          => $dorDetailpQuantityAvailable,
              'dorTopbarSkin'               => $dorTopbarSkin != ""?_PS_ROOT_DIR_."/modules/".$this->name."/options/topbar/".$dorTopbarSkin.".tpl":"", // Skin Topbar
              'dorthemebg'                  => Configuration::get('dorthemebg'), // bachground skin
              'tagProducts'                        => $tagProducts,
          );
        $smarty->assign($ps);
        return $this->display(__FILE__, 'dor_themeoptions.tpl');
    }
	function hookdorthemeoptions($params) {
        $dorOptfrontend                     = Configuration::get('dorOptfrontend');
        if($dorOptfrontend){
            global  $smarty;
            $ps = array(
                'dorthemebg'        => Configuration::get('dorthemebg'), // name skin
                'dorOptfrontend'    => Configuration::get('dorOptfrontend'), // ennable /disable name skin show_fontend
                'this_path'         => $this->_path,
                'DORTHEMENAME'      => _THEME_NAME_,
                'PS_BASE_URL'       => _PS_BASE_URL_,
                'PS_BASE_URI'       => __PS_BASE_URI__,
                'dorHeaderBgOutside'=> Configuration::get('dorHeaderBgOutside'),
                'PS_BASE_URL'       => _PS_BASE_URL_,
            );
            $smarty->assign($ps);
            return $this->display(__FILE__, 'dortool.tpl');
        }
    }
	/*function hookdisplayTop ($params){
        global  $smarty;
        $ps = array(
            'dorthemebg'        => Configuration::get('dorthemebg'), // name skin
            'dorOptfrontend'    => Configuration::get('dorOptfrontend'), // ennable /disable name skin show_fontend
            'this_path'         => $this->_path,
            'DORTHEMENAME'      => _THEME_NAME_,
            'PS_BASE_URL'       => _PS_BASE_URL_,
            'PS_BASE_URI'       => __PS_BASE_URI__,
            'dorHeaderBgOutside'=> Configuration::get('dorHeaderBgOutside'),
            'PS_BASE_URL'       => _PS_BASE_URL_,
        );
        $smarty->assign($ps);
        return $this->display(__FILE__, 'dortool.tpl');
    }*/
}
