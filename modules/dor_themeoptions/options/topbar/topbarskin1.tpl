<div class="nav" id="topbarDorado1">
	<div class="container">
		{capture name='topbarDorado1'}{hook h='topbarDorado1'}{/capture}
		{if $smarty.capture.topbarDorado1}
		<div class="pull-left list-unstyled list-inline">{$smarty.capture.topbarDorado1}</div>
		{/if}
		{capture name='topbarDorado2'}{hook h='topbarDorado2'}{/capture}
		{if $smarty.capture.topbarDorado2}
		<div class="pull-right list-unstyled list-inline">{$smarty.capture.topbarDorado2}</div>
		{/if}
		<a href="#" class="dor-top-link-mobile"><i aria-hidden="true" class="fa fa-cog"></i></a>
	</div>
</div>
