<?php

class AdminDorManagerFooterController extends ModuleAdminController {
    protected $id_banner;
    public function __construct() {
        $this->table = 'dor_blockfooter';
        $this->className = 'Managerblockfooter';
        $this->identifier = 'id_dor_blockfooter';
	    $this->bootstrap = true;
        $this->lang = true;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
        Shop::addTableAssociation($this->table, array('type' => 'shop'));
        $this->context = Context::getContext();

        parent::__construct();
    }

    

    public function renderList() {
         
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?')
            )
        );

        $this->fields_list = array(
            'id_dor_blockfooter' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'width' => 25,
                'lang' => false
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'width' => 90,
                'lang' => false
            ),
            'identify' => array(
                'title' => $this->l('Identify'),
                'width' => '100',
                'lang' => false
            ),
            'hook_position' => array(
                'title' => $this->l('Hook Position'),
                'width' => '300',
                'lang' => false
            ),
            'order' => array(
                'title' => $this->l('Order Position'),
                'width' => '30',
                'lang' => false
            )
        );
        $lists = parent::renderList();
        parent::initToolbar();

        return $lists;
    }
    
  

    public function renderForm() {
        
        $mod = new Dor_managerblockfooter();
        $listModules = $mod->getListModuleInstalled();
        
        $listHookModules = array(
            array('hook_position'=>'displayFooter'),
            array('hook_position'=>'blockDoradoFooter'),
            array('hook_position'=>'doradoFooterTop'),
            array('hook_position'=>'doradoFooterAdv'),
			array('hook_position'=>'doradoFooter1'),
			array('hook_position'=>'doradoFooter2'),
			array('hook_position'=>'doradoFooter3'),
            array('hook_position'=>'doradoFooter4'),
            array('hook_position'=>'doradoFooter5'),
            array('hook_position'=>'doradoFooter6'),
            array('hook_position'=>'doradoFooter7'),
            array('hook_position'=>'doradoFooter8'),
            array('hook_position'=>'doradoFooter9'),
			array('hook_position'=>'doradoFooter10')
        );
        
	      
        
        $listHookFooterModules = array(
            array('hook_position'=>'displayFooter'),
        );
        
        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Config Data'),
                'image' => '../img/admin/cog.gif'
            ),
            
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title:'),
                    'name' => 'title',
                    'size' => 40,
                    'lang' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Identify:'),
                    'name' => 'identify',
                    'size' => 40,
                    'require' => false
                ),
                array(
                  'type'      => 'radio',                              
                  'label'     => $this->l('Show/hide title'),       
                  'desc'      => $this->l('Show/hide title?'),  
                  'name'      => 'active',                             
                  'required'  => true,                                 
                  'class'     => 't',                                  
                  'is_bool'   => true,                                 
                  'values'    => array(                                
                        array(
                          'id'    => 'active_on',                          
                          'value' => 1,                                    
                          'label' => $this->l('Enabled')                   
                        ),
                        array(
                          'id'    => 'active_off',
                          'value' => 0,
                          'label' => $this->l('Disabled')
                        )
                  ),
                ),
               array(
                'type' => 'select',
                'label' => $this->l('Hook Position:'),
                'name' => 'hook_position',
                'required' => true,
                'options' => array(
                    'query' => $listHookModules,
                    'id' => 'hook_position',
                    'name' => 'hook_position'
                ),
             
                'desc' => $this->l('Choose the type of the Hooks <br> With new hooks as doradoFooter1, doradoFooter2 , doradoFooter3... must been  insert at tpl file. For example {hook h = "newHook"}')
            ),
            
           array(
				  'type'      => 'radio',                              
				  'label'     => $this->l('Show/hide Hook'),       
				  'desc'      => $this->l('Show/hide Hook?'),  
				  'name'      => 'showhook',                             
				  'required'  => true,                                 
				  'class'     => 't',                                  
				  'is_bool'   => true,                                 
				  'values'    => array(                                
						array(
						  'id'    => 'active_on',                          
						  'value' => 1,                                    
						  'label' => $this->l('Enabled')                   
						),
						array(
						  'id'    => 'active_off',
						  'value' => 0,
						  'label' => $this->l('Disabled')
						)
				  ),
				),
			    array(
                    'type' => 'textarea',
                    'label' => $this->l('Description'),
                    'name' => 'description',
                    'autoload_rte' => TRUE,
                    'lang' => true,
                    'required' => TRUE,
                    'rows' => 5,
                    'cols' => 40,
                    'hint' => $this->l('Invalid characters:') . ' <>;=#{}'
                ),
                
                array(
                    'type' => 'text',
                    'label' => $this->l('Order Position:'),
                    'name' => 'order',
                    'size' => 40,
                    'require' => false
                ),
                 array(
                    'type' => 'radio',
                    'label' => $this->l('Insert Module?'),
                    'desc' => $this->l('Insert module?'),
                    'name' => 'insert_module',
                    'required' => true,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on_module',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off_module',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                'type' => 'select',
                'label' => $this->l('Modules:'),
                'name' => 'name_module',
                'required' => true,
                'options' => array(
                    'query' => $listModules,
                    'id' => 'name',
                    'name' => 'name'
                ),
                    'desc' => $this->l('Choose the type of the Module')
               ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Hook-Modules:'),
                    'name' => 'hook_module',
                    'required' => true,
                    'options' => array(
                        'query' => $listHookFooterModules,
                        'id' => 'hook_position',
                        'name' => 'hook_position'
                    ),
                    'desc' => $this->l('Choose the type of the Hooks')
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );

        if (Shop::isFeatureActive()) {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association:'),
                'name' => 'checkBoxShopAsso',
            );
        }

        if (!($obj = $this->loadObject(true)))
            return;

        return parent::renderForm();
    }
    
}
