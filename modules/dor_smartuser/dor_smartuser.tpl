{if !$logged}
<div id="loginFormSmart">
	<span class="button b-close"><span>X</span></span>
	<form class="box" id="login_form" method="post" action="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}login">
		<h3 class="page-subheading">Already registered?</h3>
		<div class="form_content clearfix">
			<div class="form-group">
				<label for="email">Email address</label>
				<input type="email" value="" name="email" id="email_smart" data-validate="isEmail" class="is_required validate account_input form-control">
			</div>
			<div class="form-group">
				<label for="passwd">Password</label>
				<input type="password" value="" name="passwd" id="passwd_smart" data-validate="isPasswd" class="is_required validate account_input form-control">
			</div>
			<p class="lost_password_smart form-group"><a rel="nofollow" title="Recover your forgotten password" onclick="return false" href="#">Forgot your password?</a></p>
			<p class="submit">
				<input type="hidden" value="my-account" name="back" class="hidden">						<button class="button btn btn-default button-medium dor-effect-hzt" name="SubmitLogin" id="SubmitLoginCus" type="submit">
					<span>
						<i class="icon-lock left"></i>
						Sign in
					</span>
				</button>
				<a href="#" onclick="return false" class="smartRegister reActLogReg">Register</a>
			</p>
			
		</div>
	</form>
</div>

<div id="registerFormSmart">
	<span class="button b-close"><span>X</span></span>
	<form class="std box" id="account-creation_form" method="post" action="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}login">
		<div class="account_creation">
			<h3 class="page-subheading">Your personal information</h3>
			<div class="clearfix">
				<label>Gender</label>
				<br>
					<div class="radio-inline">
						<label class="top" for="id_gender1">
							<input type="radio" value="1" id="id_gender1" name="id_gender">Mr.
						</label>
					</div>
					<div class="radio-inline">
						<label class="top" for="id_gender2">
							<input type="radio" value="2" id="id_gender2" name="id_gender">Mrs.
						</label>
					</div>
			</div>
			<div class="required form-group">
				<label for="customer_firstname">First name <sup>*</sup></label>
				<input type="text" value="" name="customer_firstname" id="customer_firstname" data-validate="isName" class="is_required validate form-control" onkeyup="$('#firstname').val(this.value);">
			</div>
			<div class="required form-group">
				<label for="customer_lastname">Last name <sup>*</sup></label>
				<input type="text" value="" name="customer_lastname" id="customer_lastname" data-validate="isName" class="is_required validate form-control" onkeyup="$('#lastname').val(this.value);">
			</div>
			<div class="required form-group">
				<label for="email">Email <sup>*</sup></label>
				<input type="email" value="" name="email" id="email_register_smart" data-validate="isEmail" class="is_required validate form-control">
			</div>
			<div class="required password form-group">
				<label for="passwd">Password <sup>*</sup></label>
				<input type="password" id="passwd" name="passwd" data-validate="isPasswd" class="is_required validate form-control">
				<span class="form_info">(Five characters minimum)</span>
			</div>
			
			<div class="checkbox">
				<input type="checkbox" value="1" id="newsletter" name="newsletter">
				<label for="newsletter">Sign up for our newsletter!</label>
			</div>
			<div class="checkbox">
					<input type="checkbox" value="1" id="optin" name="optin">
					<label for="optin">Receive special offers from our partners!</label>
			</div>
		</div>
						
		<div class="submit clearfix">
			<input type="hidden" value="0" name="email_create">
			<input type="hidden" value="1" name="is_new_customer">
			<input type="hidden" value="my-account" name="back" class="hidden">			
			<button class="btn btn-default button button-medium dor-effect-hzt" id="submitAccount" name="submitAccount" type="submit">
				<span>Register<i class="icon-chevron-right right"></i></span>
			</button>
			<a href="#" onclick="return false" class="smartLogin reActLogReg">Login</a>
		</div>
	</form>
</div>
<div id="smartForgotPass">
	<span class="button b-close"><span>X</span></span>
	<div class="center_column" id="center_column_smart">
<div class="box">
<h1 class="page-subheading">Forgot your password?</h1>

<p>Please enter the email address you used to register. We will then send you a new password. </p>
<form id="form_forgotpassword" class="std" method="post" action="/en/password-recovery">
	<fieldset>
		<div class="form-group">
			<label for="email">Email address</label>
			<input type="email" value="" name="email" id="email_forgot_smart" class="form-control">
		</div>
		<p class="submit">
            <button class="btn btn-default button button-medium dor-effect-hzt" type="submit"><span>Retrieve Password<i class="icon-chevron-right right"></i></span></button>
            <a href="#" onclick="return false" class="smartLogin reActLogReg">Login</a>
		</p>
	</fieldset>
</form>
</div>
					</div>
</div>


{/if}