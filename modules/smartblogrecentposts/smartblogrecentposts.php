<?php
if (!defined('_PS_VERSION_'))
    exit;
require_once (_PS_MODULE_DIR_.'smartblog/classes/SmartBlogPost.php');
require_once (_PS_MODULE_DIR_.'smartblog/smartblog.php');
if (!class_exists( 'DorImageBase' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/DorImageBase.php');
}
class SmartBlogRecentPosts extends Module {

     public function __construct() {
            $this->name = 'smartblogrecentposts';
            $this->tab = 'front_office_features';
            $this->version = '2.0.1';
            $this->bootstrap = true;
            $this->author = 'SmartDataSoft';
            $this->secure_key = Tools::encrypt($this->name);

            parent::__construct();

            $this->displayName = $this->l('Smart Blog Recent Posts');
            $this->description = $this->l('The Most Powerfull Presta shop Blog  Module\'s tag - by smartdatasoft');
            $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');
        }

            public function install(){
                if (!parent::install() || !$this->registerHook('leftColumn') 
				|| !$this->registerHook('displaySmartBlogLeft')
                || !$this->registerHook('displaySmartBlogRight')
				|| !$this->registerHook('actionsbdeletepost')
				|| !$this->registerHook('actionsbnewpost')
				|| !$this->registerHook('actionsbupdatepost')
				|| !$this->registerHook('actionsbtogglepost')
				)
        return false;
                Configuration::updateGlobalValue('smartshowrecentpost',5);
                Configuration::updateGlobalValue('recent_quanlity_image',90);
                Configuration::updateGlobalValue('recent_thumb_width',50);
                Configuration::updateGlobalValue('recent_thumb_height',70);
             return true;
        }
                
        public function uninstall() {
		 $this->DeleteCache();
            if (!parent::uninstall())
                 return false;
            Configuration::deleteByName('smartshowrecentpost');
            Configuration::deleteByName('recent_quanlity_image');
            Configuration::deleteByName('recent_thumb_width');
            Configuration::deleteByName('recent_thumb_height');
            return true;
                }
                
		public function DeleteCache()
            {
				return $this->_clearCache('smartblogrecentposts.tpl', $this->getCacheId());
			}
     public function hookLeftColumn($params)
            {
         
            }
            
     public function hookRightColumn($params)
            {
                 return $this->hookLeftColumn($params);
            }
         public function hookdisplaySmartBlogLeft($params)
            {
                 if(Module::isInstalled('smartblog') != 1){
                 $this->smarty->assign( array(
                              'smartmodname' => $this->name
                     ));
                        return $this->display(__FILE__, 'views/templates/front/install_required.tpl');
                }
                else
                {
                 if (!$this->isCached('smartblogrecentposts.tpl', $this->getCacheId()))
                    {
                           global $smarty;
                           $id_lang = $this->context->language->id;
                           $posts =  SmartBlogPost::getRecentPosts($id_lang);
                           $i = 0;
                           $postsItem = array();
                           $thumbWidth = Configuration::get('recent_thumb_width');
                           $thumbHeight = Configuration::get('recent_thumb_height');
                           foreach($posts as $post) {
                               if (file_exists(_PS_MODULE_DIR_.'smartblog/images/' . $post['id_smart_blog_post'] . '.jpg') )
                               {
                                    $image =   $post['id_smart_blog_post'];
                                    $posts[$i]['post_img'] = $image;
                                    $pathImg = "smartblog/images/".$post['id_smart_blog_post'].".jpg";
                                    $width=$thumbWidth;$height=$thumbHeight;
                                    $images = DorImageBase::renderThumb($pathImg,$width,$height);
                                    $post['thumb_image'] = $images;
                               }
                               else
                               {
                                  $posts[$i]['post_img'] ='no';
                                  $posts[$i]['thumb_image'] ='no';
                               }
                               $postsItem[] = $post;
                               $i++;

                           }
                           $posts = $postsItem;
                           $smarty->assign( array(
                                         'posts' => $posts
                               ));
                      }
                  return $this->display(__FILE__, 'views/templates/front/smartblogrecentposts.tpl',$this->getCacheId());
                 
                }
            }
         public function hookdisplaySmartBlogRight($params)
            {
                 return $this->hookdisplaySmartBlogLeft($params);
            }        
            
         public function getContent(){
         
                $html = '';
                if(Tools::isSubmit('save'.$this->name))
                {
                    Configuration::updateValue('smartshowrecentpost', Tools::getvalue('smartshowrecentpost'));
                    Configuration::updateValue('recent_quanlity_image', Tools::getvalue('recent_quanlity_image'));
                    Configuration::updateValue('recent_thumb_width', Tools::getvalue('recent_thumb_width'));
                    Configuration::updateValue('recent_thumb_height', Tools::getvalue('recent_thumb_height'));
                    $html = $this->displayConfirmation($this->l('The settings have been updated successfully.'));
                    $helper = $this->SettingForm();
                    $html .= $helper->generateForm($this->fields_form); 
                    return $html;
                }
                else
                {
                   $helper = $this->SettingForm();
                   $html .= $helper->generateForm($this->fields_form);
                   return $html;
                }
            }
            
     public function SettingForm() {
        $default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
        $this->fields_form[0]['form'] = array(
          'legend' => array(
          'title' => $this->l('General Setting'),
            ),
            'input' => array(
                
                array(
                    'type' => 'text',
                    'label' => $this->l('Show Number Of Recent Posts'),
                    'name' => 'smartshowrecentpost',
                    'size' => 15,
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => 'Quanlity Image:',
                    'name' => 'recent_quanlity_image',
                    'class' => 'fixed-width-md',
                ),
                array(
                    'type' => 'text',
                    'label' => 'Thumb width image:',
                    'name' => 'recent_thumb_width',
                    'class' => 'fixed-width-md',
                ),
                array(
                    'type' => 'text',
                    'label' => 'Thumb height image:',
                    'name' => 'recent_thumb_height',
                    'class' => 'fixed-width-md',
                ),          
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        foreach (Language::getLanguages(false) as $lang)
                            $helper->languages[] = array(
                                    'id_lang' => $lang['id_lang'],
                                    'iso_code' => $lang['iso_code'],
                                    'name' => $lang['name'],
                                    'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
                            );
        $helper->toolbar_btn = array(
            'save' =>
            array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save'.$this->name.'token=' . Tools::getAdminTokenLite('AdminModules'),
            )
        );
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;       
        $helper->toolbar_scroll = true;    
        $helper->submit_action = 'save'.$this->name;
        
        $helper->fields_value['smartshowrecentpost'] = Configuration::get('smartshowrecentpost');
        $helper->fields_value['recent_quanlity_image'] = Configuration::get('recent_quanlity_image');
        $helper->fields_value['recent_thumb_width'] = Configuration::get('recent_thumb_width');
        $helper->fields_value['recent_thumb_height'] = Configuration::get('recent_thumb_height');
        return $helper;
      }
		public function hookactionsbdeletepost($params)
            {
                 return $this->DeleteCache();
            }
		public function hookactionsbnewpost($params)
            {
                 return $this->DeleteCache();
            }
		public function hookactionsbupdatepost($params)
            {
                 return $this->DeleteCache();
            }
		public function hookactionsbtogglepost($params)
            {
                 return $this->DeleteCache();
            }
}