<?php
if (!class_exists( 'DorImageBase' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/DorImageBase.php');
}
class dor_flipimage extends Module {
	private $_postErrors  = array();
	public function __construct() {
		$this->name 		= 'dor_flipimage';
		$this->tab 			= 'front_office_features';
		$this->version 		= '1.0';
		$this->bootstrap    = true;
		$this->author 		= 'Dorado Themes';
		$this->displayName 	= $this->l('Dor Hover flip image product');
		$this->description 	= $this->l('Dor Hover flip image product');
		parent :: __construct();
	}
	
	public function install() {
        // Install Tabs
        if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
            $parent_tab = new Tab();
            // Need a foreach for the language
            $parent_tab->name[$this->context->language->id] = $this->l('Dor Extensions');
            $parent_tab->class_name = 'AdminDorMenu';
            $parent_tab->id_parent = 0; // Home tab
            $parent_tab->module = $this->name;
            $parent_tab->add();
        }
        $tab = new Tab();
        foreach (Language::getLanguages() as $language)
        $tab->name[$language['id_lang']] = $this->l('Dor Flip image product');
        $tab->class_name = 'AdminDorFlipImage';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
        $tab->module = $this->name;
        $tab->add();
	   // Install SQL
		Configuration::updateValue($this->name . '_ThumbBaseWidthImage', 250);
		Configuration::updateValue($this->name . '_ThumbBaseHeightImage', 250);
		Configuration::updateValue($this->name . '_productCateWidthImage', 250);
		Configuration::updateValue($this->name . '_productCateHeightImage', 250);
		Configuration::updateValue($this->name . '_quanlityImageFlip', 90);
		include(dirname(__FILE__).'/sql/install.php');
		foreach ($sql as $s)
			if (!Db::getInstance()->execute($s))
				return false;
		return parent :: install()
			&& $this->registerHook('rotatorImg')
			&& $this->registerHook('rotatorThumbImg')
			&& $this->registerHook('rotatorThumbLists')
            && $this->registerHook('header')
            ;
	}
	
	public function uninstall(){
		Configuration::deleteByName($this->name . '_ThumbBaseWidthImage', 250);
        Configuration::deleteByName($this->name . '_ThumbBaseHeightImage', 250);
		Configuration::deleteByName($this->name . '_productCateWidthImage', 250);
        Configuration::deleteByName($this->name . '_productCateHeightImage', 250);
        Configuration::deleteByName($this->name . '_quanlityImageFlip', 90);
		include(dirname(__FILE__).'/sql/uninstall_sql.php');
		foreach ($sql as $s)
			if (!Db::getInstance()->execute($s))
				return false;
		return parent::uninstall();
	}

	public function psversion() {
		$version=_PS_VERSION_;
		$exp=$explode=explode(".",$version);
		return $exp[1];
	}
	private function _postProcess() {
        Configuration::updateValue($this->name . '_ThumbBaseWidthImage', Tools::getValue('ThumbBaseWidthImage'));
        Configuration::updateValue($this->name . '_ThumbBaseHeightImage', Tools::getValue('ThumbBaseHeightImage'));
        Configuration::updateValue($this->name . '_productCateWidthImage', Tools::getValue('productCateWidthImage'));
        Configuration::updateValue($this->name . '_productCateHeightImage', Tools::getValue('productCateHeightImage'));
        Configuration::updateValue($this->name . '_quanlityImageFlip', Tools::getValue('quanlityImageFlip'));
        $this->_html .= $this->displayConfirmation($this->l('Configuration updated'));
    }
	public function getContent() {
        $output = '<h2>' . $this->displayName . '</h2>';
        if (Tools::isSubmit('submitConfigFlip')) {
            if (!sizeof($this->_postErrors))
                $this->_postProcess();
            else {
                foreach ($this->_postErrors AS $err) {
                    $this->_html .= '<div class="alert error">' . $err . '</div>';
                }
            }
        }
        return $output . $this->_displayForm();
    }
	public  function _displayForm() {
		$id_lang = (int)Context::getContext()->language->id;
		$languages = $this->context->controller->getLanguages();
		$default_language = (int)Configuration::get('PS_LANG_DEFAULT');
		$spacer = str_repeat('&nbsp;', $this->spacer_size);
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => 'Quanlity image:',
                        'name' => 'quanlityImageFlip',
                        'class' => 'fixed-width-md',
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Thumb base width image:',
                        'name' => 'ThumbBaseWidthImage',
                        'class' => 'fixed-width-md',
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Thumb base height image:',
                        'name' => 'ThumbBaseHeightImage',
                        'class' => 'fixed-width-md',
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Product category width image:',
                        'name' => 'productCateWidthImage',
                        'class' => 'fixed-width-md',
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Product category height image:',
                        'name' => 'productCateHeightImage',
                        'class' => 'fixed-width-md',
                    ),

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitConfigFlip';
        $helper->module = $this;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id ,
            'cate_data' => $categories,
        );
        $helper->override_folder = '/';
        return $helper->generateForm(array($fields_form));
    }
    public function getConfigFieldsValues()
    {
        return array(
            'ThumbBaseWidthImage' =>     Tools::getValue('ThumbBaseWidthImage', Configuration::get($this->name . '_ThumbBaseWidthImage')),
            'ThumbBaseHeightImage' =>     Tools::getValue('ThumbBaseHeightImage', Configuration::get($this->name . '_ThumbBaseHeightImage')),
            'productCateWidthImage' =>     Tools::getValue('productCateWidthImage', Configuration::get($this->name . '_productCateWidthImage')),
            'productCateHeightImage' =>     Tools::getValue('productCateHeightImage', Configuration::get($this->name . '_productCateHeightImage')),
            'quanlityImageFlip' =>     Tools::getValue('quanlityImageFlip', Configuration::get($this->name . '_quanlityImageFlip')),
        );
    }

	public function getImageProduct($idproduct){
		$id_shop = (int)Context::getContext()->shop->id;
		$sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'image` img 
				LEFT JOIN `'. _DB_PREFIX_ . 'image_shop` imgs
				ON img.id_image = imgs.id_image
				where imgs.`id_shop` ='.$id_shop.'
				AND img.`id_product` ='.$idproduct.'
				AND imgs.`rotator` =1';
		$imageNew = Db::getInstance()->ExecuteS($sql);
		if(!$imageNew) {
			  $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'image` img'; 
			  $sql .= ' where img.`rotator` =1';
			  $sql .= ' AND img.`id_product` ='.$idproduct ;
			  $imageNew = Db::getInstance()->ExecuteS($sql);
		}
		return $imageNew;
	}
	public function hookRotatorImg($params) {
		$idproduct = $params['product']['id_product'];
		$imageNew = $this->getImageProduct($idproduct);

		$this->smarty->assign(
			array('flipImages'=>$imageNew,
			'idproduct'=>$idproduct,
			'product'=>$params['product'],
		));

		return $this->display(__FILE__, 'dor_flipimage.tpl');
	}
	
	public function hookRotatorThumbImg($params){
		
	}
	public function hookrotatorThumbLists($params){
		$idproduct = $params['product']['id_product'];
		$link_rewrite = $params['product']['link_rewrite'];
		$imageNew = $this->getImageProduct($idproduct);
		$thumbWidth = Configuration::get($this->name . '_productCateWidthImage');
		$thumbHeight = Configuration::get($this->name . '_productCateHeightImage');
        $thumbWidth = (isset($thumbWidth) && $thumbWidth != "")?$thumbWidth:450;
        $thumbHeight = (isset($thumbHeight) && $thumbHeight != "")?$thumbHeight:450;
        $quanlity_image = Configuration::get($this->name . '_quanlityImageFlip');
        if($quanlity_image <= 0){
            $quanlity_image = 90;
        }
        $dorDetailCols  = Tools::getValue('dorCategoryThumb',Configuration::get('dorCategoryThumb'));
        $dorCatQuanlity  = Tools::getValue('dorCatQuanlity',Configuration::get('dorCatQuanlity'));
        $dorCatThumbWidth  = Tools::getValue('dorCatThumbWidth',Configuration::get('dorCatThumbWidth'));
        $dorCatThumbHeight  = Tools::getValue('dorCatThumbHeight',Configuration::get('dorCatThumbHeight'));
        if($dorDetailCols == 1 && $dorCatQuanlity != 0 && $dorCatThumbWidth != "" && $dorCatThumbHeight != ""){
            $thumbWidth = $dorCatThumbWidth;
            $thumbHeight = $dorCatThumbHeight;
            $quanlity_image = $dorCatQuanlity;
        }
		
		$id_image = Product::getCover($idproduct);
        
        $images = "";
        $idNewImage = $imageNew[0]['id_image'];
        if (isset($id_image['id_image'])){
            $imageMain = new Image($id_image['id_image']);
            // get image full URL
            $imageMain_url = "/p/".$imageMain->getExistingImgPath().".jpg";
            $linkRewriteMain = $idproduct.'_'.$id_image['id_image']."_1_".$link_rewrite;
            $imageMains = DorImageBase::renderThumbProduct($imageMain_url,$linkRewriteMain,$thumbWidth,$thumbHeight,$quanlity_image);
        }
        if ($idNewImage != ""){
            $image = new Image($idNewImage);
            // get image full URL
            $image_url = "/p/".$image->getExistingImgPath().".jpg";
            $linkRewrite = $idproduct.'_'.$idNewImage."_2_".$link_rewrite;
            $images = DorImageBase::renderThumbProduct($image_url,$linkRewrite,$thumbWidth,$thumbHeight,$quanlity_image);
        }
		$this->smarty->assign(
			array('flipImages'=>$imageNew,
			'idproduct'=>$idproduct,
			'imageThumb1' => $imageMains,
			'imageThumb2' => $images,
			'product'=>$params['product']
		));

		return $this->display(__FILE__, 'dor_flipthumblistimage.tpl');
	}
	public function hookdisplayHeader($params)
	{
		$this->context->controller->addCSS($this->_path.'css/flipimage.css', 'all');
	}
	
}