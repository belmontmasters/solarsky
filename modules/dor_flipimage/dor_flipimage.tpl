<a class = "product_img_link hasFlipImage" href="{$product.link|escape:'html'}" title="{$product.name|escape:html:'UTF-8'}">
	{if isset($flipImages) && $flipImages}
		<img class="img-responsive flip-image-1" itemprop="image" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html'}" alt="{$product.name|escape:html:'UTF-8'}" />
		{foreach from=$flipImages item=image name=thumbimage}
	        {assign var=imageIds value="`$product.id_product`-`$image.id_image`"}
	    <img  class="img-responsive flip-image-2" itemprop="image" src="{$link->getImageLink($product.link_rewrite,$imageIds,'large_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" />
	    {/foreach}
	{else}
		<img class="img-responsive flip-image-0" itemprop="image" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html'}" alt="{$product.name|escape:html:'UTF-8'}" />
	{/if}
</a>