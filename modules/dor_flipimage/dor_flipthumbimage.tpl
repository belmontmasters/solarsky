<a class = "product_img_link hasFlipImage" href="{$product.link|escape:'html'}" title="{$product.name|escape:html:'UTF-8'}">
	{if isset($flipImages) && $flipImages}
		<img class="img-responsive flip-image-1" itemprop="image" src="{$product.imageThumb}" alt="{$product.name|escape:html:'UTF-8'}" />
		{foreach from=$flipImages item=image name=thumbimage}
	        {assign var=imageIds value="`$product.id_product`-`$image.id_image`"}
	    <img  class="img-responsive flip-image-2" itemprop="image" src="{$imageThumb}" alt="{$imageTitle}" />
	    {/foreach}
	{else}
		<img class="img-responsive flip-image-0" itemprop="image" src="{$product.imageThumb}" alt="{$product.name|escape:html:'UTF-8'}" />
	{/if}
</a>