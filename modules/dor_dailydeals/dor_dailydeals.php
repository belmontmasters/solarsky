<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;
if (!class_exists( 'DorImageBase' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/DorImageBase.php');
}
if (!class_exists( 'DorCaches' )) {     
    require_once (_PS_ROOT_DIR_.'/override/Dor/Caches/DorCaches.php');
}

class Dor_dailydeals extends Module
{
	protected $_prefix = '';
	protected $_html = '';
	protected $_postErrors = array();

	protected static $cache_products;

	public function __construct()
	{
		$this->name = 'dor_dailydeals';
		$this->tab = 'pricing_promotion';
		$this->version = '1.0.0';
		$this->author = 'Dorado Themes';
		$this->need_instance = 0;
		$this->_prefix = 'dorpcount_';
		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Dor Daily Deals');
		$this->description = $this->l('Adds a block displaying your current discounted products with countdown timer.');
	}

	public function install()
	{

		// Install Tabs
        if(!(int)Tab::getIdFromClassName('AdminDorMenu')) {
            $parent_tab = new Tab();
            // Need a foreach for the language
            $parent_tab->name[$this->context->language->id] = $this->l('Dor Extensions');
            $parent_tab->class_name = 'AdminDorMenu';
            $parent_tab->id_parent = 0; // Home tab
            $parent_tab->module = $this->name;
            $parent_tab->add();
        }
        $tab = new Tab();
        foreach (Language::getLanguages() as $language)
        $tab->name[$language['id_lang']] = $this->l('Dor Daily Deals');
        $tab->class_name = 'AdminDorDailyDeal';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminDorMenu'); 
        $tab->module = $this->name;
        $tab->add();

		$this->_clearCache('*');
		Configuration::updateValue($this->name . '_typeData', 0);
		Configuration::updateValue($this->name . '_nbr', 8);
		Configuration::updateValue($this->name . '_columns', 1);
		Configuration::updateValue($this->name . '_dealProductIDs', "");
		Configuration::updateValue($this->name . '_deal_content_custom', "");
		Configuration::updateValue($this->name . '_deal_quanlity_image', 90);
		Configuration::updateValue($this->name . '_deal_thumb_width', 300);
		Configuration::updateValue($this->name . '_deal_thumb_height', 400);
		$success = parent::install()
			&& $this->registerHook('header')
			&& $this->registerHook('addproduct')
			&& $this->registerHook('updateproduct')
			&& $this->registerHook('deleteproduct')
			&& $this->registerHook('blockDorado3')
			&& $this->registerHook('blockDorado4')
			&& $this->registerHook('blockDorado5')
			&& $this->registerHook('blockDorado6')
			&& $this->registerHook('blockDorado7')
			&& $this->registerHook('DorHomeLeftColumn');

		return $success;
	}

	public function uninstall()
	{
		$this->_clearCache('*');
		$tab = new Tab((int) Tab::getIdFromClassName('AdminDorDailyDeal'));
        $tab->delete();
		Configuration::deleteByName($this->name . '_typeData', 0);
		Configuration::deleteByName($this->name . '_nbr', 8);
		Configuration::deleteByName($this->name . '_columns', 1);
		Configuration::deleteByName($this->name . '_dealProductIDs', "");
		Configuration::deleteByName($this->name . '_deal_content_custom', "");
		Configuration::deleteByName($this->name . '_deal_quanlity_image', 90);
		Configuration::deleteByName($this->name . '_deal_thumb_width', 300);
		Configuration::deleteByName($this->name . '_deal_thumb_height', 400);
		return parent::uninstall();
	}
	private function _postProcess() {
        Configuration::updateValue($this->name . '_typeData', Tools::getValue('typeData'));
        Configuration::updateValue($this->name . '_nbr', Tools::getValue('nbr'));
        Configuration::updateValue($this->name . '_columns', Tools::getValue('columns'));
        Configuration::updateValue($this->name . '_dealProductIDs', Tools::getValue('dealProductIDs'));
        Configuration::updateValue($this->name . '_deal_content_custom', Tools::getValue('deal_content_custom'));
        Configuration::updateValue($this->name . '_deal_quanlity_image', Tools::getValue('deal_quanlity_image'));
        Configuration::updateValue($this->name . '_deal_thumb_width', Tools::getValue('deal_thumb_width'));
        Configuration::updateValue($this->name . '_deal_thumb_height', Tools::getValue('deal_thumb_height'));
        $this->_html .= $this->displayConfirmation($this->l('Configuration updated'));
    }
	public function getContent()
	{
		$output = '';
		if (Tools::isSubmit('submitUpdate'))
		{
			if (!sizeof($this->_postErrors))
                $this->_postProcess();
            else {
                foreach ($this->_postErrors AS $err) {
                    $this->_html .= '<div class="alert error">' . $err . '</div>';
                }
            }
		}
		return $output . $this->_displayForm();
	}

	public function _displayForm()
	{

		$typeData = array(
            array( 'id'=>'0','mode'=>'Data'),
            array('id'=>'1','mode'=>'Custom Html'),
        );
		$id_lang = (int)Context::getContext()->language->id;
		$languages = $this->context->controller->getLanguages();
		$default_language = (int)Configuration::get('PS_LANG_DEFAULT');
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
                        'type' => 'select',
                        'label' => 'Type Data: ',
                        'name' => 'typeData',
                        'options' => array(
                            'query' => $typeData,
                            'id' => 'id',
                            'name'=>'mode',
                        ),
                    ),
					array(
						'type' => 'text',
						'label' => $this->l('Products to display'),
						'name' => 'nbr',
						'class' => 'dor-data fixed-width-xs',
						'desc' => $this->l('Define the number of products to be displayed in this block on home page.'),
						'default' => 8
					),
					array(
						'type' => 'text',
						'label' => $this->l('Number Columns'),
						'name' => 'columns',
						'class' => 'dor-data fixed-width-xs',
						'desc' => $this->l('Define the number of columns to be displayed products.'),
						'default' => 4
					),
					array(
                        'type' => 'text',
                        'label' => 'Only Product IDs:',
                        'name' => 'dealProductIDs',
                        'desc' => $this->l('When input value "1,2,3..." then only display product selected.'),
                        'class' => 'dor-data fixed-width-md',
                    ),
	                array(
                        'type' => 'textarea',
                        'label' => 'Custom Text Content:',
                        'name' => 'deal_content_custom',
                        'class' => 'fixed-width-full',
                        'cols' => 60,
                        'rows' => 10,
                    ),
					array(
                        'type' => 'text',
                        'label' => 'Quanlity Image:',
                        'name' => 'deal_quanlity_image',
                        'class' => 'fixed-width-md',
                        'default' => 90
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Thumb width image:',
                        'name' => 'deal_thumb_width',
                        'class' => 'fixed-width-md',
                        'default' => 300
                    ),
                    array(
                        'type' => 'text',
                        'label' => 'Thumb height image:',
                        'name' => 'deal_thumb_height',
                        'class' => 'fixed-width-md',
                        'default' => 400
                    ),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = (int)Tools::getValue('id_carrier');
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitUpdate';
        $helper->module = $this;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id ,
            'cate_data' => $categories,
        );
        $helper->override_folder = '/';
        return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
    {
        return array(
            'typeData' =>     Tools::getValue('typeData', Configuration::get($this->name . '_typeData')),
            'nbr' =>     Tools::getValue('nbr', Configuration::get($this->name . '_nbr')),
            'columns' =>     Tools::getValue('columns', Configuration::get($this->name . '_columns')),
            'dealProductIDs' =>   Tools::getValue('dealProductIDs', Configuration::get($this->name . '_dealProductIDs')),
            'deal_content_custom' =>     Tools::getValue('deal_content_custom', Configuration::get($this->name . '_deal_content_custom')),
            'deal_quanlity_image' =>     Tools::getValue('deal_quanlity_image', Configuration::get($this->name . '_deal_quanlity_image')),
            'deal_thumb_width' =>    Tools::getValue('deal_thumb_width', Configuration::get($this->name . '_deal_thumb_width')),
            'deal_thumb_height' =>    Tools::getValue('deal_thumb_height', Configuration::get($this->name . '_deal_thumb_height')),
        );
    }
    public function FilterProductSales($productID)
    {
        $sql = 'SELECT SUM(od.product_quantity) totalSales
							FROM '._DB_PREFIX_.'order_detail od WHERE od.product_id = '.$productID.' GROUP BY od.product_id';
        $result = Db::getInstance()->getValue($sql);
        return $result;
    }
    public function DorCacheDaily($fileCache, $id_lang){
    	/****Dor Caches****/
    	$id_shop = (int)Context::getContext()->shop->id;
    	$fileCache = $fileCache."-Shop".$id_shop;
		$dorCaches  = Tools::getValue('enableDorCache',Configuration::get('enableDorCache'));
		$cacheData = array();
		if($dorCaches){
			$objCache = new DorCaches(array('name'=>'default','path'=>_PS_ROOT_DIR_.'/override/Dor/Caches/smartcaches/dailydeals/','extension'=>'.cache'));
            $objCache->setCache($fileCache);
            $cacheData = $objCache->renderData($fileCache);
        }
        if($cacheData && $dorCaches){
                $productsData = $cacheData['lists'];
        }else{
        	$nbr = Configuration::get($this->name . '_nbr');
			$quanlity_image = Configuration::get($this->name . '_deal_quanlity_image');
			$thumbWidth = Configuration::get($this->name . '_deal_thumb_width');
			$thumbHeight = Configuration::get($this->name . '_deal_thumb_height');
			$dealProductIDs = Configuration::get($this->name . '_dealProductIDs');
			$columns = Configuration::get($this->name . '_columns');
			$deal_content_custom = Configuration::get($this->name . '_deal_content_custom');
        	if(trim($dealProductIDs) == ""){
				$productsData = Product::getPricesDrop($id_lang, 0, $nbr, $count = false, $order_by = "date_add", $order_way = "DESC");
			}else{
				$dealProductIDs = explode(",", $dealProductIDs);
				$productsData = $this->getPricesDrop($dealProductIDs,$id_lang, 0, $nbr, $count = false, $order_by = "date_add", $order_way = "DESC");
			}

			$products = array();
			if($deal_content_custom != ""){
				$contentCustom = explode(PHP_EOL, $deal_content_custom);
				$cusDatas = array();
				foreach ($contentCustom as $k => $content) {
					$customs = explode("=>", $content);
					$idp = isset($customs[0]) && $customs[0] != ""?$customs[0]:0;
					$images = isset($customs[1])?$customs[1]:"";
					if($images != ""){
						$image = json_decode($images);
						$image1 = isset($image->image1) && $image->image1 != ""?$image->image1:"";
						$image2 = isset($image->image2) && $image->image2 != ""?$image->image2:"";
					}
					if($idp > 0 && $images != ""){
						$cusDatas[$idp]['image1'] = isset($image1)?$image1:"";
						$cusDatas[$idp]['image2'] = isset($image2)?$image2:"";
					}
					
				}
			}

			$fullUrl = _PS_BASE_URL_.__PS_BASE_URI__;
			foreach ($productsData as &$product) {
				$proAccept = 0;
				$totalSales = $this->FilterProductSales($product['id_product']);
				$product['totalSales'] = $totalSales;
				if(count($dealProductIDs) > 0 && !in_array($product['id_product'], $dealProductIDs)){
					$proAccept = 1;
				}
				if ($product['specific_prices']['to'] != '0000-00-00 00:00:00' && $proAccept == 0) {
					$time = strtotime($product['specific_prices']['to']);
					$product['special_finish_time'] = date('m', $time).'/'.date('d', $time).'/'.date('Y', $time).' '.date('H', $time).':'.date('i', $time).':'.date('s', $time);
					$idProduct = $product['id_product'];
					if(count($cusDatas) > 0 && isset($cusDatas[$idProduct])){
						$images = $cusDatas[$idProduct];
						$product['image1'] = $fullUrl.$images['image1'];
						$product['image2'] = $fullUrl.$images['image2'];
					}else{
						$id_image = Product::getCover($product['id_product']);
	                    // get Image by id
	                    $images = "";
	                    $product['thumb_image'] = "";
	                    if (sizeof($id_image) > 0){
	                        $image = new Image($id_image['id_image']);
	                        // get image full URL
	                        $image_url = "/p/".$image->getExistingImgPath().".jpg";
	                        $width=$thumbWidth;$height=$thumbHeight;
	                        $linkRewrite = $product['id_product']."_".$id_image['id_image']."_".$product['link_rewrite'];
	                        $images = DorImageBase::renderThumbProduct($image_url,$linkRewrite,$width,$height,$quanlity_image);
	                    	$product['thumb_image'] = $images;
	                    }
	                    $roatorImage = Dor_dailydeals::RotatorImg($product['id_product']);
	                    if($roatorImage['rotator_img']){
	                    	$idRotator = $roatorImage['rotator_img'][0]['id_image'];
	                    	if (sizeof($idRotator) > 0){
		                        $imageRotator = new Image($idRotator);
		                        // get image full URL
		                        $imageRotator_url = "/p/".$imageRotator->getExistingImgPath().".jpg";
		                        $width=$thumbWidth;$height=$thumbHeight;
		                        $linkRewriteRotator = $product['id_product']."_".$idRotator."_".$product['link_rewrite'];
		                        $imageRotator = DorImageBase::renderThumbProduct($imageRotator_url,$linkRewriteRotator,$width,$height,$quanlity_image);
		                    	$product['thumb_image_rotator'] = $imageRotator;
		                    }
	                    }
                    }
                    $products[] = $product;
				}
			}
			$productsData = $products;
			if($dorCaches){
				$data['lists'] = $products;
	            $objCache->store($fileCache, $data, $expiration = TIME_CACHE_HOME);
        	}
        }
        //echo "<pre>";print_r($productsData);die;
        return $productsData;
        /****End Dor Caches****/
    }
	public function hookBlockDorado3($params)
	{
		if (!$this->isCached('dor_dailydeals.tpl', $this->getCacheId('Dor_dailydeals'))) {
			$fileCache = 'HomeDailyDealsCaches';
			$id_lang = (int)$params['cookie']->id_lang;
			$products = $this->DorCacheDaily($fileCache,$id_lang);
			Dor_dailydeals::$cache_products = $products;
		}

		if (Dor_dailydeals::$cache_products === false)
			return false;

		$product_path = dirname(__FILE__).'/views/templates/hook/product.tpl';
		$tproduct_path = _PS_ALL_THEMES_DIR_._THEME_NAME_.'/modules/'.$this->name.'/views/templates/hook/product.tpl';
		if (file_exists($tproduct_path))
			$product_path = $tproduct_path;

		$this->smarty->assign(array(
			'products' => Dor_dailydeals::$cache_products,
			'columns' => $columns,
			'description' => $deal_content_custom,
			'product_path' => $product_path
		));

		return $this->display(__FILE__, 'dor_dailydeals.tpl', $this->getCacheId('Dor_dailydeals'));
	}


	/**
    * Get prices drop
    *
    * @param int $id_lang Language id
    * @param int $pageNumber Start from (optional)
    * @param int $nbProducts Number of products to return (optional)
    * @param bool $count Only in order to get total number (optional)
    * @return array Prices drop
    */
    public static function getPricesDrop($productIds, $id_lang, $page_number = 0, $nb_products = 10, $count = false,
        $order_by = null, $order_way = null, $beginning = false, $ending = false, Context $context = null)
    {
        if (!Validate::isBool($count)) {
            die(Tools::displayError());
        }
        if (!$context) {
            $context = Context::getContext();
        }
        if ($page_number < 0) {
            $page_number = 0;
        }
        if ($nb_products < 1) {
            $nb_products = 10;
        }
        if (empty($order_by) || $order_by == 'position') {
            $order_by = 'price';
        }
        if (empty($order_way)) {
            $order_way = 'DESC';
        }
        if ($order_by == 'id_product' || $order_by == 'price' || $order_by == 'date_add' || $order_by == 'date_upd') {
            $order_by_prefix = 'product_shop';
        } elseif ($order_by == 'name') {
            $order_by_prefix = 'pl';
        }
        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) {
            die(Tools::displayError());
        }
        $current_date = date('Y-m-d H:i:00');
        
        $tab_id_product = $productIds;
        $front = true;
        if (!in_array($context->controller->controller_type, array('front', 'modulefront'))) {
            $front = false;
        }

        $sql_groups = '';
        if (Group::isFeatureActive()) {
            $groups = FrontController::getCurrentCustomerGroups();
            $sql_groups = ' AND EXISTS(SELECT 1 FROM `'._DB_PREFIX_.'category_product` cp
				JOIN `'._DB_PREFIX_.'category_group` cg ON (cp.id_category = cg.id_category AND cg.`id_group` '.(count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1').')
				WHERE cp.`id_product` = p.`id_product`)';
        }

        if (strpos($order_by, '.') > 0) {
            $order_by = explode('.', $order_by);
            $order_by = pSQL($order_by[0]).'.`'.pSQL($order_by[1]).'`';
        }

        $sql = '
		SELECT
			p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`,
			IFNULL(product_attribute_shop.id_product_attribute, 0) id_product_attribute,
			pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`,
			pl.`name`, image_shop.`id_image` id_image, il.`legend`, m.`name` AS manufacturer_name,
			DATEDIFF(
				p.`date_add`,
				DATE_SUB(
					"'.date('Y-m-d').' 00:00:00",
					INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
				)
			) > 0 AS new
		FROM `'._DB_PREFIX_.'product` p
		'.Shop::addSqlAssociation('product', 'p').'
		LEFT JOIN `'._DB_PREFIX_.'product_attribute_shop` product_attribute_shop
			ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop='.(int)$context->shop->id.')
		'.Product::sqlStock('p', 0, false, $context->shop).'
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
			p.`id_product` = pl.`id_product`
			AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
		)
		LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
			ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$context->shop->id.')
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
		LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
		WHERE product_shop.`active` = 1
		AND product_shop.`show_price` = 1
		'.($front ? ' AND p.`visibility` IN ("both", "catalog")' : '').'
		'.((!$beginning && !$ending) ? ' AND p.`id_product` IN ('.((is_array($tab_id_product) && count($tab_id_product)) ? implode(', ', $tab_id_product) : 0).')' : '').'
		'.$sql_groups.'
		ORDER BY '.(isset($order_by_prefix) ? pSQL($order_by_prefix).'.' : '').pSQL($order_by).' '.pSQL($order_way).'
		LIMIT '.(int)($page_number * $nb_products).', '.(int)$nb_products;
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if (!$result) {
            return false;
        }

        if ($order_by == 'price') {
            Tools::orderbyPrice($result, $order_way);
        }

        return Product::getProductsProperties($id_lang, $result);
    }
	public function _getProductIdByDate($beginning, $ending, Context $context = null, $with_combination = false)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        $id_address = $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
        $ids = Address::getCountryAndState($id_address);
        $id_country = $ids['id_country'] ? (int)$ids['id_country'] : (int)Configuration::get('PS_COUNTRY_DEFAULT');

        return SpecificPrice::getProductIdByDate(
            $context->shop->id,
            $context->currency->id,
            $id_country,
            $context->customer->id_default_group,
            $beginning,
            $ending,
            0,
            $with_combination
        );
    }
	public function hookdorHomeLeftColumn($params)
	{
		return $this->hookBlockDorado3($params);
	}
	public function RotatorImg($idproduct) {
			$id_shop = (int)Context::getContext()->shop->id;
			$sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'image` img'; 
			$sql .= ' LEFT JOIN `'. _DB_PREFIX_ . 'image_shop` imgs';
			$sql .= ' ON img.id_image = imgs.id_image';
			$sql .= ' where imgs.`id_shop` ='.$id_shop ;
			$sql .= ' AND img.`id_product` ='.$idproduct ;
			$sql .= ' AND imgs.`rotator` =1' ;
			$imageNew = Db::getInstance()->ExecuteS($sql);
			if(!$imageNew) {
				  $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'image` img'; 
				  $sql .= ' where img.`rotator` =1';
				  $sql .= ' AND img.`id_product` ='.$idproduct ;
				  $imageNew = Db::getInstance()->ExecuteS($sql);
			}

			$images = array(
				'rotator_img'=>$imageNew,
				'idproduct'=>$idproduct
			);

		return $images;
	}

	public function hookHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'views/css/dor_dailydeals.css', 'all');
		$this->context->controller->addJS(($this->_path).'views/js/countdown.js');
		
	}

	public function hookAddProduct($params)
	{
		$this->_clearCache('*');
	}

	public function hookUpdateProduct($params)
	{
		$this->_clearCache('*');
	}

	public function hookDeleteProduct($params)
	{
		$this->_clearCache('*');
	}

	public function getConfigValue($key)
    {
      return Configuration::get($this->getName($key));
    }

    public function getName($name) {
		return Tools::strtoupper($this->_prefix.$name);
	}

	protected function getCacheId($name = null)
	{
		if ($name === null) {
			$name = 'Dor_dailydeals';
		}
		return parent::getCacheId($name.'|'.date('Ymd'));
	}

	public function _clearCache($template, $cache_id = null, $compile_id = null)
	{
		parent::_clearCache('dor_dailydeals.tpl');
	}
}
