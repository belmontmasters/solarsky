var DORSEARCH = {
	init:function(){
		DORSEARCH.CategoryDropdown();
		DORSEARCH.DorAjaxSearch();
		DORSEARCH.ReloadCategoryName();
	},
	DorAjaxSearch:function(){
		jQuery("#dor_query_top").keyup(function(){
			var keySearch = jQuery(this).val();
			var cateId = jQuery('input[name="valSelected"]').val();
			if(keySearch.length > 1){
				var urlAjax = jQuery("#searchbox").attr("action");
				var params = {}
				params.search_query = keySearch;
				params.cateID = parseInt(cateId);
				params.ajaxSearch = 1;
				params.id_lang = id_lang;
				jQuery.ajax({
		            url: urlAjax,
		            data:params,
		            type:"GET",
		            success:function(data){
		            	jQuery(".filterDataSearch").remove();
		            	if(data){
		            		jQuery(".dor_search").append(JSON.parse(data));
		            		jQuery(document).click(function (event) {
					            if (!jQuery(event.target).is(".filterDataSearch, .filterDataSearch *")) {
					                jQuery(".filterDataSearch").remove();
					            }
					        });
		            	}
		                return false;
		            }
		        });
			}
		});
	},
	CategoryDropdown:function(){
	    $( document.body ).on( 'click', '.pos_search .dropdown-menu li a', function( event ) {
	      var $target = $( event.currentTarget );
	      var stringText = $target.text().replace(/\–/g,'');
	      var val = jQuery(this).attr("data-value");
	      jQuery('input[name="valSelected"]').val(val);
	      $target.closest( '.pos_search' )
	         .find( '[data-bind="label"]' ).text( stringText )
	            .end()
	         .children( '.dropdown-toggle' ).dropdown( 'toggle' );
	      return false;
	   });
	},
	ReloadCategoryName:function(){
		var valSelected = jQuery('input[name="valSelected"]').val();
		var cateName = jQuery(".pos_search .dropdown-menu li a[data-value='"+valSelected+"']").text();
		cateName = cateName.replace(/\–/g,'');
		jQuery(".pos_search .dropdown-toggle span[data-bind='label']").text(cateName);
	}
};
jQuery(document).ready(function(){
	DORSEARCH.init();
});