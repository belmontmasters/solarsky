{capture name=path}<a href="{smartblog::GetSmartBlogLink('smartblog')}">{l s='All Blog News' mod='smartblog'}</a><span class="navigation-pipe">{$navigationPipe}</span>{$meta_title}{/capture}
<div id="content" class="block">
   <div itemtype="#" itemscope="" id="sdsblogArticle" class="blog-post">
		{assign var="catOptions" value=null}
		{$catOptions.id_category = $id_category}
		{$catOptions.slug = $cat_link_rewrite}
      	<div data-itemprop="articleBody">
            <div id="lipsum" class="articleContent">
                    {assign var="activeimgincat" value='0'}
                    {$activeimgincat = $smartshownoimg} 
                    {if ($post_img != "no" && $activeimgincat == 0) || $activeimgincat == 1}
                        <a id="post_images" href="{$modules_dir}/smartblog/images/{$post_img}-single-default.jpg"><img src="{$modules_dir}/smartblog/images/{$post_img}-single-default.jpg" alt="{$meta_title}"></a>
                    {/if}
             </div>
             <div class="media clearfix">
	             <div class="entry-meta media-left">
	                <!-- Publish Date -->
	                <div class="entry-time meta-date">
	                    <time datetime="2015-12-09T21:10:20+00:00" data-itemprop="datePublished">
	                        <span class="entry-time-date dblock">{$created|date_format:"%e"}</span>
	                        {$created|date_format:"%b"}
	                    </time>
	                </div>

	                <!-- Number of Comments -->
	                <div class="entry-reply">
	                    <a data-itemprop="discussionURL" class="comments-link" href="single.html#comments">
	                        <i class="fa fa-comment dblock"></i>
	                        {if $countcomment != ''}{$countcomment}{else}{l s='0' mod='smartblog'}{/if}
	                    </a>
	                </div>
	            </div>
	             <div class="media-body">
		             <header class="entry-header">
		                <!-- Author Link -->
		                <span class="vcard author entry-author">
		                    <a href="#" rel="author" class="url fn n">
		                        {if $smartshowauthor ==1}<span data-itemprop="author">{if $smartshowauthorstyle != 0}{$firstname} {$lastname}{else}{$lastname} {$firstname}{/if}</span>{/if}
		                    </a>
		                </span>
		                <!-- Post Categories -->
		                <span data-itemprop="articleSection" class="entry-categories">
		                    <a href="{smartblog::GetSmartBlogLink('smartblog_category',$catOptions)}">{$title_category}</a>
		                </span>
		                <!-- Post Title -->
		                <h3 data-itemprop="headline" class="entry-title">
		                    {$meta_title}
		                </h3>
		            </header>

		            <div class="sdsarticle-des entry-content">
		               {$content}
		            </div>
		            {if $tags != ''}
		            <footer class="entry-footer clearfix">
		                <div class="sdstags-update">
		                    <span class="tags"><b>{l s='Tags:' mod='smartblog'} </b> 
		                        {foreach from=$tags item=tag}
		                            {assign var="options" value=null}
		                            {$options.tag = $tag.name}
		                            <a title="tag" href="{smartblog::GetSmartBlogLink('smartblog_tag',$options)}">{$tag.name}</a>
		                        {/foreach}
		                    </span>
		                </div>
		            </div>
		           {/if}
		        </div>
	        </div>
      </div>
     
      <div class="sdsarticleBottom">
        {$HOOK_SMART_BLOG_POST_FOOTER}
      </div>
   </div>

{if $countcomment != ''}
<div id="articleComments">
            <h3>{if $countcomment != ''}{$countcomment}{else}{l s='0' mod='smartblog'}{/if}{l s=' thoughts on' mod='smartblog'}<span></span>“{$meta_title}”</h3>
        <div id="comments">      
            <ul class="commentList">
                  {$i=1}
                {foreach from=$comments item=comment}
                    
                       {include file="./comment_loop.tpl" childcommnets=$comment}
                   
                  {/foreach}
            </ul>
        </div>
</div>
 {/if}

</div>
{if Configuration::get('smartenablecomment') == 1}
{if $comment_status == 1}
<div class="smartblogcomments" id="respond">
    <!-- <h4 id="commentTitle">{l s="Leave a Comment"  mod="smartblog"}</h4> -->
    <h4 class="comment-reply-title" id="reply-title">{l s="Leave a Comment"  mod="smartblog"} <small style="float:right;">
                <a style="display: none;" href="/wp/sellya/sellya/this-is-a-post-with-preview-image/#respond" 
                   id="cancel-comment-reply-link" rel="nofollow">{l s="Cancel Reply"  mod="smartblog"}</a>
            </small>
        </h4>
		<div id="commentInput">
			<form action="" method="post" id="commentform">
			<p class="comment-notes">
                <span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span>
            </p>
			<div>
				<div class="field-cmt">
					<div class="row">
						<div class="field-mini-row col-lg-6 col-sm-6 col-sx-6">
							<span class="required">*</span><b>{l s="Name or Nickname"  mod="smartblog"} </b>
							<input type="text" tabindex="1" class="inputName form-control cmt-field" value="" name="name">
						</div>
						<div class="field-mini-row col-lg-6 col-sm-6 col-sx-6">
							<span class="required">*</span><b>{l s="Email:"  mod="smartblog"} </b>
							<input type="text" tabindex="2" class="inputMail form-control cmt-field" value="" name="mail">
						</div>
					</div>
				</div>
				<div class="field-cmt">
					<span class="required">*</span><b> {l s="Your Message"  mod="smartblog"}</b>
					<textarea tabindex="4" class="inputContent form-control cmt-field" rows="8" cols="50" name="comment"></textarea>
				</div>
				<div class="field-cmt hidden">
					<b>{l s="Website:"  mod="smartblog"} </b>
					<input type="text" tabindex="3" value="" name="website" class="form-control grey">
				</div>
			</div>
			{if Configuration::get('smartcaptchaoption') == '1'}
			<div class="captcha-blog">
				<div class="captcha-blog-image"><img src="{$modules_dir}smartblog/classes/CaptchaSecurityImages.php?width=100&height=40&characters=5"></div>
				<div class="col-lg-6 col-sm-6 col-xs-12 row">
					<label>{l s="Enter Captcha Code" mod="smartblog"}</label>
					<input type="text" tabindex="" value="" name="smartblogcaptcha" class="smartblogcaptcha form-control cmt-field">
				</div>
			</div>
			{/if}
                 <input type='hidden' name='comment_post_ID' value='1478' id='comment_post_ID' />
                  <input type='hidden' name='id_post' value='{$id_post}' id='id_post' />

                <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
			<div class="button-submit-comment vnnClearfix">
		        <div class="submit">
		            <input type="submit" name="addComment" id="submitComment" class="bbutton btn btn-default button-medium" value="Post Comment">
				</div>
			</div>
        </form>
	</div>
</div>

<script type="text/javascript">
$('#submitComment').bind('click',function(event) {
event.preventDefault();
 
 
var data = { 'action':'postcomment', 
'id_post':$('input[name=\'id_post\']').val(),
'comment_parent':$('input[name=\'comment_parent\']').val(),
'name':$('input[name=\'name\']').val(),
'website':$('input[name=\'website\']').val(),
'smartblogcaptcha':$('input[name=\'smartblogcaptcha\']').val(),
'comment':$('textarea[name=\'comment\']').val(),
'mail':$('input[name=\'mail\']').val() };
	$.ajax( {
	  url: baseDir + 'modules/smartblog/ajax.php',
	  data: data,
	  
	  dataType: 'json',
	  
	  beforeSend: function() {
				$('.success, .warning, .error').remove();
				$('#submitComment').attr('disabled', true);
				$('#commentInput').before('<div class="attention"><img src="http://321cart.com/sellya/catalog/view/theme/default/image/loading.gif" alt="" />Please wait!</div>');

				},
				complete: function() {
				$('#submitComment').attr('disabled', false);
				$('.attention').remove();
				},
		success: function(json) {
			if (json['error']) {
					 
						$('#commentInput').before('<div class="warning">' + '<i class="icon-warning-sign icon-lg"></i>' + json['error']['common'] + '</div>');
						
						if (json['error']['name']) {
							$('.inputName').after('<span class="error">' + json['error']['name'] + '</span>');
						}
						if (json['error']['mail']) {
							$('.inputMail').after('<span class="error">' + json['error']['mail'] + '</span>');
						}
						if (json['error']['comment']) {
							$('.inputContent').after('<span class="error">' + json['error']['comment'] + '</span>');
						}
						if (json['error']['captcha']) {
							$('.smartblogcaptcha').after('<span class="error">' + json['error']['captcha'] + '</span>');
						}
					}
					
					if (json['success']) {
						$('input[name=\'name\']').val('');
						$('input[name=\'mail\']').val('');
						$('input[name=\'website\']').val('');
						$('textarea[name=\'comment\']').val('');
				 		$('input[name=\'smartblogcaptcha\']').val('');
					
						$('#commentInput').before('<div class="success">' + json['success'] + '</div>');
						setTimeout(function(){
							$('.success').fadeOut(300).delay(450).remove();
													},2500);
					
					}
				}
			} );
		} );
		
 




    var addComment = {
	moveForm : function(commId, parentId, respondId, postId) {

		var t = this, div, comm = t.I(commId), respond = t.I(respondId), cancel = t.I('cancel-comment-reply-link'), parent = t.I('comment_parent'), post = t.I('comment_post_ID');

		if ( ! comm || ! respond || ! cancel || ! parent )
			return;
 
		t.respondId = respondId;
		postId = postId || false;

		if ( ! t.I('wp-temp-form-div') ) {
			div = document.createElement('div');
			div.id = 'wp-temp-form-div';
			div.style.display = 'none';
			respond.parentNode.insertBefore(div, respond);
		}


		comm.parentNode.insertBefore(respond, comm.nextSibling);
		if ( post && postId )
			post.value = postId;
		parent.value = parentId;
		cancel.style.display = '';

		cancel.onclick = function() {
			var t = addComment, temp = t.I('wp-temp-form-div'), respond = t.I(t.respondId);

			if ( ! temp || ! respond )
				return;

			t.I('comment_parent').value = '0';
			temp.parentNode.insertBefore(respond, temp);
			temp.parentNode.removeChild(temp);
			this.style.display = 'none';
			this.onclick = null;
			return false;
		};

		try { t.I('comment').focus(); }
		catch(e) {}

		return false;
	},

	I : function(e) {
		return document.getElementById(e);
	}
};

      
      
</script>
{/if}
{/if}
{if isset($smartcustomcss)}
    <style>
        {$smartcustomcss}
    </style>
{/if}
