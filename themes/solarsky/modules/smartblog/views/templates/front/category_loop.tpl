<article class="post type-post format-standard has-post-thumbnail" itemscope="itemscope" itemtype="http://schema.org/BlogPosting" data-itemprop="blogPost">
      <!-- Featured Media -->
      <div class="entry-media">
        {assign var="options" value=null}
        {$options.id_post = $post.id_post}
        {$options.slug = $post.link_rewrite}
          <a data-itemprop="url" href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}" title="{$post.meta_title}" class="imageFeaturedLink">
              <img src="{$post.thumb_image}" alt="" data-itemprop="image">
          </a>
      </div>          
      <div class="media clearfix">
          <div class="entry-meta media-left">
              <!-- Publish Date -->
              <div class="entry-time meta-date">
                  <time data-itemprop="datePublished" datetime="{$post.created}">
                      <span class="entry-time-date dblock">{$post.created|date_format:"%e"}</span>
                      {$post.created|date_format:"%b"}
                  </time>
              </div>

              <!-- Number of Comments -->
              <div class="entry-reply">
                  <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}#comments" class="comments-link" data-itemprop="discussionURL">
                      <i class="fa fa-comment dblock"></i>
                      {$post.totalcomment}
                  </a>
              </div>
          </div>

          <div class="media-body">
              <header class="entry-header">
                  <!-- Author Link -->
                  {if $smartshowauthor ==1}
                    
                  <span class="vcard author entry-author">
                      <a class="url fn n" rel="author" href="#">
                          {if $smartshowauthorstyle != 0}
                            {$post.firstname} {$post.lastname}
                          {else}
                            {$post.lastname} {$post.firstname}
                          {/if}
                      </a>
                  </span>
                  {/if}
                  <!-- Post Categories -->
                  {assign var="catlink" value=null}
                  {$catlink.id_category = $post.id_category}
                  {$catlink.slug = $post.cat_link_rewrite}
                  <span class="entry-categories" data-itemprop="articleSection">
                    <a href="{smartblog::GetSmartBlogLink('smartblog_category',$catlink)}">{if $title_category != ''}{$title_category}{else}{$post.cat_name}{/if}</a>
                  </span>


                  <!-- Post Title -->
                  <h3 class="entry-title" data-itemprop="headline">
                      <a title="{$post.meta_title}" href='{smartblog::GetSmartBlogLink('smartblog_post',$options)}'>{$post.meta_title}</a>
                  </h3>
              </header>

              <!-- Main Content of the Post -->
              <div class="entry-content" data-itemprop="description">
                  <p>{$post.short_description|strip_tags:'UTF-8'|truncate:550:'...'}</p>
                  <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}" class="read-more-link thm-clr">Read More  <i class="fa fa-long-arrow-right"></i></a>
              </div>
          </div>
      </div>
  </article>