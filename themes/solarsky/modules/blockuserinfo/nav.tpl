<!-- Block user information module NAV  -->
	<div class="pull-right dropdown">
		<div class="current head-current-useInfo">
			<em></em>
			<div class="useInfoCurrText">
				<span class="useLine1">{l s='Hello.' mod='blockuserinfo'} {if $logged}<span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span>{else}<span>{l s='Sign in' mod='blockuserinfo'}</span>{/if}</span>
				<span class="useLine2">{l s='Your Account' mod='blockuserinfo'} <i class="fa fa-angle-down"></i></span>
			</div>
		</div>
		<ul class="toogle_content" style="display: none;">
			<li class="best-userinfo-head">
				<div class="main-userinfo-head">
					<h3>{l s='Welcome to HanoiStore' mod='blockuserinfo'}</h3>
					{if !$logged}
					<a href="{$base_uri}login" class="btn btn-default"><span>{l s='Sign in' mod='blockuserinfo'}</span></a>
					<div class="more-userinfo-link">
						<span>{l s='New Customer?' mod='blockuserinfo'}</span>
						<a href="{$base_uri}login" title="{l s='Sign up now' mod='blockuserinfo'}">{l s='Sign up now' mod='blockuserinfo'}</a>
					</div>
					{else}
						<span class="useLine1">{l s='Hello!' mod='blockuserinfo'} {if $logged}<span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span>{else}<span>{l s='Sign in' mod='blockuserinfo'}</span>{/if}</span>
					{/if}
				</div>
			</li>
			{$context = Context::getContext()}
			<li class="hidden"><a href="{$link->getPageLink('contact', true)|escape:'html':'UTF-8'}" title="{l s='Contact Us' mod='blockcontact'}">
				{l s='Contact Us' mod='blockuserinfo'}
			</a></li>
			<li><a class="link-myaccount" href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='My account' mod='blockuserinfo'}">
				{l s='My account' mod='blockuserinfo'}
			</a></li>
			<li><a class="link-wishlist wishlist_block" href="{$context->link->getModuleLink('blockwishlist', 'mywishlist')}" title="{l s='My wishlist' mod='blockuserinfo'}">
			{l s='My wishlist' mod='blockuserinfo'}</a></li>
			{if !$PS_CATALOG_MODE}
			<li><a class="link-mycart" href="{$link->getPageLink('order', true)|escape:'html'}" title="{l s='My cart' mod='blockuserinfo'}">
				{l s='My cart' mod='blockuserinfo'}</a></li>
			{/if}
			{if $logged}
			<li><a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">{l s='Sign out' mod='blockuserinfo'}</a></li>
			{else}
			<li><a href="#" onclick="return false" class="smartLogin">{l s='Sign in popup' mod='blockuserinfo'}</a></li>
			<li><a href="#" onclick="return false" class="smartRegister">{l s='Sign up popup' mod='blockuserinfo'}</a></li>
			<!-- <li class="last">
				<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='blockuserinfo'}">
					{l s='Sign in' mod='blockuserinfo'}
					<span>{l s='or' mod='blockuserinfo'}</span>
					{l s='Register' mod='blockuserinfo'}
				</a>
			</li> -->
			{/if}
		</ul>
	</div>
<!-- /Block usmodule NAV -->
