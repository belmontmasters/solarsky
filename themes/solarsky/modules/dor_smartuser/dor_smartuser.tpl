{if !$logged}
<div id="loginFormSmart">
	<span class="button b-close"><i class="fa-times fa"></i></span>
	<form id="login_form" method="post" action="{$base_dir}login">
		<div class="login-wrap text-center wht-clr">
			<h2 class="fsz-35 spcbtm-15"> 
				<img src="{$tpl_uri|escape:'html':'UTF-8'}img/logo_1.png" alt="{$shop_name|escape:'html':'UTF-8'}"> 
			</h2>
			<p class="fsz-20 title-3"> {l s='WELCOME TO OUR  WONDERFUL WORLD' mod='dor_smartuser'} </p>
			<p class="fsz-15 bold-font-4"> {l s='Did you know that we ship to over' mod='dor_smartuser'} <span class="thm-clr"> {l s='24 different countries' mod='dor_smartuser'} </span> </p>
			<div class="login-form">
				<a href="#" class="fb-btn btn spcbtm-15"> <i class="fa fa-facebook btn-icon"></i>{l s='Login with Facebook' mod='dor_smartuser'}</a>
				<p class="bold-font-2 fsz-12 signup"> {l s='OR SIGN UP' mod='dor_smartuser'} </p>
				<div class="form-group">
					<label data-for="email_smart" class="hidden">{l s='Email' mod='dor_smartuser'}</label>
					<input type="email" value="" name="email" id="dor_email_smart" data-validate="isEmail" class="is_required validate account_input form-control" placeholder="{l s='Email' mod='dor_smartuser'}">
				</div>
				<div class="form-group">
					<label data-for="passwd" class="hidden">{l s='Password' mod='dor_smartuser'}</label>
					<input type="password" value="" name="passwd" id="dor_passwd_smart" data-validate="isPasswd" class="is_required validate account_input form-control" placeholder="{l s='Password' mod='dor_smartuser'}">
				</div>
				<div class="form-group">
					<button name="SubmitLogin" id="SubmitLoginCus" type="submit" class="alt fancy-button"> <span class="fa fa-lightbulb-o"></span> {l s='Login' mod='dor_smartuser'}</button>
				</div>
				<div class="auth-dor-moreinfo clearfix">
					<p>* Denotes mandatory field.</p>
					<p>** At least one telephone number is required.</p>
				</div>
				<p class="submit">
					<input type="hidden" value="my-account" name="back" class="hidden">	
					<a href="#" onclick="return false" class="smartRegister reActLogReg"><i aria-hidden="true" class="fa fa-user-plus"></i> {l s='Register' mod='dor_smartuser'}</a>
					<a rel="nofollow" title="Recover your forgotten password" class="lost_password_smart" onclick="return false" href="#"><i aria-hidden="true" class="fa fa-key"></i> {l s='Forgot your password?' mod='dor_smartuser'}</a>
				</p>
			</div>
		</div>
	</form>
</div>

<div id="registerFormSmart">
	<span class="button b-close"><i class="fa-times fa"></i></span>
	<form class="std login-wrap text-center" id="account-creation_form" method="post" action="{$base_dir}login">
		<div class="account_creation">
			<h2 class="fsz-35 spcbtm-15"> 
				<img src="{$tpl_uri|escape:'html':'UTF-8'}img/logo_2.png" alt="{$shop_name|escape:'html':'UTF-8'}"> 
			</h2>
			<p class="fsz-20 title-3"> {l s='WELCOME TO OUR  WONDERFUL WORLD' mod='dor_smartuser'} </p>
			<p class="fsz-15 bold-font-4"> {l s='Did you know that we ship to over' mod='dor_smartuser'} <span class="thm-clr"> {l s='24 different countries' mod='dor_smartuser'} </span> </p>
			<div class="login-form">
				<a href="#" class="fb-btn btn spcbtm-15"> <i class="fa fa-facebook btn-icon"></i>{l s='Register with Facebook' mod='dor_smartuser'}</a>
				<div class="clearfix hidden">
					<label>{l s='Gender' mod='dor_smartuser'}</label>
					<br>
						<div class="radio-inline">
							<label class="top" data-for="id_gender1">
								<input type="radio" value="1" id="dor_id_gender1" name="id_gender">{l s='Mr.' mod='dor_smartuser'}
							</label>
						</div>
						<div class="radio-inline">
							<label class="top" data-for="id_gender2">
								<input type="radio" value="2" id="dor_id_gender2" name="id_gender">{l s='Mrs.' mod='dor_smartuser'}
							</label>
						</div>
				</div>
				<div class="required form-group hidden">
					<label data-for="customer_firstname" class="hidden">{l s='First name' mod='dor_smartuser'} <sup>*</sup></label>
					<input type="text" value="" name="customer_firstname" id="dor_customer_firstname" data-validate="isName" class="is_required validate form-control" onkeyup="$('#firstname').val(this.value);" placeholder="{l s='First name' mod='dor_smartuser'}">
				</div>
				<div class="required form-group hidden">
					<label data-for="customer_lastname" class="hidden">{l s='Last name' mod='dor_smartuser'} <sup>*</sup></label>
					<input type="text" value="" name="customer_lastname" id="dor_customer_lastname" data-validate="isName" class="is_required validate form-control" onkeyup="$('#lastname').val(this.value);" placeholder="{l s='Last name' mod='dor_smartuser'}">
				</div>
				<div class="required form-group">
					<label data-for="email_register_smart" class="hidden">{l s='Email' mod='dor_smartuser'} <sup>*</sup></label>
					<input type="email" value="" name="email" id="email_register_smart" data-validate="isEmail" class="is_required validate form-control" placeholder="{l s='Email' mod='dor_smartuser'}">
				</div>
				<div class="required password form-group">
					<label data-for="passwd" class="hidden">{l s='Password' mod='dor_smartuser'} <sup>*</sup></label>
					<input type="password" id="dor_passwd" name="passwd" data-validate="isPasswd" class="is_required validate form-control" placeholder="{l s='Password' mod='dor_smartuser'}">
					<span class="form_info hidden">{l s='(Five characters minimum)' mod='dor_smartuser'}</span>
				</div>
				
				<div class="checkbox reg-checkbox">
					<input type="checkbox" value="1" id="newsletter" name="newsletter">
					<label for="newsletter">{l s='Sign up for our newsletter!' mod='dor_smartuser'}</label>
				</div>
				<div class="checkbox reg-checkbox">
						<input type="checkbox" value="1" id="optin" name="optin">
						<label for="optin">{l s='Receive special offers from our partners!' mod='dor_smartuser'}</label>
				</div>
				<div class="form-group">
					<button class="alt fancy-button" id="submitAccountCus" name="submitAccount" type="submit">
						<i class="fa fa-user-plus"></i> {l s='Register' mod='dor_smartuser'}
					</button>
				</div>
				<p class="submit clearfix">
					<input type="hidden" value="0" name="email_create">
					<input type="hidden" value="1" name="is_new_customer">
					<input type="hidden" value="my-account" name="back" class="hidden">			
					<a href="#" onclick="return false" class="smartLogin reActLogReg"> <i class="fa fa-lightbulb-o"></i> {l s='Login' mod='dor_smartuser'}</a>
					<a rel="nofollow" title="Recover your forgotten password" class="lost_password_smart" onclick="return false" href="#"><i aria-hidden="true" class="fa fa-key"></i> {l s='Forgot your password?' mod='dor_smartuser'}</a>
				</p>
			</div>
		</div>
						

	</form>
</div>
<div id="smartForgotPass">
	<span class="button b-close"><i class="fa-times fa"></i></span>
	<div class="center_column" id="center_column_smart">
		<div class="box- login-wrap text-center">
			<h2 class="fsz-35 spcbtm-15"> 
				<img src="{$tpl_uri|escape:'html':'UTF-8'}img/logo_2.png" alt="{$shop_name|escape:'html':'UTF-8'}"> 
			</h2>
			<p class="fsz-20 title-3"> {l s='WELCOME TO OUR  WONDERFUL WORLD' mod='dor_smartuser'} </p>
			<p class="fsz-15 bold-font-4"> {l s='Did you know that we ship to over' mod='dor_smartuser'} <span class="thm-clr"> {l s='24 different countries' mod='dor_smartuser'} </span> </p>
			<h1 class="page-subheading hidden">{l s='Forgot your password?' mod='dor_smartuser'}</h1>
			<p>{l s='Please enter the email address you used to register. We will then send you a new password.' mod='dor_smartuser'} </p>
			<div class="login-form">
				<form id="dor_form_forgotpassword" class="std" method="post" action="/en/password-recovery">
					<fieldset>
						<div class="form-group">
							<label data-for="email_forgot_smart" class="hidden">{l s='Email address' mod='dor_smartuser'}</label>
							<input type="email" value="" placeholder="{l s='Email' mod='dor_smartuser'}" name="email" id="email_forgot_smart" class="form-control">
						</div>
						<div class="form-group">
							<button class="alt fancy-button" id="Retrieve-Passwrd" type="submit"><i class="fa fa-key" aria-hidden="true"></i> {l s='Retrieve Password' mod='dor_smartuser'}</button>
						</div>
						<p class="submit">
				            <a href="#" onclick="return false" class="smartLogin reActLogReg"><i class="fa fa-lightbulb-o"></i> {l s='Login' mod='dor_smartuser'}</a>
				            <a href="#" onclick="return false" class="smartRegister reActLogReg"><i aria-hidden="true" class="fa fa-user-plus"></i> {l s='Register' mod='dor_smartuser'}</a>
						</p>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>


{/if}