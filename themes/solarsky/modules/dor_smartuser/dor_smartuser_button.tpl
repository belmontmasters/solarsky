
<!-- Block user information module NAV  -->
{if $logged}
<div class="header_user_info smart-user-act">
	<a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow"><span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a>
	&nbsp;&nbsp;
</div>
{/if}

<div class="header_user_info smart-user-act">
	{if $logged}
		<a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">{l s='Logout' mod='blockuserinfo'}</a>&nbsp;&nbsp;
	{else}
		<a href="#" onclick="return false" class="smartLogin">{l s='Login' mod='dor_smartuser'}</a><span class="hidden"> Or </span>
		<a href="#" onclick="return false" class="smartRegister hidden">{l s='Register' mod='dor_smartuser'}</a>
	{/if}
	&nbsp;&nbsp;
</div>