<!-- Latest News -->
{if isset($view_data) AND !empty($view_data)}
    {assign var='i' value=1}
    <section class="dorButtonArrow row-latest-news col-lg-12 col-md-12 col-sm-12 col-xs-12" id="last-news-theme2">
        <div class="container theme-container">
            <div class="gst-column">
                <div class="fancy-heading head-tab-lists">
                    <h3 class="title-layout3-dor">{l s='Blog & News' mod='smartbloghomelatestnews'}</h3>
                </div>
                <div class="gst-post-list">
                    {foreach from=$view_data item=post}
                    {assign var="catOptions" value=null}
                    {assign var="options" value=null}
                    {$options.id_post = $post.id}
                    {$options.slug = $post.link_rewrite}
                    {$catOptions.id_category = $post.category}
                    {$catOptions.slug = $post.category_link_rewrite}
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-home-last">
                        <div class="main-content-item-news-home">
                            <div>
                                <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">
                                    <img src="{$post.thumb_image}" alt="" />
                                </a>
                                <div class="media-item-blog clearfix">
                                    <h3 class="entry-title">
                                        <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}">{$post.title|truncate:35:'...'|escape:'htmlall':'UTF-8'}</a>
                                    </h3>
                                    <div class="entry-meta media-line">
                                        <span class="entry-time-date h-item-line"><i aria-hidden="true" class="fa fa-clock-o"></i>{$post.date_added|date_format:"%e %B %Y"}</span>
                                        <a href="{smartblog::GetSmartBlogLink('smartblog_post',$options)}#comments" class="comments-link h-item-line">
                                            <i class="fa fa-comment"></i>
                                            {$post.totalcomment}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {$i=$i+1}
                {/foreach}
                </div>
            </div>
        </div>
    </section>
{/if}
<!-- Latest News -->