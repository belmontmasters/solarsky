
<nav class="dor-megamenu col-lg-9 col-sx-9 col-sm-9">
    <div class="navbar navbar-default " role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle open_menu">
                <span class="sr-only">{l s='Toggle navigation' mod='dormegamenu'}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="dor-top-menu" class="collapse navbar-collapse navbar-ex1-collapse">
            <div class="close_menu" style="display:none;">
                <span class="btn-close"><i class="fa fa-angle-left"></i></span>
            </div>
            {$output}{* HTML, can not escape *}
        </div>
    </div>  
</nav>