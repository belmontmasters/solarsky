{if $page_name =='index'}
	{if isset($homeslider_slides)}
	<div class="row">
		{if isset($homeslider_slides.0) && isset($homeslider_slides.0.sizes.1)}{capture name='height'}{$homeslider_slides.0.sizes.1}{/capture}{/if}
		<div id="Dor_Full_Slider" style="width: 1300px; height: {$homeslider.height}px;">
		    <!-- Loading Screen -->
		    <div class="slider-loading" data-u="loading" style="position: absolute; top: 0px; left: 0px;">
		        <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
		        <div class="slider-loading-img"></div>
		    </div>
		    <div class="slider-content-wrapper" data-u="slides">
		    	{foreach from=$homeslider_slides key=i item=slide}
		    		{if ($i+1)%2 == 0}
		    			{assign var='nbItem' value=0}
		    		{else}
		    			{assign var='nbItem' value=1}
		    		{/if}
					{if $slide.active}
			        <div class="slider-content item-slider-{$i+1}" data-p="225.00" style="display:none;">
			            <img data-u="image" src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`dor_homeslider/images/`$slide.image|escape:'htmlall':'UTF-8'`")}"{if isset($slide.size) && $slide.size} {$slide.size}{else} width="100%" height="100%"{/if} alt="{$slide.legend|escape:'htmlall':'UTF-8'}" />
			            <div class="dor-info-perslider">
			            	<div class="container">
					            {if $slide.description}
					            <div class="dor-slider-desc" data-u="caption" data-t="{if $i==3}15{else}10{/if}">{$slide.description}</div>
					            {/if}
					            {if $slide.txtReadmore1 || $slide.txtReadmore2}
					            <div class="slider-read-more read-more-{$i}" data-u="caption" data-t="{if $nbItem==0}5{else}5{/if}">
					            	{if $slide.txtReadmore1}<a href="{$slide.UrlReadmore1}" class="fancy-btn fsz-16">{$slide.txtReadmore1}</a>{/if}
					            	{if $slide.txtReadmore2}<a href="{$slide.UrlReadmore2}" class="fancy-btn fsz-16">{$slide.txtReadmore2}</a>{/if}
					            </div>
					            {/if}
			            	</div>
			            </div>
			        </div>
			        {/if}
			        
				{/foreach}
		    </div>
		    <!-- Bullet Navigator -->
		    <div data-u="navigator" class="dorNavSlider" style="bottom:16px;right:16px;" data-autocenter="1">
		        <!-- bullet navigator item prototype -->
		        <div data-u="prototype" style="width:16px;height:16px;"></div>
		    </div>
		    <!-- Arrow Navigator -->
		    <span data-u="arrowleft" class="dorArrowLeft" style="" data-autocenter="2"><i class="fa fa-angle-left"></i></span>
		    <span data-u="arrowright" class="dorArrowRight" style="" data-autocenter="2"><i class="fa fa-angle-right"></i></span>
		</div>
	</div>
	{/if}
{/if}