{if isset($homeslider)}
    {addJsDef homeslider_loop=$homeslider.loop|intval}
    {addJsDef homeslider_width=$homeslider.width|intval}
    {addJsDef homeslider_speed=$homeslider.speed|intval}
    {addJsDef homeslider_pause=$homeslider.pause|intval}
    {addJsDef homeslider_arrow=$homeslider.arrow|intval}
    {addJsDef homeslider_nav=$homeslider.nav|intval}
    {addJsDef homeslider_pause_hover=$homeslider.pause_hover|intval}
{/if}