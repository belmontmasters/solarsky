{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if (isset($right_column_size) && !empty($right_column_size)) && $dorCategoryCols == "proCateCol3"}
						{capture name='displayDorRightColumn'}{hook h='displayDorRightColumn'}{/capture}
						{if ($page_name|escape:'html':'UTF-8' == 'product' && !$smarty.capture.displayDorRightColumn) || ($page_name|escape:'html':'UTF-8' != 'product')}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">
						{if $page_name|escape:'html':'UTF-8' == 'category'}
						<h3 class="sec-title fsz-25 no-mrgn blk-clr hidden">{l s='FILTER BY'}</h3>
						{/if}
						{$HOOK_RIGHT_COLUMN}</div>
						{/if}
					{/if}
					{if $page_name|escape:'html':'UTF-8' == 'product' && $dorDetailCols == 'proDetailCol2'}
						{capture name='displayDorRightColumn'}{hook h='displayDorRightColumn'}{/capture}
						{if $smarty.capture.displayDorRightColumn}
							<div id="dor_right_column" class="col-xs-12 col-sm-3 column">
								{$smarty.capture.displayDorRightColumn}
							</div>
						{/if}
					{/if}
					{if ($page_name|escape:'html':'UTF-8' == 'dorSmartBlogs' && $dorBlogsCols == 'proBlogCol3') || ($page_name|escape:'html':'UTF-8' == 'dorSmartBlogsDetail' && $dorBlogsDetailCols == 'proBlogDetailCol3')}
						{capture name='displaySmartBlogRight'}{hook h='displaySmartBlogRight'}{/capture}
						{if $smarty.capture.displaySmartBlogRight}
							<div id="right_column_blog" class="col-xs-12 col-sm-3 column">
								{$smarty.capture.displaySmartBlogRight}
							</div>
						{/if}
					{/if}

					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='blockDorado2'}{hook h='blockDorado2'}{/capture}
				{if $smarty.capture.blockDorado2}
					<div class="blockDorado2 blockPosition {if $dorCategoryEffect != ''}dor-list-effect-pizza{$dorCategoryEffect}{else}dor-list-effect-pizza1{/if}">
						<div class="container">
							<div class="row">
							{$smarty.capture.blockDorado2}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='blockDorado4'}{hook h='blockDorado4'}{/capture}
				{if $smarty.capture.blockDorado4}
					<div class="blockDorado4 blockPosition {if $dorCategoryEffect != ''}dor-list-effect-pizza{$dorCategoryEffect}{else}dor-list-effect-pizza1{/if}">
						<div class="container">
							<div class="row">
								{$smarty.capture.blockDorado4}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			
			
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='blockDorado5'}{hook h='blockDorado5'}{/capture}
				{if $smarty.capture.blockDorado5}
					<div class="blockDorado5 blockPosition {if $dorCategoryEffect != ''}dor-list-effect-pizza{$dorCategoryEffect}{else}dor-list-effect-pizza1{/if}">
						<div class="container">
							<div class="row">
								{$smarty.capture.blockDorado5}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='blockDorado6'}{hook h='blockDorado6'}{/capture}
				{if $smarty.capture.blockDorado6}
					<div class="blockDorado6 blockPosition {if $dorCategoryEffect != ''}dor-list-effect-pizza{$dorCategoryEffect}{else}dor-list-effect-pizza1{/if}">
						<div class="container">
							<div class="row">
								{$smarty.capture.blockDorado6}
							</div>
						</div>
					</div>
				{/if}
			{/if}

			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='DorTestimonial'}{hook h='DorTestimonial'}{/capture}
				{if $smarty.capture.DorTestimonial}
					<div class="DorTestimonial blockPosition {if $dorCategoryEffect != ''}dor-list-effect-pizza{$dorCategoryEffect}{else}dor-list-effect-pizza1{/if}">
						<div class="container">
							<div class="row">
								{$smarty.capture.DorTestimonial}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='DorSpecialProducts'}{hook h='DorSpecialProducts'}{/capture}
				{if $smarty.capture.DorSpecialProducts}
					<div class="DorSpecialProducts blockPosition {if $dorCategoryEffect != ''}dor-list-effect-pizza{$dorCategoryEffect}{else}dor-list-effect-pizza1{/if}">
						<div class="container">
							<div class="row">
								{$smarty.capture.DorSpecialProducts}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='blockDorado7'}{hook h='blockDorado7'}{/capture}
				{if $smarty.capture.blockDorado7}
					<div class="blockDorado7 blockPosition">
						<div class="container">
							<div class="row">
								{$smarty.capture.blockDorado7}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='blockPosition4'}{hook h='blockPosition4'}{/capture}
				{if $smarty.capture.blockPosition4}
					<div class="blockPosition4 blockPosition {if $dorCategoryEffect != ''}dor-list-effect-pizza{$dorCategoryEffect}{else}dor-list-effect-pizza1{/if}">
						<div class="container">
							<div class="row">
								{$smarty.capture.blockPosition4}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='DorHomeLatestNews'}{hook h='DorHomeLatestNews'}{/capture}
				{if $smarty.capture.DorHomeLatestNews}
					<div class="dorHomeLatestNews blockPosition dor-bg-white {if $dorCategoryEffect != ''}dor-list-effect-pizza{$dorCategoryEffect}{else}dor-list-effect-pizza1{/if}">
						<div class="container">
							<div class="row">
								{$smarty.capture.DorHomeLatestNews}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			{if $page_name|escape:'html':'UTF-8' == 'contact'}
			<section id="map-canvas2"></section>
			{/if}
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='dormanufacturer'}{hook h='dormanufacturer'}{/capture}
				{if $smarty.capture.dormanufacturer}
					<div class="dormanufacturer blockPosition">
						<div class="container">
							<div class="row">
								{$smarty.capture.dormanufacturer}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='blockDorado8'}{hook h='blockDorado8'}{/capture}
				{if $smarty.capture.blockDorado8}
				<div class="blockDorado8 blockDorado8 clearfix">
					<div class="container">
						<div class="row">
						{$smarty.capture.blockDorado8}
						</div>
					</div>
				</div>
				{/if}
			{/if}
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='blockDorado9'}{hook h='blockDorado9'}{/capture}
				{if $smarty.capture.blockDorado9}
				<div class="blockDorado9 blockDorado9 dor-bg-white clearfix">
					<div class="container">
						<div class="row">
						{$smarty.capture.blockDorado9}
						</div>
					</div>
				</div>
				{/if}
			{/if}
			{if $page_name|escape:'html':'UTF-8' != 'index'}
				{capture name='dorNewsletter'}{hook h='dorNewsletter'}{/capture}
				{if $smarty.capture.dorNewsletter}
					<div class="dorNewsletter gst-row gst-color-white row-newsletter ovh">
						<div class="container">
							<div class="row">
								{$smarty.capture.dorNewsletter}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			{capture name='dorBlockCustom1'}{hook h='dorBlockCustom1'}{/capture}
			{if $smarty.capture.dorBlockCustom1}
			<div class="dorBlockCustom1 clearfix">
				<div class="container">
					<div class="row">
					{$smarty.capture.dorBlockCustom1}
					</div>
				</div>
			</div>
			{/if}
			{capture name='blockDorado10'}{hook h='blockDorado10'}{/capture}
			{if $smarty.capture.blockDorado10}
			<div class="blockDorado10 clearfix">
				<div class="container">
					<div class="row">
					{$smarty.capture.blockDorado10}
					</div>
				</div>
			</div>
			{/if}
			{if isset($dorFooterSkin) && $dorFooterSkin != ''}
				{include file="$dorFooterSkin"}
			{else}
			<!-- Footer -->
				{if isset($HOOK_FOOTER)}
				<div class="footer-container">
					<footer id="footer"  class="container">
						<div class="row">
							{capture name='blockDoradoFooter'}{hook h='blockDoradoFooter'}{/capture}
							{if $smarty.capture.blockDoradoFooter}
								{$smarty.capture.blockDoradoFooter}
							{/if}
							{$HOOK_FOOTER}
						</div>
					</footer>
				</div><!-- #footer -->
				{/if}
			{/if}
			{if !$logged}
				{capture name='dorSmartuser_'}{hook h='dorSmartuser_'}{/capture}
				{if $smarty.capture.dorSmartuser_}
					<div class="dorSmartuser">
						<div class="container">
							<div class="row">
								{$smarty.capture.dorSmartuser_}
							</div>
						</div>
					</div>
				{/if}
			{/if}
			{if $page_name|escape:'html':'UTF-8' == 'index'}
				{capture name='dorthemeoptions'}{hook h='dorthemeoptions'}{/capture}
				{if $smarty.capture.dorthemeoptions}
					{$smarty.capture.dorthemeoptions}
				{/if}
			{/if}
		</div><!-- #page -->
{/if}
{if $page_name|escape:'html':'UTF-8' == 'index' && $dorSubsPop != 0}
	{include file="$tpl_dir./dor-subscribe-popup.tpl"}
{/if}
<div id="to-top" class="to-top" style="bottom: 0px;"> <i class="fa fa-long-arrow-up"></i> </div>
{if $dorOptReload == 1}
<div class="dor-page-loading">
  	<div id="loader"></div>
  	<div class="loader-section section-left"></div>
  	<div class="loader-section section-right"></div>
</div>
{/if}
{include file="$tpl_dir./global.tpl"}
{addJsDefL name=max_item}{l s='You cannot add more than %d product(s) to the product comparison' sprintf=$comparator_max_item js=1}{/addJsDefL}
{addJsDef comparator_max_item=$comparator_max_item}
{addJsDef comparedProductsIds=$compared_products}
{addJsDef floatHeader=$dorFloatHeader}
{if $page_name|escape:'html':'UTF-8' == 'contact'}
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="{$tpl_uri|escape:'html':'UTF-8'}js/jquery.gmap.js"></script>
<script type="text/javascript" src="{$tpl_uri|escape:'html':'UTF-8'}js/map.js"></script>
{/if}
{if $page_name|escape:'html':'UTF-8' == 'contact'}
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
{/if}
<script src="{$tpl_uri|escape:'html':'UTF-8'}js/dor.script.js"></script>

	</body>
</html>