jQuery(".dor-page-loading").show();
window.onload = function () { 
	setTimeout(function(){
		jQuery(".dor-page-loading").remove();
	},500);
}
var DORSCRIPT = {
	init:function(){
		DORSCRIPT.Testimonial();
		DORSCRIPT.EventActionProduct();
		DORSCRIPT.RelatedProduct();
		DORSCRIPT.HomeCategoryProduct();
		DORSCRIPT.PaymentSlider();
		DORSCRIPT.OptShowItems();
		DORSCRIPT.SubmitFormBase();
		DORSCRIPT.MobileLinkTop();
		DORSCRIPT.SlidePictureBlock();
		DORSCRIPT.HotDeal();
		DORSCRIPT.NewArrivals();
		DORSCRIPT.BlockHover();
		DORSCRIPT.SlideImageTabs();
		DORSCRIPT.BrandsLists();
		DORSCRIPT.PartnersLists();
		DORSCRIPT.HomeLastNews();
		DORSCRIPT.ResizeTodayDealWindow();
		DORSCRIPT.FeaturedCarousel();
		DORSCRIPT.CategoryCarousel();
		DORSCRIPT.OptionHeadOct();
		DORSCRIPT.LoadMoreProduct();
		DORSCRIPT.ClickLoadPage();
		//DORSCRIPT.AutoScrollLoad();
		//DORSCRIPT.scroll_to();
		
		/*jQuery("#currencies-block-top").detach().appendTo('.sliding-content .container');
		jQuery("#languages-block-top").detach().appendTo('.sliding-content .container');*/
		//jQuery(".nbrItemPage").detach().prependTo('#left_column');
		//jQuery("#productsSortForm").detach().prependTo('#left_column');


		

		jQuery("#currencies-block-top, #languages-block-top").show();
		var checkLeftCol = jQuery("#left_column").html();
		var checkRightCol = jQuery("#right_column").html();
		if(typeof checkLeftCol != "undefined"){
			jQuery(".nbrItemPage").detach().prependTo('#left_column');
			jQuery("#productsSortForm").detach().prependTo('#left_column');
		}
		if(typeof checkRightCol != "undefined"){
			jQuery(".nbrItemPage").detach().prependTo('#right_column');
			jQuery("#productsSortForm").detach().prependTo('#right_column');
		}

		
		jQuery(".dor-search-button").click(function() {
		  $( "#searchbox" ).slideToggle( 100, function() {
		    	var isVisible = $( "#searchbox" ).is( ":visible" );
		    	/*if(isVisible){
		    		jQuery(".dor-toggle").addClass("open");
		    	}else{
		    		jQuery(".dor-toggle").removeClass("open");
		    	}*/
		  });
		});

		jQuery(".dor-toggle").click(function() {
		  $( ".sliding-content" ).slideToggle( "fast", function() {
		    	var isVisible = $( ".sliding-content" ).is( ":visible" );
		    	if(isVisible){
		    		jQuery(".dor-toggle").addClass("open");
		    	}else{
		    		jQuery(".dor-toggle").removeClass("open");
		    	}
		  });
		});

		jQuery(".fa-icon-menu").click(function(){
			$( ".dor-verticalmenu" ).slideToggle( "fast", function() {
		    	var isVisible = $( ".dor-verticalmenu" ).is( ":visible" );
		    	
		  	});
		});
		var checkCompare = parseInt(jQuery(".compare-product-sliding > span").text());
		if((typeof comparedProductsIds != "undefined" && comparedProductsIds.length > 0) || checkCompare > 0){
			jQuery(".compare-product-sliding > span").text(comparedProductsIds.length);
			jQuery(".compare-product-sliding").click(function(){
				jQuery(".bt_compare").click();
				window.location.href=baseUri+"products-comparison";
			});
		}else{
			jQuery(".compare-product-sliding").mouseover(function() {
				var offset = jQuery(this).offset();
				var checkCompare = parseInt(jQuery(".compare-product-sliding > span").text());
				if(checkCompare == 0){
					jQuery(".compare-popup").css({
				    	'left':offset.left,
				    	'opacity':1,
				    	'visibility':'visible'
				    });
				}else{
					jQuery(".compare-product-sliding").click(function(){
						jQuery(".bt_compare").click();
					});
				}
			    
			}).mouseout(function() {
			    jQuery(".compare-popup").css({
			    	'opacity':0,
			    	'visibility':'hidden'
			    });
			});
		}
		
		var idObjPage = jQuery("body").attr("id");
		if(idObjPage != 'index'){
			jQuery(".dor-verticalmenu.block_content").hide();
		}
		if(idObjPage == "category"){
			if(parseInt($( window ).width()) >=1628){
				DORSCRIPT.AutoScrollLoad(2);
			}else{
				DORSCRIPT.AutoScrollLoad(0);
			}
			if(parseInt($( window ).width()) <= 767){
			  	jQuery("#left_column").detach().insertAfter('#center_column');
			}
			DORSCRIPT.ResizeCategoryWindow();
		}
		if(idObjPage == "product"){
			if(parseInt($( window ).width()) <= 767){
			  	jQuery("#dor_left_column").detach().insertAfter('#center_column');
			}
			DORSCRIPT.DorResizeCategoryWindow();
		}
		
		if(floatHeader != undefined && parseInt(floatHeader) == 1){
			DORSCRIPT.ScrollFixedMenu();
		}
		
        jQuery('.to-top').click(function () {
            jQuery('html, body').animate({scrollTop: 0}, 800);
            return false;
        });
        var localStorPopup = localStorage.getItem("popupScrAgain");
        if(localStorPopup == null && parseInt(localStorPopup) != 1 && typeof sessionStorage['popupScr'] == "undefined" && sessionStorage['popupScr'] != 1){
        	DORSCRIPT.ShowPopupScre();
        }
        if(typeof sessionStorage['popupScr'] == "undefined" && sessionStorage['popupScr'] != 1){
        	sessionStorage['popupScr'] = 1;
        }
        jQuery("input[name='notShowSubs']").change(function(){
        	if($(this).is(':checked')){
        		localStorage.setItem("popupScrAgain",1);
        	}else{
        		localStorage.removeItem("popupScrAgain");
        	}
        });
	},

	LoadMoreProduct:function(){
		localStorage.removeItem("dorpage");
		jQuery(".productLoadMore").click(function(){
			var urlAjax = baseUri+'modules/dorcategoryload/ajax.php';
			var cateID = jQuery("input[name='id_category']").val();
			var np = jQuery("select#nb_item").val();
			var params = {}
			params.cateID = parseInt(cateID);
			params.limit = parseInt(np);
			params.page = 2;
			jQuery.ajax({
	            url: urlAjax,
	            data:params,
	            type:"POST",
	            success:function(data){
	            	var results = JSON.parse(data);
	            	jQuery("#category #center_column .product_list").append(results);
	            }
	        });
		});
	},
	ClickLoadPage:function(){
		var loading2 = false;
		jQuery(".productLoadMore").unbind("click");
		jQuery(".productLoadMore").click(function(){
			var page = localStorage.getItem("dorpage");
	    	if(page == null){
	    		page = 2;
	    	}
	    	if(page != null){
	    		if(loading2 == false){
		    		loading2 = true;
					var checkLoad = jQuery(".cate-loadmore").html();
		    		if(typeof checkLoad != "undefined"){
				    	var urlAjax = baseUri+'modules/dorcategoryload/ajax.php';
						var cateID = jQuery("input[name='id_category']").val();
						var np = jQuery("select#nb_item").val();
						page = parseInt(page);
						var params = {}
						params.cateID = parseInt(cateID);
						params.limit = parseInt(np);
						params.page = page;
						jQuery.ajax({
				            url: urlAjax,
				            data:params,
				            type:"POST",
				            success:function(data){
				            	loading2 = false;
				            	var results = JSON.parse(data);
				            	var lengthVal = jQuery(results).find(".product-container").length;
				            	if(lengthVal < parseInt(np)){
				            		jQuery(".cate-loadmore").remove();
				            	}
				            	jQuery("#category #center_column .product_list").append(results);
				            	localStorage.setItem("dorpage",page+1);
				            }
				        });
			        }
		    		
				}
	    	}
		});
	},
	AutoScrollLoad:function(page){
		var loading  = false;
		$(window).scroll(function() {
			//console.log("height1=",$(document).height() - 800)
			//console.log("height2=",$(window).scrollTop() + $(window).height())
		    if($(window).scrollTop() + $(window).height() >= $(document).height() - 800) {
		    	var page = localStorage.getItem("dorpage");
		    	if(page == null){
		    		page = 2;
		    	}
		    	if(page != null){
		    		if(loading == false){
			    		loading = true;
						var checkLoad = jQuery(".cate-loadmore").html();
			    		if(typeof checkLoad != "undefined"){
					    	var urlAjax = baseUri+'modules/dorcategoryload/ajax.php';
							var cateID = jQuery("input[name='id_category']").val();
							var np = jQuery("select#nb_item").val();
							page = parseInt(page);
							var params = {}
							params.cateID = parseInt(cateID);
							params.limit = parseInt(np);
							params.page = page;
							jQuery.ajax({
					            url: urlAjax,
					            data:params,
					            type:"POST",
					            success:function(data){
					            	loading = false;
					            	var results = JSON.parse(data);
					            	var lengthVal = jQuery(results).find(".product-container").length;
					            	if(lengthVal < parseInt(np)){
					            		jQuery(".cate-loadmore").remove();
					            	}
					            	jQuery("#category #center_column .product_list").append(results);
					            	localStorage.setItem("dorpage",page+1);
					            }
					        });
				        }
			    		
					}
		    	}
		    	
		    }
		});
		if(page==2){
			DORSCRIPT.ScrollDataLoad(page, loading);
		}
	},
	ScrollDataLoad:function(page, loading){
		//console.log("loading=",loading);
		if(loading == false){
    		loading = true;
			var checkLoad = jQuery(".cate-loadmore").html();
    		if(typeof checkLoad != "undefined"){
		    	var urlAjax = baseUri+'modules/dorcategoryload/ajax.php';
				var cateID = jQuery("input[name='id_category']").val();
				var np = jQuery("select#nb_item").val();
				page = parseInt(page);
				var params = {}
				params.cateID = parseInt(cateID);
				params.limit = parseInt(np);
				params.page = page;
				jQuery.ajax({
		            url: urlAjax,
		            data:params,
		            type:"POST",
		            success:function(data){
		            	loading = false;
		            	var results = JSON.parse(data);
		            	var lengthVal = jQuery(results).find(".product-container").length;
		            	if(lengthVal < parseInt(np)){
		            		jQuery(".cate-loadmore").remove();
		            	}
		            	jQuery("#category #center_column .product_list").append(results);
		            	localStorage.setItem("dorpage",page+1);
		            }
		        });
	        }
    		
		}
	},
	scroll_to:function(){
		$('html, body').animate({
			scrollTop: $(".cate-loadmore").offset().top
		},1000);
	},

	AboutTestimonial:function(){
		
	},
	ShortcodeTestimonial:function(){
		
	},
	ShorcodeBrands:function(){
		
	},
	OptionHeadOct:function(){
		var checkcurr = jQuery("#currencies-block-top").html();
		var checklang = jQuery("#languages-block-top").html();
		if(typeof checkcurr != "undefined"){
			jQuery("#currencies-block-top").detach().appendTo('.option-oct .main-oct');
			jQuery("#currencies-block-top").show();
		}
		if(typeof checklang != "undefined"){
			jQuery("#languages-block-top").detach().appendTo('.option-oct .main-oct');
			jQuery("#languages-block-top").show();
		}
	},
	FeaturedCarousel:function(){
		var idCate = jQuery(".featured-tab-content").attr("data-cate-id");
		if(typeof idCate != 'undefined'){
			$('.productFeaturedTabContent_'+idCate+' .product_list').owlCarousel({
		        items: 7,
		        loop: true,
		        navigation: true,
		        nav: true,
		        autoplay: false,
		        margin:10,
		        responsive: {
		            0: {items: 1},
		            1628: {items: 7},
		            1396: {items: 6},
		            1200: {items: 5},
		            992: {items: 4},
		            650: {items: 3},
		            400: {items: 2},
		            300: {items: 1}
		        },
		        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
		    });

			var widthOwl = parseInt(jQuery(".dor-featured .owl-stage").width());
			jQuery(".dor-featured .owl-stage").width(widthOwl+0.9);
		}
		$( window ).resize(function() {
		  var widthOwl = parseInt(jQuery(".dor-featured .owl-stage").width());
		  jQuery(".dor-featured .owl-stage").width(widthOwl+0.9);
		});
	},
	CategoryCarousel:function(){
		var checklist1 = jQuery("#dorTabListCategoryContent .product_list > li").length;
		if(checklist1 > 0){
			$('#dorTabListCategoryContent .product_list').owlCarousel({
		        items: 5,
		        loop: true,
		        navigation: true,
		        nav: true,
		        autoplay: false,
		        responsive: {
		            0: {items: 1},
		            1628: {items: 7},
		            1396: {items: 6},
		            1200: {items: 5},
		            992: {items: 4},
		            650: {items: 3},
		            400: {items: 2},
		            300: {items: 1}
		        },
		        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
		    });
		}
		var checklist2 = jQuery("#dorTabListCategoryContent2 .product_list > li").length;
		if(checklist2 > 0){
			$('#dorTabListCategoryContent2 .product_list').owlCarousel({
		        items: 5,
		        loop: true,
		        navigation: true,
		        nav: true,
		        autoplay: false,
		        responsive: {
		            0: {items: 1},
		            1628: {items: 7},
		            1396: {items: 6},
		            1200: {items: 5},
		            992: {items: 4},
		            650: {items: 3},
		            400: {items: 2},
		            300: {items: 1}
		        },
		        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
		    });
		}
		var checklist3 = jQuery("#dorTabListCategoryContent3 .product_list > li").length;
		if(checklist3 > 0){
			$('#dorTabListCategoryContent3 .product_list').owlCarousel({
		        items: 5,
		        loop: true,
		        navigation: true,
		        nav: true,
		        autoplay: false,
		        responsive: {
		            0: {items: 1},
		            1628: {items: 7},
		            1396: {items: 6},
		            1200: {items: 5},
		            992: {items: 4},
		            650: {items: 3},
		            400: {items: 2},
		            300: {items: 1}
		        },
		        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
		    });
		}

		var owl = $(".lists_category");
            owl.owlCarousel({
            	items: 5,
		        loop: true,
		        navigation: true,
		        nav: true,
		        autoplay: false,
		        margin:10,
                responsive: {
		            0: {items: 1},
		            1628: {items: 7},
		            1396: {items: 6},
		            1200: {items: 5},
		            992: {items: 4},
		            650: {items: 3},
		            400: {items: 2},
		            300: {items: 1}
		        },
		        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
            });
            var widthOwl = parseInt(jQuery(".lists_category .owl-stage").width());
			jQuery(".lists_category .owl-stage").width(widthOwl+0.9);
	},
	ResizeTodayDealWindow:function(){
		var idObjPage = jQuery("body").attr("id");
		if(idObjPage == "index"){
			if(parseInt($( window ).width()) <= 767){
			  	$('.promotion-banner').owlCarousel({
			        items: 1,
			        loop: true,
			        navigation: true,
			        nav: true,
			        autoplay: true,
			        responsive: {
			            0: {items: 1},
			            1200: {items: 1},
			            1165: {items: 1},
			            990: {items: 1},
			            700: {items: 1},
			            600: {items: 1},
			            480: {items: 1},
			            320: {items: 1}
			        },
			        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
			    });
			}
		}
		$( window ).resize(function() {
		  var widthW = $( window ).width();
		  if(parseInt(widthW) <= 767){
		  	if($('.promotion-banner').hasClass('itemOwlCarousel')){
		  		var htmlPromo = jQuery(".promotion-banner > .owl-stage-outer").html();
		  		jQuery(".promotion-banner > .owl-stage-outer").remove();
		  		jQuery(".promotion-banner").html(htmlPromo);
		  	}
		  	$('.promotion-banner').owlCarousel({
		        items: 1,
		        loop: true,
		        navigation: true,
		        nav: true,
		        autoplay: true,
		        responsive: {
		            0: {items: 1},
		            1200: {items: 1},
		            1165: {items: 1},
		            990: {items: 1},
		            700: {items: 1},
		            600: {items: 1},
		            480: {items: 1},
		            320: {items: 1}
		        },
		        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
		    });
		  }else{
		  	try {
		  		$owl = $('.promotion-banner');
		  		if(typeof $owl.data('owlCarousel') != 'undefined') {
				    $owl.data('owlCarousel').destroy();
				    $owl.removeClass('owl-carousel owl-loaded');
				    $owl.find('.owl-stage-outer').children().unwrap();
				    $owl.removeData();
				}
			}
			catch(err) {
			    
			}
		  }
		});
	},
	ResizeCategoryWindow:function(){
		$( window ).resize(function() {
		  var widthW = $( window ).width();
		  if(parseInt(widthW) <= 767){
		  	jQuery("#left_column").detach().insertAfter('#center_column');
		  }else{
		  	jQuery("#left_column").detach().insertBefore('#center_column');
		  }
		});
	},
	DorResizeCategoryWindow:function(){
		$( window ).resize(function() {
		  var widthW = $( window ).width();
		  if(parseInt(widthW) <= 767){
		  	jQuery("#dor_left_column").detach().insertAfter('#center_column');
		  }else{
		  	jQuery("#dor_left_column").detach().insertBefore('#center_column');
		  }
		});
	},
	ScrollTop:function(){
		jQuery(window).scroll(function () {
			if (jQuery(this).scrollTop() > 100) {
				jQuery("#back-top").remove();
				jQuery("body").append("<span id='back-top'><i class='fa fa-chevron-up'></i></span>");
				jQuery('#back-top').fadeIn();
			} else {
				jQuery('#back-top').fadeOut();
				jQuery("#back-top").remove();
			}
			jQuery('#back-top').click(function () {
				jQuery('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});
	},
	CheckLoginReview:function(){
		if( typeof isLogged != "undefined" && parseInt(isLogged) == 0){
			jQuery(".new_comment_form_content #comment_title").attr("disabled","disabled");
			jQuery(".new_comment_form_content #content").attr("disabled","disabled");
			jQuery("#submitNewMessage").click(function(){
				jQuery(".smartLogin").click();
				return false;
			});
		}
	},
	MoveHtmlBlock:function(){
		var checkDorLeftHtml = jQuery(".dor-custom-html-left").html();
		var checkDorRightHtml = jQuery(".dor-custom-html-right").html();
		if(typeof checkDorLeftHtml != "undefined"){
			jQuery(".dor-custom-html-left").appendTo("#dor_left_column").show();

		}
		if(typeof checkDorRightHtml != "undefined"){
			jQuery(".dor-custom-html-right").appendTo("#dor_right_column").show();
		}
	},
	HoverCart:function(){
		jQuery(".shopping_cart > a").hover(function(){
			jQuery("#header .cart_block").show(100);
		})
	},
	ScrollFixedMenu:function(){
		/***Dorado Theme Custom***/
		var checkPage = jQuery("body").attr("id");
		var checkHeader0Page = jQuery("#page").hasClass("noneHeaderSkin");
		var checkHeader1Page = jQuery("#page").hasClass("headerskin1");
		if(checkPage == "index" && (checkHeader0Page == true || checkHeader1Page == true)){
			var nScreen = parseInt($("#Dor_Full_Slider").height());
			n = nScreen+50;
		}else{
			n = 150;
		}
		$(window).bind('scroll', function() {
	     var navHeight = n;
	       if ($(window).scrollTop() > navHeight) {
	         $('#header').addClass('fixed fixed-tran');
	         var checkLogoFix = jQuery(".logoFixed").html();
	         if(jQuery.trim(checkLogoFix).length == 0){
	          var logo = jQuery(".logo.img-responsive").attr("src");
	          var linkHomePage = jQuery("#header_logo > a").attr("href");
	        }
	       }
	       else {
	         $('#header').removeClass('fixed');
	         $('#header').removeClass('fixed-tran');
	         jQuery(".logoFixed").remove();
	       }
	    });
		/**End Dorado Theme Custom**/
	},
	OptionThemes:function(){

	},
	HotDeal:function(){
		$('.hotdeal-items-lists').owlCarousel({
	        items: 3,
	        loop: true,
	        navigation: true,
	        nav: true,
	        autoplay: false,
	        margin:10,
	        responsive: {
	            0: {items: 1},
	            1628: {items: 4},
	            1396: {items: 3},
	            1200: {items: 3},
	            992: {items: 2},
	            650: {items: 3},
	            400: {items: 2},
	            300: {items: 1}
	        },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	    });
	},
	NewArrivals:function(){
		$('.productNewArrivals .product_list').owlCarousel({
	        items: 3,
	        loop: true,
	        navigation: true,
	        nav: true,
	        margin:10,
	        autoplay: false,
	        responsive: {
	            0: {items: 1},
	            1628: {items: 4},
	            1396: {items: 3},
	            1200: {items: 3},
	            992: {items: 2},
	            650: {items: 3},
	            400: {items: 2},
	            300: {items: 1}
	        },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	    });
	},
	BrandsLists:function(){
		$('.brands-lists > ul').owlCarousel({
	        items: 6,
	        loop: true,
	        navigation: true,
	        nav: true,
	        autoplay: true,
	        lazyLoad:true,
	        responsive: {
	            0: {items: 1},
	            1198: {items: 6},
	            1165: {items: 6},
	            990: {items: 4},
	            700: {items: 3},
	            600: {items: 3},
	            480: {items: 2},
	            300: {items: 1}
	        },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	    });
	},
	PartnersLists:function(){
		$('.partners-lists > ul').owlCarousel({
	        items: 8,
	        loop: true,
	        navigation: true,
	        nav: false,
	        autoplay: true,
	        lazyLoad:true,
	        responsive: {
	            0: {items: 1},
	            1628: {items: 10},
	            1396: {items: 8},
	            1200: {items: 7},
	            1165: {items: 6},
	            990: {items: 4},
	            700: {items: 3},
	            600: {items: 3},
	            480: {items: 2},
	            300: {items: 1}
	        },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	    });
	},
	SlideImageTabs:function(){
		$('.slide1 .slideImages, .slide2 .slideImages, .slide3 .slideImages').owlCarousel({
	        items: 1,
	        loop: true,
	        navigation: true,
	        nav: true,
	        autoplay: true,
	        lazyLoad:true,
	        responsive: {
	            0: {items: 1},
	            1198: {items: 1},
	            1165: {items: 1},
	            990: {items: 1},
	            700: {items: 1},
	            600: {items: 1},
	            480: {items: 1},
	            320: {items: 1}
	        },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	    });
	},
	PaymentSlider:function(){
		$('#payment-systems').owlCarousel({
	        items: 8,
	        loop: true,
	        navigation: true,
	        nav: true,
	        autoplay: true,
	        responsive: {
	            0: {items: 1},
	            1198: {items: 8},
	            1165: {items: 7},
	            990: {items: 6},
	            700: {items: 5},
	            600: {items: 4},
	            480: {items: 3},
	            320: {items: 2}
	        },
	        navText: ["<i class='fa fa-long-arrow-left'></i>", "<i class='fa fa-long-arrow-right'></i>"]
	    });
	    $('#brands-systems').owlCarousel({
	        items: 5,
	        loop: true,
	        navigation: true,
	        nav: true,
	        autoplay: true,
	        responsive: {
	            0: {items: 1},
	            1198: {items: 5},
	            1165: {items: 5},
	            990: {items: 5},
	            700: {items: 4},
	            600: {items: 3},
	            480: {items: 3},
	            320: {items: 2}
	        },
	        navText: ["<i class='fa fa-long-arrow-left'></i>", "<i class='fa fa-long-arrow-right'></i>"]
	    });
	},
	RelatedProduct:function(){
		$('.productSameCategory-wrapper .product_list_items').owlCarousel({
	        items: 4,
	        nav: true,
	        autoplay: false,
	        responsive: {
	            0: {items: 1},
	            1200: {items: 4},
	            992: {items: 4},
	            768: {items: 3},
	            450: {items: 2},
	            320: {items: 1}
	        },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	    });
	},
	SlidePictureBlock:function(){
		$('.slide-picture > ul').owlCarousel({
	        items: 1,
	        nav: false,
	        autoplay: true,
	        loop: true,
	        dots:true
	    });
	},
	HomeCategoryProduct:function(){
		$('.dorTabContentHome ul.product_list').owlCarousel({
	        items: 4,
	        rtl: false,
	        center: true,
	        loop: true,
	        dots: false,
	        nav: true,
	        autoplay: false,
	        responsive: {
	            0: {items: 1},
	            1500: {items: 6},
	            1200: {items: 4},
	            990: {items: 4},
	            767: {items: 2},
	            600: {items: 2},
	            450: {items: 1},
	            320: {items: 1}
	        },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	    });
	},
	HomeLastNews:function(){
		$('.gst-post-list').owlCarousel({
	        items: 3,
	        loop: true,
	        nav: true,
	        autoplay: false,
	        margin:10,
	        responsive: {
	            0: {items: 1},
	            1500: {items: 4},
	            1200: {items: 3},
	            990: {items: 3},
	            767: {items: 2},
	            600: {items: 2},
	            450: {items: 1},
	            320: {items: 1}
	        },
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	    });
	},
	EventActionProduct:function(){
		if (jQuery(".scroll-div").length) {
            jQuery(".scroll-div").mCustomScrollbar({
                theme: "dark-2",
                scrollButtons: {
                    enable: false
                }
            });
        }
		if (jQuery("#layered_form div div ul#ul_layered_category_0").length) {
            jQuery("#layered_form div div ul#ul_layered_category_0").mCustomScrollbar({
                theme: "dark-2",
                scrollButtons: {
                    enable: false
                }
            });
        }
        if (jQuery("#ul_layered_manufacturer_0").length) {
            jQuery("#ul_layered_manufacturer_0").mCustomScrollbar({
                theme: "dark-2",
                scrollButtons: {
                    enable: false
                }
            });
        }
		if (jQuery(".list-category-fil > ul").length) {
            jQuery(".list-category-fil > ul").mCustomScrollbar({
                theme: "dark-2",
                scrollButtons: {
                    enable: false
                }
            });
        }
		if (jQuery("#categories_block_left div.block_content > ul").length) {
            jQuery("#categories_block_left div.block_content > ul").mCustomScrollbar({
                theme: "dark-2",
                scrollButtons: {
                    enable: false
                }
            });
        }
        if(jQuery.trim($(".dorRelatedProduct").html()).length == 0) $(".dorRelatedProduct").remove();
	},
	OptShowItems:function(){
		jQuery(".opionnbi").click(function(){
			if(jQuery(this).hasClass("selected")){return false;}
			var valData = jQuery(this).text();
			valData = parseInt(valData);
			$("select#nb_item").val(valData).change();
			jQuery(".opionnbi").removeClass("selected");
			jQuery(this).addClass("selected");
		});
	},
	
	MoveThumbListDetail:function(){
		if($(".primary_block").hasClass("dor-media-top")){
			jQuery("#views_block").detach().insertBefore('#image-block');
		}
	},
	SubmitFormBase:function(){
		jQuery("#submitAddress").click(function(){
			var validate = true, mess = "";
			var arrField = ['firstname','lastname','address1','city','postcode','phone','phone_mobile','alias'];
			jQuery("input[name='"+field+"']").removeClass("hight-light");
			var idState = jQuery("select[name='id_state']").val();
			var checkState = jQuery(".id_state").css("opacity");
			if(typeof jQuery("select[name='id_state']") != "undefined" && parseInt(checkState) == 1 && idState == ""){
				jQuery("button[data-id='id_state']").addClass("hight-light");
				mess += "";
				validate = false;
			}else{
				jQuery("button[data-id='id_state']").removeClass("hight-light");
			}
			for(var i=0 in arrField){
				var field = arrField[i];
				var valField = jQuery("input[name='"+field+"']").val();
				var valGet = jQuery.trim(valField).length;
				if( typeof jQuery("input[name='"+field+"']") != "undefined" && valGet==0){
					jQuery("input[name='"+field+"']").addClass("hight-light");
					mess += "";
					validate = false;
				}else{
					jQuery("input[name='"+field+"']").removeClass("hight-light");
				}
			}
			return validate;
		});
	},
	ChooseCountryAddress:function(){
		jQuery("#address #id_country").change(function(){
			var val = jQuery(this).val();
			jQuery(".id_state").css("opacity",1);
			setTimeout(function(){
				var lengthOpts = jQuery("#id_state option").length;
				if(lengthOpts <= 1){
					jQuery(".id_state").css("opacity",0);
				}
			},500);
		});
	},
	CutTextCountDown:function(){
		var textName = jQuery(".titleProductCountdown").text();
		textName = jQuery.trim(textName).split(' ');
		if(textName.length > 1){
			textName[textName.length-1] = '<span class="thm-clr">'+textName[textName.length-1]+'</span>';
		}
		jQuery(".titleProductCountdown").html(textName.join(" "));
		
	},
	Testimonial:function(){
	    $('.they-say').owlCarousel({
	        items: 1,
	        loop: true,
	        navigation: true,
	        nav: true,
	        autoplay: true,
	        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	    });
	},
	MobileLinkTop:function(){
		jQuery(".dor-top-link-mobile").click(function(){
			jQuery(".pull-right.list-unstyled.list-inline").slideToggle();
		});
		if(parseInt($( window ).width()) <= 655){
			jQuery(document).click(function (event) {
	            if (!jQuery(event.target).is(".dor-top-link-mobile i.fa.fa-cog, .pull-right.list-unstyled.list-inline, .pull-right.list-unstyled.list-inline *")) {
	                jQuery(".pull-right.list-unstyled.list-inline").hide();
	            }
	        });
		}
		$( window ).resize(function() {
		  	var widthW = $( window ).width();
			if(parseInt(widthW) <= 655){
				jQuery(document).click(function (event) {
		            if (parseInt($( window ).width()) <= 655 && !jQuery(event.target).is(".dor-top-link-mobile i.fa.fa-cog, .pull-right.list-unstyled.list-inline, .pull-right.list-unstyled.list-inline *")) {
		                jQuery(".pull-right.list-unstyled.list-inline").hide();
		            }
		        });
			}else{
				jQuery(".pull-right.list-unstyled.list-inline").show();
			}
		});
        jQuery(".header_user_info .current > a").click(function(){
			jQuery(".header_user_info ul.toogle_content").toggle();
		});
	},
	ShowPopupScre:function(){
		$('.subscribe-me').bPopup({
	    	easing: 'easeOutBack',
            speed: 450,
            transition: 'slideDown'
        });
	},
	BlockHover:function(status)
	{
		/*var screenLg = $('body').find('.container').width() == 1170;

		$(document).off('mouseenter').on('mouseenter', '.hotdeal-items-lists .owl-item .product-container', function(e){
			if (screenLg)
			{
				var pcHeight = $(this).parent().parent().outerHeight();

				var pcPHeight = $(this).parent().parent().find('.button-container').outerHeight() + $(this).parent().parent().find('.comments_note').outerHeight() + $(this).parent().parent().find('.functional-buttons').outerHeight();
				$(this).parent().parent().addClass('hovered').css({'height':pcHeight, 'margin-bottom':pcPHeight * (-1)});
				$(this).find('.button-container').show();
				$(".hotdeal-items-lists .owl-stage-outer").css({'height':pcHeight});
			}
		});

		$(document).off('mouseleave').on('mouseleave', '.hotdeal-items-lists .owl-item .product-container', function(e){
			if (screenLg)
			{
				$(this).parent().parent().removeClass('hovered').css({'height':'auto', 'margin-bottom':'0'});
				$(this).find('.button-container').hide();
				$(".hotdeal-items-lists .owl-stage-outer").css({'height':'auto'});
			}
		});*/
	}
}
jQuery(".blockmainmenu_footer").detach().insertAfter('#footer > div > div.clearfix');
/*jQuery("#layer_cart").detach().insertAfter('#header .container');
jQuery("#header > .nav #layer_cart").remove();*/

jQuery("#layer_cart .layer_cart_cart .button-container").detach().insertAfter('#layer_cart .layer_cart_product .layer_cart_product_info');
if(parseInt($( window ).width()) <= 767){
  	jQuery("#left_column").detach().insertAfter('#center_column');
}
jQuery(document).ready(function(){
	DORSCRIPT.init();
	DORSCRIPT.ChooseCountryAddress();
	DORSCRIPT.CutTextCountDown();
})
DORSCRIPT.MoveThumbListDetail();
jQuery(document).ready(function ($) {
(function () {
    if (typeof google === 'object' && typeof google.maps === 'object') {
        if (jQuery('#map-canvas2').length) {

            var map;
            var marker;
            var infowindow;
            var mapIWcontent = '' +
                    '' +
                    '<div class="map-info-window">' +
                    '<div class="map-location">' +
                    '<div class="loctn-img">' +
                    '<a class="media-link" href="#">' +
                    '<img class="img-responsive" src="../img/cms/map-location.jpg" alt=""/>' +
                    '</a>' +
                    '</div>' +
                    '<div class="loctn-info">' +
                    '<h4 class="title-2"> Location </h4>' +
                    '<p> 79 Orchard St,New York <br>NY 10002, USA </p>' +
                    '<p> (0096) 8645 234 438 </p>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '';
            var contentString = '' +
                    '' +
                    '<div class="iw-container">' +
                    '<div class="iw-content">' +
                    '' + mapIWcontent +
                    '</div>' +
                    '<div class="iw-bottom-gradient"></div>' +
                    '</div>' +
                    '' +
                    '';
            var image = '../img/cms/map-icon.png'; // marker icon
            google.maps.event.addDomListener(window, 'load', function () {
                var mapOptions = {
                    scrollwheel: false,
                    zoom: 10,
                    center: new google.maps.LatLng(41.079379, 28.9984466) // map coordinates
                };

                map = new google.maps.Map(document.getElementById('map-canvas2'),
                        mapOptions);
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(41.0096559, 28.9755535), // marker coordinates
                    map: map,
                    icon: image,
                    title: 'Hello World!'
                });


            });

        }
    }
}());
});

jQuery(window).scroll(function () {

    // --------------------------- Scroll To Top ------------------------ //
    if (jQuery(this).scrollTop() > 100) {
        jQuery('.to-top').css({bottom: '0px'});
    }
    else {
        jQuery('.to-top').css({bottom: '-150px'});
    }

});