<?php
	class Search extends  SearchCore {
		public function fetchCategoryTree($id_category = 1, $dataReturn, $id_lang = false, $id_shop = false,$recursive = true) {
	        if (!is_array($dataReturn))
	        $dataReturn = array();
	        $id_lang = $id_lang ? (int)$id_lang : (int)Context::getContext()->language->id;
	        $category = new Category((int)$id_category, (int)$id_lang, (int)$id_shop);
	        if (is_null($category->id))
	            return;


	        if ($recursive)
	        {
	            $children = Category::getChildren((int)$id_category, (int)$id_lang, true, (int)$id_shop);
	        }
	        $dataReturn[] = array(
	            "id_category" => $category->id_category
	        );
	        if (isset($children) && count($children))
	            foreach ($children as $child)
	                $dataReturn = Search::fetchCategoryTree((int)$child['id_category'], $dataReturn, (int)$id_lang, (int)$child['id_shop']);
	        return $dataReturn ;
	    }
	}